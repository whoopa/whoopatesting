::Enable Sentry in index.android.js
::Update API Config
::Update version in login reducer
::Update Version in android\app\build.gradle

::call react-native bundle --platform android --dev false --entry-file index.android.js --bundle-output android\app\src\main\assets\index.android.bundle --assets-dest android\app\src\main\res\
cd android
::del /Q .\app\src\main\res\drawable-mdpi\*
::del /Q .\app\src\main\res\drawable-hdpi\*
::del /Q .\app\src\main\res\drawable-xhdpi\*
::del /Q .\app\src\main\res\drawable-xxhdpi\*
::del /Q .\app\src\main\res\drawable-xxxhdpi\*
call .\gradlew.bat clean --console plain
call .\gradlew.bat assembleRelease --console plain
adb install -r .\app\build\outputs\apk\release\app-release.apk
cd ..
