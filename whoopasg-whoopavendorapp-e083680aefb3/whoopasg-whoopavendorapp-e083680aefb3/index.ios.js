import { AppRegistry } from 'react-native';
import App from './App';

import { Sentry } from 'react-native-sentry';

Sentry.config("https://0d908866de6347e7a04dbe7855188e97:6036a0dc76194227b4a5368f1bd8e92b@sentry.io/216909").install();

AppRegistry.registerComponent('WhoopaVendorApp', () => App);
