import React from 'react';
import {StyleSheet, View, StatusBar, Linking} from 'react-native';
import {Spinner, Button, AppText} from './src/components';
import {Provider} from 'react-redux';
import configureStore from "./src/store";
import AppRouter from "./src/router";
import I18n from './src/translations'
import {persistStore} from 'redux-persist'
import {AsyncStorage} from 'react-native'
import VersionCheck from 'react-native-version-check';
import FCM from 'react-native-fcm';
import API from "./src/services/api/index";


const store = configureStore();

export default class App extends React.Component {
    state = {
        upgradeAvailable: null,
        hydrated: false,
        dataStatus: "Loading",
        notificationStatus: "Loading",
        versionCheckStatus: "Loading"
    };

    componentDidMount() {
        // iOS: show permission prompt for the first call. later just check permission in user settings
        // Android: check permission in user settings
        this.setState({notificationStatus: "Checking Notification Permissions..."});
        FCM.requestPermissions()
            .then(() =>
                this.setState({notificationStatus: "Notification access confirmed!"})
            )
            .catch(() =>
                this.setState({notificationStatus: "Notification access not granted!"})
            );
    }

    componentWillMount() {
        //Initialize the API to get the locale
        const api = API.instance();

        this.setState({versionCheckStatus: "Checking for latest version..."});
        VersionCheck.getLatestVersion()
            .then(latestVersion  => {
                const needsUpdate = parseFloat(latestVersion) > parseFloat(VersionCheck.getCurrentVersion());
                console.log(`Current version:${VersionCheck.getCurrentVersion()} | Latest Version: ${latestVersion} | Needs Update: ${needsUpdate}`)
                this.setState({
                    versionCheckStatus: `Current version:${VersionCheck.getCurrentVersion()} | Latest Version: ${latestVersion} | Needs Update: ${needsUpdate}`,
                    upgradeAvailable: needsUpdate
                });
            })
            .catch(e => {
              console.log(`Error! Version Check Failed with message`, e)
                this.setState({
                    versionCheckStatus: `Error! Version Check Failed with message: ${e.message}`,
                    upgradeAvailable: false,
                });
            })
        ;


        // begin periodically persisting the store for auto-rehydrations
        // Keeps the data stored offline
        this.setState({dataStatus: "Loading from local storage..."});
        persistStore(store, {storage: AsyncStorage}, () => {
            this.setState({dataStatus: "Loaded"});
            this.setState({
                ...this.state,
                hydrated: true
            })
        })
    }

    render() {
        if (!this.state.hydrated || this.state.upgradeAvailable === null) {
            return (
                <View style={styles.container}>
                    <StatusBar hidden/>
                    <Spinner/>
                    <AppText style={{margin: 5}}>Data: {this.state.dataStatus}</AppText>
                    <AppText style={{margin: 5}}>Permissions: {this.state.notificationStatus}</AppText>
                    <AppText style={{margin: 5}}>Version Check: {this.state.versionCheckStatus}</AppText>
                </View>
            );
        }

        if (this.state.upgradeAvailable === true) {
            return (
                <View style={styles.container}>
                    <AppText style={{margin: 20}}>
                        {I18n.t('upgradeNotice')}
                    </AppText>
                    <Button onPress={async () => {
                        Linking.openURL(await VersionCheck.getStoreUrl())
                    }}>
                        {I18n.t('upgradeButton')}
                    </Button>
                </View>
            );
        }

        return (
            <Provider store={store} style={styles.container}>
                <AppRouter/>
            </Provider>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
    }
});
