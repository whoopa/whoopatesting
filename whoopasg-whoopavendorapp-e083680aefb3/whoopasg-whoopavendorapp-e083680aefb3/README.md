## Whoopa Vendor App

* [Platform and Requirements](#platform-and-requirements)
* [Dev Environment Setup](#dev-environment-setup)
* [Testing on Android](#testing-on-android)
* [Building for Playstore](#building-for-playstore)

## Platform and Requirements

React-Native based app
Requires the following to run
* nodejs
* react-native-client
* android-sdk

## Dev Environment Setup
* ```npm install```
* ```react-native link```
* ```RNFB_ANDROID_PERMISSIONS=true react-native link react-native-fetch-blob```

## Testing on Android

Connect your PC to a android device or start up an emulator, and then run'
```
react-native run-android
```
## Building for Playstore
1. Update src/services/api/config.js and enable the production API Config. Comment out the dev config.
2. Run the following commands

    ```
    react-native bundle --platform android --dev false --entry-file index.android.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/
    cd android
    .\gradlew.bat assembleRelease --console plain
    cd ..
    ```
    
The apk can be found under *android/app/build/outputs/apk/app-release.apk*

