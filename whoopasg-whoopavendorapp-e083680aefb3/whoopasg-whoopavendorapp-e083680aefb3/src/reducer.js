import { reducer as screenReducer } from './screens/reducer';
import { combineReducers } from 'redux';

export default combineReducers({...screenReducer});