export const groupBy = function(xs, key) {
    return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

export const transformOrder = function (uid, order){
    const {FCMToken, paid, orderNumber, remarks, tableNumber} = order;
    return {
        FCMToken,
        id: uid,
        complete: false,
        items: order.items.map((item, idx)=>{
            return {
                id: idx,
                menu_item: item.menuItemName,
                menu_item_id: item.id,
                price: parseFloat(item.price),
                quantity: item.quantity,
                size: item.sizeName,
                status: 0,
                table_number: tableNumber,
                take_away: (item.takeAway === 1),
                customizations: [...item.customizationInfo]
            }
        }),
        order_number: orderNumber,
        paid,
        price: order.items.reduce((total, item)=>{
            return total + parseFloat(item.price);
        }, 0),
        remarks,
        table_number: tableNumber,
        loading: true
    };

};