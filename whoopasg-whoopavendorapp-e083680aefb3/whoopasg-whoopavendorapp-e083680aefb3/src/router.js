import React from 'react';
import {Scene, Router, Modal, Lightbox, Reducer} from 'react-native-router-flux';
import Login from './screens/Login';
import Menu from './screens/Menu';
import Orders from './screens/Orders';
import Simplified from './screens/Simplified';
import Drawer from './screens/Drawer';
import I18n from './translations'
import {MenuItemsOverlay, ProcessingItemsOverlay} from './screens/Orders/components';
import {OrderConfirmationOverlay, EditItemOverlay} from './screens/Simplified/components';
import {OrderReport,ColumnSelectionOverlay,CalendarOverlay} from './screens/Drawer/components';
import {ConfirmationOverlay, NotificationOverlay, NavBar} from './components'
import {SelectedMenuItemOverlay, OrdersOverlay, KeyPadOverlay, RemarksOverlay} from './screens/Menu/components';
import {connect} from 'react-redux';


const AppRouter = (props) => {
    const reducerCreate = (params) => {
        /** Disabled screen routing to improve performance **/
        // const defaultReducer = new Reducer(params);
        // return (state, action) => {
        //     props.dispatch(action);
        //     return defaultReducer(state, action);
        // };
    };
    return (

        <Router createReducer={reducerCreate.bind(this)}>
            <Lightbox>
                <Scene key="root" titleStyle={{}}>
                    <Scene key="auth" hideNavBar>
                        <Scene key="login" component={Login} title={ I18n.t('loginTitle') }/>
                    </Scene>
                    <Scene key="drawer" drawer drawerPosition="right" drawerWidth={200} contentComponent={Drawer}
                           navBar={NavBar}>
                        <Lightbox key="fullApp">
                            <Scene key="main" tabs hideTabBar swipeEnabled={false}>
                                <Scene key="menu" component={Menu} title={ I18n.t('menuTitle') } hideNavBar/>
                                <Scene key="orders" component={Orders} title={ I18n.t('ordersTitle') } hideNavBar/>
                            </Scene>
                            <Scene key="ordersMatchingItems" component={MenuItemsOverlay}/>
                            <Scene key="ordersProcessingItems" component={ProcessingItemsOverlay}/>
                            <Scene key="menuSelectedItemOverlay" component={SelectedMenuItemOverlay}/>
                            <Scene key="menuOrdersOverlay" component={OrdersOverlay}/>
                            <Scene key="keyPadOverlay" component={KeyPadOverlay}/>
                            <Scene key="remarksOverlay" component={RemarksOverlay}/>
                        </Lightbox>
                        <Scene key="simplifiedApp" component={Simplified} hideNavBar/>
                        <Scene key="orderReport" component={OrderReport} hideNavBar/>
                    </Scene>
                </Scene>
                <Scene key="orderConfirmationOverlay" component={OrderConfirmationOverlay} hideNavBar/>
                <Scene key="editItemOverlay" component={EditItemOverlay} hideNavBar/>
                <Scene key="offlineOverlay" component={NotificationOverlay}/>
                <Scene key="confirmationOverlay" component={ConfirmationOverlay}/>
                <Scene key="notificationOverlay" component={NotificationOverlay}/>
                <Scene key="columnSelectionOverlay" component={ColumnSelectionOverlay} hideNavBar/>
                <Scene key="calendarOverlay" component={CalendarOverlay} hideNavBar/>
            </Lightbox>
        </Router>
    );
};

export default AppRouter;
//export default connect()(AppRouter);