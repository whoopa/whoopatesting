import React, {Component} from 'react';
import {View, StyleSheet, ScrollView, StatusBar, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {
    loadData,
    itemSelected,
    toggleProcessing,
    orderCompleted,
    markItem,
    toggleServed,
    onEdit,
    onScrollReset,
    orderPaid
} from './actions';
import {OrderCard, Spinner, AppText} from '../../components';
import {orderSchema} from './reducer';
import {denormalize} from 'normalizr';

class Orders extends Component {

    static timer = null;
    state = {
        reset: false,
        showLeft: false,
        showRight: false,
    };

    componentWillMount() {
        console.log("componentWillMount");
        if (Orders.timer === null) {
            Orders.timer = setInterval(this.processQueue.bind(this), 10000);
        }
    }

    componentDidMount() {
        this.loadOrderData();
    }

    componentDidUpdate() {
        if (this.props.resetScroll) {
            this.refs._mainScrollView.scrollTo();
            this.props.onScrollReset();
        }
        this.loadOrderData();
    }

    loadOrderData() {
        const {orders, password, authenticated, loadData, loading} = this.props;
        if (authenticated && orders === null && password !== null && !loading) {
            loadData(password);
        }
    }

    processQueue() {
        console.log("Processing Order API Queue");
        const {apiQueue, markItem, password, authenticated} = this.props;
        if (authenticated && apiQueue && apiQueue.length > 0) {
            console.log(apiQueue.length + " in Queue");
            const next = apiQueue[0]; // get the first item
            const {id, itemId, itemState, timestamp} = next;
            markItem(password, id, itemId, itemState, timestamp)
        }
    }

    renderUnpaid() {
        const {unpaidOrders, itemSelected, orderCompleted, toggleProcessing, toggleServed, password, onEdit} = this.props;
        if (unpaidOrders) {
            let elevation = unpaidOrders.length + 3;
            return unpaidOrders.map(order => {
                elevation--;
                return <OrderCard
                    collapsible
                    elevation={elevation}
                    order={order}
                    key={'OrderCard' + order.id}
                    onTap={(item) => {
                        toggleProcessing(order.id, item)
                    }}
                    onDoubleTap={itemSelected}
                    onComplete={() => {
                        orderCompleted(password, order.id, order.price, 4)
                    }}
                    onButtonPress={(item) => {
                        toggleServed(order.id, item)
                    }}
                    onEdit={(item) => {
                        onEdit(order.id, item, true)
                    }}
                />
            });
        }
        return <Spinner size="large"/>
    }

    onContentChange(contentWidth, contentHeight){
        const {width} = Dimensions.get('window');
        console.log("onContentChange", contentWidth, width);
        if(contentWidth <= width){
            this.setState({
                ...this.state,
                showLeft: false,
                showRight: false
            });
        }else{
            this.setState({
                ...this.state,
                showRight: true
            });
        }
    }

    renderScrollMarkers(e) {
        const viewWidth = e.nativeEvent.layoutMeasurement.width;
        const contentWidth = e.nativeEvent.contentSize.width;
        const xPos = e.nativeEvent.contentOffset.x;
        let showLeft = false;
        let showRight = false;
        if (contentWidth > viewWidth) {
            if (xPos > 0) {
                console.log('show left');
                showLeft = true;
            }
            if (xPos + viewWidth < contentWidth) {
                console.log('show right');
                showRight = true;
            }
        }
        this.setState({
            ...this.state,
            showLeft, showRight
        });
    }

    renderOrders() {
        const {incompleteOrders, itemSelected, orderCompleted, toggleProcessing, toggleServed, password, onEdit, orderPaid} = this.props;
        if (incompleteOrders) {
            return incompleteOrders.map(order => {
                return <OrderCard
                    order={order}
                    key={'OrderCard' + order.id}
                    onTap={(item) => {
                        toggleProcessing(order.id, item)
                    }}
                    onDoubleTap={itemSelected}
                    onButtonPress={(item) => {
                        toggleServed(order.id, item)
                    }}
                    onPay={() => {
                        orderPaid(password, order.id, order.price, 2)
                    }}
                    onComplete={() => {
                        orderCompleted(password, order.id, order.price, 3)
                    }}
                    onEdit={(item) => {
                        onEdit(order.id, item, (order.paid === order.price))
                    }}
                />
            });
        }
    }

    renderUnsaved() {
        const { unsaved } = this.props;
        console.log(unsaved);
        if (unsaved) {
            return Object.keys(unsaved).map(uid => {
                const order = unsaved[uid];
                return <OrderCard
                    order={order}
                    key={'OrderCard' + uid}
                />
            });
        }
    }

    render() {
        return (
            <View style={styles.outerContainer}>
                <StatusBar hidden/>
                <View style={{flex: 0, marginTop: 0, marginBottom: 5}}>
                    <ScrollView ref='_mainScrollView' horizontal
                                onContentSizeChange={this.onContentChange.bind(this)}
                                onMomentumScrollEnd={this.renderScrollMarkers.bind(this)}>
                        <ScrollView style={{marginTop: 20}}>
                            {this.renderUnpaid()}
                        </ScrollView>
                        <View style={styles.container}>
                            {this.renderOrders()}
                            {this.renderUnsaved()}
                        </View>
                    </ScrollView>
                    {this.state.showLeft ?
                        <View style={[styles.scrollMarker, {left: 5}]}>
                            <AppText style={styles.scrollMarkerText}>&lt;</AppText>
                        </View>
                        : null}
                    {this.state.showRight ?
                        <View style={[styles.scrollMarker, {right: 5}]}>
                            <AppText style={styles.scrollMarkerText}>&gt;</AppText>
                        </View>
                        : null}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollMarker: {
        width: 40,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: '50%',
        backgroundColor: '#FFF'
    },
    scrollMarkerText: {
        flex: 1,
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'right'
    },
    outerContainer: {
        flex: 1,
        alignItems: 'flex-start',
        backgroundColor: '#222',
        flexDirection: 'row'
    },
    container: {
        flex: 0,
        alignItems: 'flex-start',
        backgroundColor: '#222',
        flexDirection: 'row',
        marginRight: 1 // silly adjustment to make the overlay items line up when orders are scrolled to the last item in the list
    },
});

const mapStateToProps = (state) => {
    const {orders, items, apiQueue, resetScroll, loading, unsaved} = state.kitchen;
    const {password, authenticated} = state.user;

    const unpaidOrders = [];
    const incompleteOrders = [];
    if (orders) {
        Object.keys(orders).map(key => {
            const data = denormalize(orders[key], orderSchema, {orders, items});
            //if(data.items){
            if (data.items.filter(item => (item.status !== 2)).length === 0) {
                unpaidOrders.push(data);
            } else {
                incompleteOrders.push(data);
            }
            //}
        });
    }

    return {orders, password, authenticated, unpaidOrders, incompleteOrders, items, apiQueue, resetScroll, loading, unsaved};
};

export default connect(mapStateToProps, {
    loadData,
    itemSelected,
    toggleProcessing,
    orderCompleted,
    markItem,
    toggleServed,
    onEdit,
    onScrollReset,
    orderPaid
})(Orders);