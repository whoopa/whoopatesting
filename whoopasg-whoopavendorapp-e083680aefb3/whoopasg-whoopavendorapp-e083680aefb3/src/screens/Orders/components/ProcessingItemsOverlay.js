import React, {Component} from 'react';
import {View, StyleSheet, StatusBar, ScrollView, TouchableWithoutFeedback} from 'react-native';
import {connect} from 'react-redux';
import {OrderItemCard, AppText} from '../../../components';
import I18n from '../../../translations'
import {toggleServed} from '../actions'
import {orderSchema} from '../reducer';
import {denormalize} from 'normalizr';
import {groupBy} from '../../../utilities/ObjectUtils'
import {Actions} from 'react-native-router-flux';

class ProcessingItemsOverlay extends Component {

    componentWillMount() {
    }

    renderGroups() {
        const {orderItems} = this.props;
        if (orderItems) {
            return Object.keys(orderItems).map(key => {
                return (
                    <View
                        key={`Group${key}`}
                        style={styles.groupContainer}>
                        <View style={styles.row}>
                            <AppText style={styles.title}>{key}</AppText>
                        </View>
                        <View style={{flexDirection: 'column', flexWrap: 'wrap'}}>
                            {this.renderItems(key)}
                        </View>
                    </View>
                );
            });
        }
    }

    renderItems(key) {
        const {orderItems, password, toggleServed} = this.props;
        if (orderItems) {
            return orderItems[key].map(item => {
                return (
                    <View style={styles.cardContainer}
                          key={'item' + item.id}>
                        <OrderItemCard
                            item={item}
                            altTitle={`${item.vendor_order_number}`}
                            onServe={(e) => {
                                if(item.isLast === true){
                                    Actions.push('confirmationOverlay', {
                                        message: I18n.t('confirmServedMessage'),
                                        confirmText: I18n.t('yes'),
                                        cancelText: I18n.t('no'),
                                        onConfirmation: ()=>{toggleServed(item.vendor_order_id, item)},
                                        onCancel: ()=> {},
                                        showCalculator: false,
                                        style: {left: (e.nativeEvent.pageX - e.nativeEvent.locationX), top:(e.nativeEvent.pageY - e.nativeEvent.locationY), position: 'absolute' }
                                    });
                                }else{
                                    toggleServed(item.vendor_order_id, item)
                                }
                            }}
                        />
                    </View>
                );
            });
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar hidden/>
                <View style={styles.row}>
                    <ScrollView horizontal>
                        {this.renderGroups()}
                    </ScrollView>
                </View>
                <View style={styles.closeContainer}>
                    <TouchableWithoutFeedback onPress={Actions.pop}>
                        <View><AppText style={styles.close}>X</AppText></View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignSelf: 'stretch',
        alignItems: 'stretch',
        justifyContent: 'space-between'
    },
    closeContainer: {
        position: 'absolute',
        right: 10,
        top: 10,
    },
    container: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#000",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        paddingTop: 10,
        padding: 12,
    },
    title: {
        color: '#FFF',
        fontSize: 32,
        padding: 10,
    },
    close: {
        color: '#FFF',
        fontSize: 32,
        width: 55,
        height: 55,
        borderRadius: 100,
        borderColor: '#FFF',
        borderWidth: 3,
        textAlign: 'center',
        alignSelf: 'flex-end',
        paddingTop: 5,
        backgroundColor: "#000",
    },
    cardContainer: {
        marginRight: 20,
        marginBottom: 20
    },
    groupContainer: {
        marginRight: 19,
        borderRightWidth: 1,
        borderRightColor: 'rgba(255,255,255,0.5)'
    }
});

const mapStateToProps = (state) => {
    const {orders, items} = state.kitchen;
    const {password} = state.user;


    let orderItems = null;
    if (orders) {
        orderItems = [];

        Object.keys(orders).map(id => {
            let order = orders[id];
            let denormalized = denormalize(order, orderSchema, {orders, items});
            let numServed = false;
            let itemsInProcess = (denormalized.items.filter(item => {
                if (item.status === 2) {
                    numServed++
                }
                return item.status === 1; //return processing
            }));

            // if order has one item in processing
            // and order is paid
            // and is last item of order
            if(itemsInProcess.length === 1 &&
                order.paid === order.price &&
                (order.items.length - numServed === 1)){

                itemsInProcess[0].isLast = true;
            }

            orderItems.push(...itemsInProcess);
        });
        orderItems = groupBy(orderItems, 'menu_item');
    }
    // if (items) {
    //     orderItems = Object.keys(items)
    //         .filter(key => (items[key].status === 1))
    //         .map(key => items[key]);
    //     orderItems = groupBy(orderItems, 'menu_item');
    // }


    return {password, orderItems, orders};
};

export default connect(mapStateToProps, {toggleServed})(ProcessingItemsOverlay);