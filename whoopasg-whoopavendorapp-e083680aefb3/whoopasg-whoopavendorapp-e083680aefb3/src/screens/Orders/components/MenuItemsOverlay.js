import React, {Component} from 'react';
import {View, StyleSheet, StatusBar, ScrollView, TouchableWithoutFeedback} from 'react-native';
import {connect} from 'react-redux';
import {OrderItemCard, AppText} from '../../../components';
import { toggleProcessing, toggleServed } from '../actions'
import { orderSchema } from '../reducer';
import { denormalize } from 'normalizr';
import { Actions } from 'react-native-router-flux';

class MenuItemsOverlay extends Component {

    componentWillMount() {
    }

    renderItems() {
        const {orderItems, password, toggleProcessing, toggleServed} = this.props;
        if (orderItems) {
            let items = [];
            orderItems.map(order => {
                order.items.map(item => {
                    items.push(item);
                });
            });
            // Orders with no customizations first
            items = items.sort((a,b)=>{
                return a.customizations.length > b.customizations.length;
                //return a.customizations.length > b.customizations.length
            });

            return items.map(item=>(
                <View style={styles.cardContainer}
                      key={'item' + item.vendor_order_id + item.id}>
                    <OrderItemCard
                        item={item}
                        altTitle={`${item.vendor_order_number}`}
                        enableButtons={false}
                        onTap={()=>toggleProcessing(item.vendor_order_id, item)}
                    />
                </View>
            ));
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar hidden/>
                <View style={styles.row}>
                    <AppText style={styles.title}>{this.props.selectedItemName}</AppText>
                    <TouchableWithoutFeedback onPress={Actions.pop}>
                        <View><AppText style={styles.close}>X</AppText></View>
                    </TouchableWithoutFeedback>
                </View>
                <ScrollView horizontal>
                    <View style={{flexDirection: 'column', flexWrap: 'wrap'}}>
                        {this.renderItems()}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    row: {
      flexDirection: 'row',
      alignSelf: 'stretch',
        alignItems: 'stretch',
        justifyContent: 'space-between'
    },
    container: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "rgba(0,0,0,0.9)",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        padding: 13,
    },
    title: {
        color: '#FFF',
        fontSize: 32,
        padding: 10,
    },
    close: {
        color: '#FFF',
        fontSize: 32,
        width: 55,
        height: 55,
        borderRadius: 100,
        borderColor: '#FFF',
        borderWidth: 3,
        textAlign: 'center',
        alignSelf: 'flex-end',
        paddingTop: 5,

    },
    cardContainer:{
        marginRight: 25,
        marginBottom: 20,
    }
});

const mapStateToProps = (state) => {
    const {orders, items, selectedItem} = state.kitchen;
    const {password} = state.user;
    let selectedItemName = false;
    let orderItems = null;
    if (orders) {
        orderItems = Object.keys(orders).map(key => {
            let filteredItems = null;
            const order = denormalize(orders[key], orderSchema, {orders, items});
            if (order.items) {
                filteredItems = order.items.filter(item => {
                    if (!selectedItemName && item.menu_item_id === selectedItem) {
                        selectedItemName = item.menu_item;
                    }
                    //Pick same menu items that have not been served
                    return (item.menu_item_id === selectedItem && item.status !== 2);
                });
            }
            return {...order, items: filteredItems};
        });

        orderItems = orderItems.filter(order => (order.items !== null && order.items.length > 0));
    }


    return {password, orderItems, selectedItemName};
};

export default connect(mapStateToProps, {toggleProcessing, toggleServed})(MenuItemsOverlay);