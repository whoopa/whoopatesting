import * as actionTypes from './actionTypes';
import * as drawerActionTypes from '../Drawer/actionTypes';
import * as loginActionTypes from '../Login/actionTypes';
import * as menuActionTypes from '../Menu/actionTypes';
import {transformOrder} from '../../utilities/ObjectUtils'
import { normalize, schema, arrayOf } from 'normalizr';
import update from 'immutability-helper';


const initialState = {
    orders: null,
    unsaved: null,
    items: null,
    selectedItem: null,
    apiQueue: null,
    resetScroll: false,
    loading: false
};

export const itemSchema = new schema.Entity('items');
export const orderSchema = new schema.Entity('orders', {
    items: [itemSchema]
});
const ordersSchema = new schema.Array(orderSchema);

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "REACT_NATIVE_ROUTER_FLUX_JUMP":
            console.log("On Jump", action);
            if(action.routeName === "orders" && action.params.clicked === true){
                return {...state, resetScroll: true};
            }
            return state;
            break;
        case drawerActionTypes.ORDER_TAB_CLICKED:
            return {...state, resetScroll: true};
        case actionTypes.SCROLL_RESET:
            return {...state, resetScroll: false};

        case actionTypes.ORDER_ITEM_SELECTED:
            return {...state, selectedItem: action.itemId};
        case actionTypes.ORDER_UPDATING:
            return setOrderData(state, action.id, {loading: true});
        case actionTypes.ORDER_PAID:
            return setOrderData(state, action.id, {loading: false, paid: action.paid});
        case actionTypes.ORDER_COMPLETE:
            //Delete the order
            return {
                ...state,
                items: update(state.items, {$unset: state.orders[action.id].items}),
                orders: update(state.orders, {$unset: [action.id]})
            };

        case actionTypes.ORDERS_LOADING:
            return {
                ...state,
                loading: true
            };
        case actionTypes.ORDER_LOADING_FAILED:
            return {
                ...state,
                loading: false
            };
        case actionTypes.ORDERS_LOADED:
            if (action.payload) {
                let ordersData = action.payload.map(calculateOrderPrice);
                const normalizedData = normalize(ordersData, ordersSchema);
                const { orders, items } = normalizedData.entities;
                return {...state, orders, items, loading: false }
            }
            return state;

        case menuActionTypes.CONFIRMING_ORDER:
            return {
                ...state,
                unsaved: {
                    ...state.unsaved,
                    [action.uid]: transformOrder(action.uid, action.order) //Convert order data into the same format as seen in the API
                }
            };
        case drawerActionTypes.ORDER_RECEIVED:
        case menuActionTypes.ORDER_CONFIRMED:
        case menuActionTypes.ORDER_UPDATED:
            //Merge new order
            let normalizedData = normalize(calculateOrderPrice(action.payload), orderSchema);
            let { orders, items } = normalizedData.entities;
            let unsaved = {};
            if(state.unsaved){
              unsaved = update(state.unsaved, {$unset: [action.uid]});
            }
            return {
                ...state,
                unsaved,
                orders:{
                    ...state.orders,
                    ...orders
                },
                items:{
                    ...state.items,
                    ...items
                },
            };

        case actionTypes.UPDATE_FAILED:
            return setOrderData(state, action.id, {loading: false});

        case actionTypes.ADD_ITEM_CALL:
            let apiQueue = [];
            if(state.apiQueue !== null){
                apiQueue = state.apiQueue.filter((queue)=> {
                    // create a copy of calls already in process
                    // Or are not the same item
                    // Same item ignored for efficiency
                    return (
                        queue.processing === true || queue.id !== action.id || queue.itemId !== action.itemId
                    )
                });
            }
            apiQueue.push({
                timestamp: action.timestamp,
                id: action.id,
                itemId: action.itemId,
                itemState: action.itemState,
                processing: false
            });

            //set state and add to queue
            return setItemDataWithQueue(state, action.itemId, {status: action.itemState}, apiQueue);


        case actionTypes.PROCESSING_ITEM_CALL:
            apiQueue = [];
            if(state.apiQueue !== null){
                apiQueue = state.apiQueue.map(queue => {
                    if(queue.timestamp === action.timestamp){
                        queue.processing = true;
                    }
                    return queue;
                });
            }
            return {
                ...state,
                apiQueue
            };

        case actionTypes.UPDATE_ITEM_FAILED:
            apiQueue = [];
            if(state.apiQueue !== null){
                apiQueue = state.apiQueue.map(queue => {
                    if(queue.timestamp === action.timestamp){
                        queue.processing = false;
                    }
                    return queue;
                });
            }
            return {
                ...state,
                apiQueue
            };

        case actionTypes.REMOVE_ITEM_CALL:
            apiQueue = [];
            if(state.apiQueue !== null){
                apiQueue = state.apiQueue.filter((queue)=> (queue.timestamp !== action.timestamp));
            }
            return {
                ...state,
                apiQueue
            };


        case loginActionTypes.REFRESH_FAILED:
        case loginActionTypes.LOGIN_FAIL:
        case drawerActionTypes.VERSION_UPDATED:
        case drawerActionTypes.RELOAD_DATA:
        case drawerActionTypes.LOGOUT:
            return initialState;
        default:
            return state;
    }
};

const calculateOrderPrice = (order) =>{
    let value = 0;
    order.items.forEach(item => {
        value += item.price * item.quantity;
    });
    return {...order, price: value}

};

const setItemDataWithQueue = (state, itemId, data, apiQueue) => {
    return {
        ...state,
        apiQueue,
        items: {
            ...state.items,
            [itemId]: {
                ...state.items[itemId],
                ...data
            }
        }
    }
};

const setOrderData = (state, orderId, data) => {
    return {
        ...state,
        orders: {
            ...state.orders,
            [orderId]: {
                ...state.orders[orderId],
                ...data
            }
        }
    }
};
