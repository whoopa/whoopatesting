import * as actionTypes from './actionTypes';
import API, { ExpiredTokenError } from '../../services/api';
import { Actions } from 'react-native-router-flux';

export const NEW = 0;
export const PROCESSING = 1;
export const SERVED = 2;

export const loadData = (password) => {
    return (dispatch) => {

        dispatch({ type: actionTypes.ORDERS_LOADING});

        API.instance().callSecure(password, 'orders?filters[complete]=0')
            .then(response => onLoad(dispatch, response))
            .catch(e => {onError(dispatch, null, e)});
    };
};

export const orderCompleted = (password, id, price, paidAtStage) => {
    return (dispatch) => {

        dispatch({ type: actionTypes.ORDER_UPDATING, id});
        API.instance().callSecure(password, `orders/${id}`, {complete: true, price, paidAtStage}, 'patch')
            .then(response => {dispatch({ type: actionTypes.ORDER_COMPLETE, id})})
            .catch(e => {onError(dispatch, id, e)});
    };
};

export const orderPaid = (password, id, paid, paidAtStage) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.ORDER_UPDATING, id});
        API.instance().callSecure(password, `orders/${id}`, {paid, paidAtStage}, 'patch')
            .then(response => {dispatch({ type: actionTypes.ORDER_PAID, id, paid: response.paid})})
            .catch(e => {onError(dispatch, id, e)});
    };
};

export const markItem = (password, id, itemId, itemState, timestamp) => {
    return (dispatch) => {
        dispatch({
            type: actionTypes.PROCESSING_ITEM_CALL,
            timestamp
        });
        let data = {status: itemState, patchTimestamp: timestamp};

        API.instance().callSecure(password, `orders/${id}/items/${itemId}`, data, 'patch')
            .then(response => markItemSuccess(dispatch, timestamp))
            .catch(e => {onItemError(dispatch, timestamp)});
    };
};

export const onLoad = (dispatch, response) => {
    dispatch({
        type: actionTypes.ORDERS_LOADED,
        payload: response
    });
};

export const itemSelected = (itemId) => {
    Actions.push('ordersMatchingItems');
    return {
        type: actionTypes.ORDER_ITEM_SELECTED,
        itemId
    };
};

export const toggleProcessing = (id, item) => {
    if(item.status === NEW){
        return setItemState(id,item.id,PROCESSING);
    }else{
        return setItemState(id,item.id,NEW);
    }
};

export const toggleServed = (id, item) => {
    if(item.status === PROCESSING){
        return setItemState(id,item.id,SERVED);
    }else{
        return setItemState(id,item.id,PROCESSING);
    }
};

export const onEdit = (id, item, isPaid) => {
    Actions.menu();
    Actions.push('menuSelectedItemOverlay');
    return{
        type: actionTypes.EDIT_ITEM,
        id,
        item,
        isPaid
    }
};

const setItemState = (id, itemId, itemState) => {
    const now = new Date();
    return {
        type: actionTypes.ADD_ITEM_CALL,
        id,
        itemId,
        itemState,
        timestamp: now.getTime()
    };
};


const markItemSuccess = (dispatch, timestamp) => {
    dispatch({
        type: actionTypes.REMOVE_ITEM_CALL,
        timestamp
    });
};

const onError = (dispatch, id, error) => {
    if(error instanceof ExpiredTokenError){
        console.log("Token Expired");
        //dispatch()
    }else{
        console.log(error);
        Actions.push('notificationOverlay', {message: error.toString(), button: 'OK'});
        if(id){
            dispatch({
                type: actionTypes.UPDATE_FAILED,
                id,
                payload: error
            });
        }else{
            dispatch({
                type: actionTypes.ORDER_LOADING_FAILED
            });
        }
    }
};

const onItemError = (dispatch, timestamp) => {
    console.log(error);
    Actions.push('notificationOverlay', {message: error.toString(), button: 'OK'});
    dispatch({
        type: actionTypes.UPDATE_ITEM_FAILED,timestamp
    });
};

export const onScrollReset = ()=>{
    return{
        type:actionTypes.SCROLL_RESET
    }
};