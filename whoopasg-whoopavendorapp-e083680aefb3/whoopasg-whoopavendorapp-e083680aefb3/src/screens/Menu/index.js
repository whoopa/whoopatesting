import React, { Component } from 'react';
import { View, StyleSheet, StatusBar, ToastAndroid } from 'react-native';
import { connect } from 'react-redux';
import {denormalize} from 'normalizr';
import {categorySchema} from "./reducer";
import { IconNav, Spinner, CategoryMenu, MenuItems } from '../../components'
import { loadData, selectCategory, selectItem, toggleSoldOut } from './actions'
import { Actions } from 'react-native-router-flux'

class Menu extends Component {


    componentWillMount() {
        console.log("Menu will Mount");
        if(this.props.simplifiedApp){
            Actions.simplifiedApp();
        }
    }

    componentDidMount(){
        console.log("Menu did Mount");
        if(this.props.simplifiedApp){
            Actions.simplifiedApp();
        }else{
            this.loadMenu();
        }
    }

    componentDidUpdate(){
        console.log("Menu did Update");
        this.loadMenu();
    }

    loadMenu(){
        const{categories, password, authenticated, loadData, loading} = this.props;
        if(authenticated && categories === null && password !== null && !loading){
            loadData(password);
        }
    }

    getContent(){
        let {categories, selectedCategory, selectCategory} = this.props;
        if(categories){
            return <IconNav
                items={Object.keys(categories).map(key => categories[key])}
                selected={selectedCategory}
                onPress={selectCategory}
            />
        }
        //return <Spinner />
    }

    renderMenuItems() {
      let { categories,items, selectItem, toggleSoldOut, password } = this.props;
        if(categories) {
            return (
                <CategoryMenu>
                    <MenuItems items={items} onPress={selectItem} onSoldOut={(id, soldOut) => toggleSoldOut(password, id, soldOut)}/>
                </CategoryMenu>
            );
        }
        return <View style={{flex:1, alignItems:'center', justifyContent: 'center'}}><Spinner /></View>
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar hidden />
                {this.getContent()}
                {this.renderMenuItems()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        backgroundColor: '#FFF'
    },
});

const mapStateToProps = (state) => {
    const { categories, menuItems, selectedCategory, loading } = state.menu;
    const { password, authenticated, simplifiedApp } = state.user;
    let items = null;
    if(selectedCategory){
        let denorm = denormalize(categories[selectedCategory], categorySchema, {categories, menuItems});
        items = denorm.menu_items;
    }
    return { categories, selectedCategory, items, password, authenticated, loading, simplifiedApp };
};


export default connect(mapStateToProps, {loadData, selectCategory, selectItem, toggleSoldOut })(Menu);

