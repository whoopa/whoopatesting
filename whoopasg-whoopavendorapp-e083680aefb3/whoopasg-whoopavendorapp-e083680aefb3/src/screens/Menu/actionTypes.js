export const LOADING = 'menu/loading';
export const FAILED = 'menu/failed';
export const LOADED = 'menu/loaded';

export const CATEGORY_SELECTED = 'menu/category';
export const ITEM_SELECTED = 'menu/item';
export const TOGGLE_SOLD_OUT = 'menu/item/sold_out/toggle';

export const QUANTITY_CHANGED = 'menu/quantity';
export const ITEM_ADDED = 'menu/add';
export const ITEM_REMOVED = 'menu/remove';
export const EDIT_ITEM = 'menu/edit';
export const EDIT_ITEM_DONE = 'menu/edit/done';
export const SIZE_SELECTED = 'menu/size';
export const TOGGLE_CUSTOMIZATION = 'menu/customization/toggle';
export const TOGGLE_TAKEAWAY = 'menu/order/takeaway/toggle';

export const KEYPAD_ENTER = 'menu/order/keypad/enter';
export const REMARKS_ADDED = 'menu/order/remarks/added';
export const ORDER_CANCEL = 'menu/order/cancel';

export const CONFIRMING_ORDER = 'menu/order/confirming';
export const ORDER_CONFIRMED = 'menu/order/confirm';
export const ORDER_UPDATING = 'menu/order/updating';
export const ORDER_UPDATED = 'menu/order/update';


