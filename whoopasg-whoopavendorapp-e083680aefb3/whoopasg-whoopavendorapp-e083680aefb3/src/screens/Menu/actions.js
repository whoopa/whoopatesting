import * as actionTypes from './actionTypes';
import { Actions } from 'react-native-router-flux';
import API from '../../services/api'

export const loadData = (password) => {
    return (dispatch) => {
        dispatch({type: actionTypes.LOADING});

        API.instance().callSecure(password, 'options')
            .then(response => onLoad(dispatch, response))
            .catch(e => {onError(dispatch, e)})
    };
};

export const onLoad = (dispatch, response) => {
    dispatch({
        type: actionTypes.LOADED,
        payload: response
    });
    dispatch(selectCategory(response.menu[0].id));
};

const onError = (dispatch, error) => {
    console.log(error);
    Actions.push('notificationOverlay', {message: error.toString(), button: 'OK'});
    dispatch({
        type: actionTypes.FAILED,
        payload: error
    });
};

export const selectCategory = (id) => {
    return {
        type: actionTypes.CATEGORY_SELECTED,
        payload: id
    };
};
export const selectItem = (id) => {
        Actions.push('menuSelectedItemOverlay',{});
        return {
            type: actionTypes.ITEM_SELECTED,
            payload: id
        };
};

export const selectSize = (id) => {
    return {
        type: actionTypes.SIZE_SELECTED,
        payload: id
    };
};

export const toggleCustomization = (customization, price, cid) => {
    return {
        type: actionTypes.TOGGLE_CUSTOMIZATION,
        customization,
        price,
        cid
    };
};

export const toggleTakeAway = () => {
    return {
        type: actionTypes.TOGGLE_TAKEAWAY
    };
};

export const changeQuantity = (qty) => {
    return {
        type: actionTypes.QUANTITY_CHANGED,
        payload: qty
    };
};
export const addToCart = (price) => {
    Actions.pop();Actions.push('menuOrdersOverlay');
    return (dispatch) => {
        dispatch({
            type: actionTypes.ITEM_ADDED,
            payload: price
        });
    }
};

export const removeFromCart = (itemIndex) => {
    return {
        type: actionTypes.ITEM_REMOVED,
        itemIndex: itemIndex
    };
};

export const confirmOrder = (password, items, orderNumber, table, paid, remarks, FCMToken) => {
    Actions.push('menuOrdersOverlay');
    return (dispatch) => {
        let data = {
            "complete": 0,
            "FCMToken": FCMToken,
            "items": items.map(item => ({
                "takeAway": item.take_away ? 1 : 0,
                "quantity": item.quantity,
                "price": item.price.toString(),
                "status": 0,
                "menuItem": item.itemId,
                "menuItemName": item.menu_item, // Extra Fields for Unsaved Orders
                "size": item.sizeId,
                "sizeName": item.size, // Extra Fields for Unsaved Orders
                "customizations": item.customizations ? item.customizations.map(customization => customization.id) : null,
                "customizationInfo": item.customizations ? item.customizations.map(customization => customization) : null // Extra Fields for Unsaved Orders
            })),
            "orderNumber": orderNumber,
            "tableNumber": table,
            'paid': paid,
            "remarks": remarks
        };
        if(paid !== 0){
            data["paidAtStage"] = 1; // "From Cart"
        }
        const uid = new Date().getTime();
        dispatch({type: actionTypes.CONFIRMING_ORDER, order: { ...data }, uid});

        //Delete Extra Fields before saving via API
        delete data["menuItemName"];
        delete data["sizeName"];
        delete data["customizationInfo"];
        saveOrder(password, dispatch, uid, data);
    };
};

const saveOrder = (password, dispatch, uid, data) => {
    API.instance().callSecure(password, 'orders', data, 'post')
        .then(response => onConfirmation(dispatch, response, uid))
        .catch(e => {
            console.log("Retrying after Save order failed: ", e);
            saveOrder(password, dispatch, uid, data)
        })
};

const onConfirmation = (dispatch, response, uid) => {
    dispatch({
        type: actionTypes.ORDER_CONFIRMED,
        payload: response,
        uid
    });
};


export const updateOrderItem = (password, currentOrder, price, isPaid, FCMToken) => {
    Actions.push('menuSelectedItemOverlay');
    return (dispatch) => {
        dispatch({type: actionTypes.ORDER_UPDATING});
        const data = {
            "patchTimestamp": new Date().getTime(),
            "FCMToken": FCMToken,
            "takeAway": currentOrder.takeAway ? 1 : 0,
            "quantity": currentOrder.quantity,
            "price": price,
            "menuItem": currentOrder.itemId,
            "size": currentOrder.size.id,
            "customizations": currentOrder.customizations ? currentOrder.customizations.map(customization => customization.id) : null,
            "isPaid": isPaid ? 1 : 0
        };
        API.instance().callSecure(password, `orders/${currentOrder.orderId}/items/${currentOrder.orderItemId}`, data, 'patch')
            .then(response => onUpdateConfirmation(dispatch, response))
            .catch(e => {onError(dispatch, e)})
    };
};
const onUpdateConfirmation = (dispatch, response) => {
    Actions.orders({clicked: false});
    console.log("onUpdateConfirmation",response);
    dispatch({
        type: actionTypes.ORDER_UPDATED,
        payload: response
    });
};

export const editCartItem = (itemIndex) => {
    Actions.pop();
    Actions.push('menuSelectedItemOverlay');
    return {
        type: actionTypes.EDIT_ITEM,
        itemIndex: itemIndex
    };
};

export const editedCartItem = (itemIndex, price) => {
    Actions.pop();
    Actions.push('menuOrdersOverlay');
    return {
        type: actionTypes.EDIT_ITEM_DONE,
        itemIndex,
        price
    };
};

export const keypadEnter = (tableNumber) => {
    Actions.pop();
    Actions.push('menuOrdersOverlay');
    return {
        type: actionTypes.KEYPAD_ENTER,
        tableNumber: tableNumber
    };
};

export const addRemarks = (remarkText) => {
    Actions.push('menuOrdersOverlay');
    return {
        type: actionTypes.REMARKS_ADDED,
        remarkText: remarkText
    };
};

export const cancelOrder = () => {
    return {
        type: actionTypes.ORDER_CANCEL,
    };
};


export const toggleSoldOut = (password, menuItemId, soldOut) => {
    const data = {
        menuItemId,
        soldOut: soldOut ? 1 : 0
    };
    API.instance().callSecure(password, 'options/item/markSoldOut', data, 'post')
        .then(response => console.log("toggleSoldOut", menuItemId, soldOut, response))
        .catch(e => console.log("ERROR in toggleSoldOut", menuItemId, soldOut, e));

    return {
        type: actionTypes.TOGGLE_SOLD_OUT,
        menuItemId,
        soldOut
    }
};