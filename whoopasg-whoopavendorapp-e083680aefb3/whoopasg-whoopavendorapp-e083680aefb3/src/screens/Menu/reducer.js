import * as actionTypes from './actionTypes';
import * as drawerActionTypes from '../Drawer/actionTypes';
import * as loginActionTypes from '../Login/actionTypes';
import * as orderActionTypes from '../Orders/actionTypes';
import update from 'immutability-helper';
import { normalize, schema, arrayOf } from 'normalizr';

const initialState = {
    hasTables: null,
    takeAwayPrice: null,
    categories: null,
    selectedCategory: null,
    selectedItem: null,
    menuItems: null,
    currentOrder: {
        orderId: null,
        orderItemId: null,
        orderIndex: null,
        itemId: null,
        quantity: 1,
        takeAway: false,
        size: null,
        customizations: null,
        processing: false,
        oldPrice: null,
        isPaid: null
    },
    orders: null,
    loading: false,
    error: false,
    table: null,
    remarks: null,
};

export const itemSchema = new schema.Entity('menuItems');
export const categorySchema = new schema.Entity('categories', {
    menu_items: [itemSchema]
});
const menuSchema = new schema.Array(categorySchema);

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOADING:
            return {
                ...state,
                loading: true
            };
        case actionTypes.LOADED:
            const normalizedData = normalize(action.payload.menu, menuSchema);
            const { categories, menuItems } = normalizedData.entities;
            return {
                ...state,
                categories, menuItems,
                hasTables:action.payload.hasTables,
                takeAwayPrice: parseFloat(action.payload.takeAwayPrice),
                loading: false
            };
        case actionTypes.FAILED:
            return {
                ...state,
                loading: false,
                processing: false,
                error: action.payload
            };
        case actionTypes.CATEGORY_SELECTED:
            return {
                ...state,
                selectedCategory: action.payload
            };
        case actionTypes.ITEM_SELECTED:
            let selectedItem = {...state.menuItems[action.payload]};
            return {
                ...state,
                selectedItem,
                currentOrder: {
                    ...initialState.currentOrder,
                    itemId: action.payload,
                    size: selectedItem.sizes[0],
                    customizations: []
                },
            };
        case actionTypes.ORDER_UPDATING:
            return {
                ...state,
                currentOrder: {
                    ...state.currentOrder,
                    processing: true,
                },
            };
        case actionTypes.EDIT_ITEM:
            let cartItem = state.orders[action.itemIndex];
            let newState = setSelectedItem(state, cartItem.itemId);
            if(newState.selectedItem === null){
                return state;
            }

            let size = newState.selectedItem.sizes.filter(size => size.name === cartItem.size).pop();
            let selectedCustomizations = cartItem.customizations.map(c=>(c));
            return {
                ...newState,
                currentOrder: {
                    orderId: null,
                    orderItemId: null,
                    orderIndex: action.itemIndex,
                    itemId: cartItem.itemId,
                    quantity: cartItem.quantity,
                    takeAway: (cartItem.take_away === 1),
                    size,
                    customizations:selectedCustomizations,
                    processing: false,
                },
            };
        case actionTypes.EDIT_ITEM_DONE:

            const updatedOrder = {
                table: state.table,
                itemId: state.selectedItem.id,
                quantity: state.currentOrder.quantity,
                take_away: state.currentOrder.takeAway ? 1 : 0,
                menu_item: state.selectedItem.name,
                sizeId: state.currentOrder.size.id,
                size: state.currentOrder.size.name,
                customizations: state.currentOrder.customizations.map(customization => ({...customization})), //Clone
                price: action.price
            };

            return{
                ...state,
                currentOrder: {
                    ...initialState.currentOrder,
                    customizations: []
                },
                orders: update(state.orders,{[action.itemIndex]: {$set: updatedOrder}})
            };
            return {
                ...state,
            };
        case orderActionTypes.EDIT_ITEM:
            newState = setSelectedItem(state, action.item.menu_item_id);

            if(!newState.selectedItem){
                return state;
            }
            size = newState.selectedItem.sizes.filter(size => size.name === action.item.size).pop();
            customizations = action.item.customizations.map(c=>{
                Object.keys(newState.selectedItem.customization_sets).map(cid=>{
                    let customizations =  newState.selectedItem.customization_sets[cid];
                    if(customizations.filter(cus=>(cus.id === c.id)).length > 0){
                        //if part of this customization set
                        c.cid = cid;
                    }
                });
                return c;
            });

            return {
                ...newState,
                currentOrder: {
                    orderId: action.id,
                    orderItemId: action.item.id,
                    orderIndex: null,
                    itemId: action.item.menu_item_id,
                    quantity: action.item.quantity,
                    takeAway: action.item.take_away,
                    size,
                    customizations,
                    processing: false,
                    isPaid: action.isPaid,
                    oldPrice: action.item.price,
                },
            };

        case actionTypes.SIZE_SELECTED:
            const selectedSize = state.selectedItem.sizes.filter(item => {
                return  item.id === action.payload;
            }).pop();
            return {
                ...state,
                currentOrder: {
                    ...state.currentOrder,
                    size: selectedSize
                },
            };

        case actionTypes.TOGGLE_CUSTOMIZATION:

            const cid = action.cid;
            const id = action.customization.id;

            let customizations = [];
            if(state.currentOrder.customizations){
                customizations =  state.currentOrder.customizations.map(c => (c)); //create a clone
            }
            //Deselect if selected
            let exists = customizations.filter(c => (c.id === id));
            if(exists.length > 0){
                return {
                    ...state,
                    currentOrder: {
                        ...state.currentOrder,
                        customizations: customizations.filter(customization => (customization.id !== id))
                    }
                }
            }

            //check if toggle exists
            //Remove existing for the same groups
            customizations = customizations.filter(c => (c.cid !== cid));

            //Add new one
            let new_customization = action.customization;
            new_customization.cid = cid;

            customizations.push(new_customization);

            return {
                ...state,
                currentOrder: {
                    ...state.currentOrder,
                    customizations
                }
            };

        case actionTypes.TOGGLE_TAKEAWAY:
            return {
                ...state,
                currentOrder: {
                    ...state.currentOrder,
                    takeAway: !state.currentOrder.takeAway
                }
            };

        case actionTypes.QUANTITY_CHANGED:
            return {
                ...state,
                currentOrder: {
                    ...state.currentOrder,
                    quantity: action.payload
                }
            };
        case actionTypes.ITEM_ADDED:
            const newOrder = {
                table: state.table,
                itemId: state.selectedItem.id,
                quantity: state.currentOrder.quantity,
                take_away: state.currentOrder.takeAway ? 1 : 0,
                menu_item: state.selectedItem.name,
                sizeId: state.currentOrder.size.id,
                size: state.currentOrder.size.name,
                customizations: state.currentOrder.customizations.map(customization => ({...customization})), //Clone
                price: action.payload
            };
            let orders = [];
            if(state.orders){
                orders = state.orders.map(o=>(o)); //create a clone
            }
            orders.push(newOrder);
            return{
                ...state,
                currentOrder: {
                    ...initialState.currentOrder,
                    customizations: []
                },
                orders
            };
        case actionTypes.ITEM_REMOVED:
            return{
                ...state,
                orders: update(state.orders, {$splice: [[action.itemIndex, 1]]})
            };
        case actionTypes.CONFIRMING_ORDER:
            return {
                ...state,
                orders: null,
                remarks: null,
                table: null
            };
        case actionTypes.KEYPAD_ENTER:
            return {
                ...state,
                table: action.tableNumber
            };
        case actionTypes.REMARKS_ADDED:
            return{
                ...state,
                remarks: action.remarkText
            };
        case actionTypes.ORDER_CANCEL:
            return{
                ...state,
                orders: null,
                remarks: null,
                table: null
            };
        case actionTypes.TOGGLE_SOLD_OUT:
            return{
                ...state,
                menuItems: {
                    ...state.menuItems,
                    [action.menuItemId]:{
                        ...state.menuItems[action.menuItemId],
                        sold_out: action.soldOut
                    }
                }
            };
        case loginActionTypes.REFRESH_FAILED:
        case loginActionTypes.LOGIN_FAIL:
        case drawerActionTypes.VERSION_UPDATED:
        case drawerActionTypes.RELOAD_DATA:
        case drawerActionTypes.LOGOUT:
            return {
                ...initialState
            };
        default:
            return state;
    }
};

function setSelectedItem(state, id){
    const categories = Object.keys(state.categories).filter(key => {
        const cat = state.categories[key];
        return cat.menu_items.includes(id);
    });
    return {
        ...state,
        selectedCategory: parseInt(categories[0]),
        selectedItem: {...state.menuItems[id]},
    };
}