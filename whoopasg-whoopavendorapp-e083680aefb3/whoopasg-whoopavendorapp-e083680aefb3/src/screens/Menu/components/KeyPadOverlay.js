import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import {KeyPad} from '../../../components/index';
import {keypadEnter} from "../actions";
import {View, StyleSheet, StatusBar, ScrollView, TouchableWithoutFeedback} from 'react-native';

class KeyPadOverlay extends Component {
    componentWillMount() {
    }

    render() {
        const {keypadEnter, table} = this.props;
        return (
            <TouchableWithoutFeedback onPress={Actions.pop}>
                <View style={styles.container}>
                    <StatusBar />
                    <TouchableWithoutFeedback>
                        <ScrollView horizontal vertical>
                            <View style={styles.keyPadContainer}>
                                <KeyPad onEnter={keypadEnter} initialValue={table}/>
                            </View>
                        </ScrollView>
                    </TouchableWithoutFeedback>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "rgba(0,0,0,0.9)",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    keyPadContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

const mapStateToProps = (state) => {
    const {table} = state.menu;
    return {table}
};

export default connect(mapStateToProps, {keypadEnter})(KeyPadOverlay);