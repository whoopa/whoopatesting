import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import numeral from 'numeral';
import {confirmOrder, removeFromCart, cancelOrder, editCartItem} from '../actions'
import I18n  from '../../../translations'
import {Button, OrderItemSummary, GestureHandler, Spinner, AppText }  from '../../../components'
import {View, StyleSheet, StatusBar, ScrollView, Animated, TouchableWithoutFeedback, Image} from 'react-native';
import update from 'immutability-helper';

class OrdersOverlay extends Component {

    constructor(props, context, updater) {
        super(props, context, updater);
    }


    componentWillMount(){
        console.log('componentWillMount');
        let pan = [];
        const {orders} = this.props;
        orders.map((order, index) => {
            pan.push(new Animated.Value(0));
        });
        this.setState({pan});
    }

    onOrderSwipe(gesture, index){
        if (Math.abs(gesture.dx) > 100) {
            this.props.removeFromCart(index);
            this.setState({
                pan: update(this.state.pan, {$splice: [[index,1]]})
            });
        } else {
            Animated.spring(
                this.state.pan[index],
                {toValue: 0}
            ).start();

        }
    }

    onOrderMove(gesture, index){
        this.state.pan[index] = new Animated.Value(gesture.dx);
        this.forceUpdate();
    }
    renderTableNumber() {
        const { table, hasTables } = this.props;
        if(hasTables){
            return (
                <TouchableWithoutFeedback onPress={() =>  Actions.push('keyPadOverlay') } >
                    <View style={styles.tableNumberContainerPadding}>
                    <View style={styles.tableNumberContainer}>
                        <AppText style={styles.tableNumberText}>{I18n.t('no')}: {table} </AppText>
                    </View>
                    </View>
                </TouchableWithoutFeedback>
            );
        }
        return null;
    }
    renderOrderNumber() {
        const { orderNumber } = this.props;
        if(orderNumber){
            return(
                <View>
                    <AppText style={[styles.flex1, styles.headerText]}>{orderNumber}</AppText>
                </View>
            );
        }
        return <Spinner/>
    }
    confirmationOverlay() {
        const {password, totalPrice, orders, confirmOrder, orderNumber, table, remarks, FCMToken, displayCalculator } = this.props;
        Actions.push('confirmationOverlay', {
            message: I18n.t('collect')+' '+ numeral(totalPrice).format('$0,0.00'),
            onConfirmation: ()=> confirmOrder(password, orders, orderNumber, table, totalPrice, remarks, FCMToken),
            confirmText: I18n.t('yes'),
            cancelText: I18n.t('no'),
            onCancel: ()=> confirmOrder(password, orders, orderNumber, table, 0, remarks, FCMToken),
            style: {top: 125, right: 20, position: 'absolute' },
            showCalculator: displayCalculator ? {price: totalPrice, style: {top: 75, right: 210, position: 'absolute' }} : false
        });
    }
    renderOrderHeader() {
        const {totalPrice, numDishes, orders } = this.props;
        if (orders && orders.length > 0) {
            return (
                <View key="OrderHeader">
                    <View style={styles.row}>
                        {this.renderOrderNumber()}
                        <AppText style={[styles.flex1, styles.headerText, styles.textRight]}>{numeral(totalPrice).format('$0,0.00')}</AppText>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.tableNumberFlex}>
                            {this.renderTableNumber()}
                        </View>
                        <Button style={styles.flex1} className="buttonFill"
                                onPress={()=> {this.confirmationOverlay()}}
                                title=""
                        >
                            <AppText style={styles.buttonText}>{I18n.t('confirm')}</AppText>
                            <View style={styles.dividerSmall}/>
                            <AppText style={styles.buttonTextSmall}>{I18n.t('numDishes')}
                                - {numeral(numDishes).format('00')}</AppText>
                        </Button>
                    </View>
                    <View style={styles.divider}/>
                </View>
            );
        }
        return <AppText style={styles.notice}>{I18n.t('noOrdersAdded')}</AppText>
    }

    renderOrders(pan) {
        const {orders, editCartItem} = this.props;
        if (orders && orders.length > 0) {
            return orders.map((order, index) => {
                let animatedStyle = {left: pan[index], backgroundColor:'#FFF'};
                return (
                    <View key={`Cart${index}${order.itemId}`}>
                        <View style={styles.removeBg}>
                            <Image source={require("../../../assets/images/remove.png")} />
                            <AppText style={styles.removeText}>{I18n.t('remove')}</AppText>
                            <Image source={require("../../../assets/images/remove.png")} />
                        </View>
                        <Animated.View
                            style={animatedStyle}>
                            <GestureHandler onSwipe={(gName, gState) => { this.onOrderSwipe(gState, index) }}
                                            onMove={(gName, gState) => { this.onOrderMove(gState, index) }}>
                                <OrderItemSummary item={order} onEdit={()=>{editCartItem(index)}}/>
                            </GestureHandler>
                        </Animated.View>
                        <View style={styles.dividerTop}/>
                    </View>
                )
            })
        }
        return null;
    }
    renderRemarks() {
        const { remarksContainer, remarksTitle, remarkTextColor, remarkPlaceholder, remarksContent } = styles;
        const { remarks, orders } = this.props;
        const hasRemarks = (remarks && remarks.length > 0);
        if(orders && orders.length > 0){
            return (
                <TouchableWithoutFeedback onPress={()=> Actions.push('remarksOverlay')}>
                    <View style={remarksContainer}>
                        <View style={{flex:1}}>
                            <AppText style={remarksTitle}>{I18n.t('remarks')}: </AppText>
                            <View style={remarksContent}>
                                <AppText style={hasRemarks ? remarkTextColor : remarkPlaceholder}>
                                    {remarks ? remarks : I18n.t('remarksPlaceholder')}
                                </AppText>
                            </View>
                            <View style={styles.dividerTop}/>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            );
        }
        return null;
    }
    orderCancellation() {
        const { cancelOrderText } = styles;
        const { orders } = this.props;
        if (orders && orders.length > 0) {
            return (
                <View>
                <View style={styles.dividerTop}/>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableWithoutFeedback onPress={this.promptConfirmation.bind(this)}>
                            <View style={{flex: 1}}>
                                <AppText style={cancelOrderText}>X {I18n.t('cancelOrderButton')}</AppText>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            );
        }
        return null;
    }
    promptConfirmation() {
        Actions.push('confirmationOverlay', {
            message: I18n.t('cancelOrderMsg'),
            confirmText: I18n.t('yes'),
            cancelText: I18n.t('no'),
            onConfirmation: this.props.cancelOrder,
            onCancel: ()=> {Actions.push("menuOrdersOverlay")},
            showCalculator: false,
            style: false
        });
    }
    render() {
        return (
            <View style={styles.container} onPress={Actions.pop}>
                <StatusBar />
                <TouchableWithoutFeedback onPress={Actions.pop}>
                    <View style={styles.filler}/>
                </TouchableWithoutFeedback>
                <View style={styles.scrollContainer}>
                    {this.renderOrderHeader()}
                    {this.renderRemarks()}
                    <ScrollView>
                        {this.renderOrders(this.state.pan)}
                    </ScrollView>
                        {this.orderCancellation()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    removeBg:{
        flexDirection: 'row',
        position: 'absolute',
        left:0,right:0,top:0,bottom:22,
        backgroundColor: '#FF3333',
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        padding: 10
    },
    removeText:{
        color: '#FFF',
        fontSize: 24,
        lineHeight: 24,
        flex: 1,
        textAlign: 'center'
    },
    filler: {
        alignSelf: 'stretch',
        flex: 1,
        backgroundColor: "rgba(0,0,0,0.9)",
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 0,
        paddingBottom: 5,
    },
    flex1: {
        flex: 1,
        padding: 5,
    },
    scrollContainer: {
        backgroundColor: '#FFF',
        padding: 20,
        paddingTop: 0,
        flex: 0,
        width: 300,
    },
    container: {
        flexDirection: 'row',
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
    },
    headerText: {
        fontWeight: 'bold',
        fontSize: 28,
        color: '#000'
    },
    buttonText: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#FFF',
        alignSelf: 'center'
    },
    buttonTextSmall: {
        fontWeight: 'bold',
        fontSize: 14,
        color: '#FFF',
        alignSelf: 'center'
    },
    divider: {
        height: 0,
        borderWidth: 1,
        marginTop: 10,
        marginBottom: 10,
    },
    dividerTop: {
        height: 0,
        borderWidth: 1,
        marginTop: 10,
        marginBottom: 10,
    },
    dividerSmall: {
        height: 0,
        borderBottomWidth: 1,
        margin: 3,
        borderColor: '#FFF',
    },
    textRight: {
        textAlign: 'right'
    },
    notice: {
        textAlign: 'center',
        padding: 10,
    },
    tableNumberContainer : {
        borderWidth:1,
        height:28,
        width:80,
        borderRadius:5,
        borderColor:'#d9d9d9',
        paddingLeft:15,
        paddingTop:2
    },
    tableNumberContainerPadding : {
        flex:0,
        padding:10,
        paddingBottom: 0,
        alignSelf:'flex-end',
    },
    tableNumberText: {
        color: '#000',
    },
    tableNumberFlex:{
        flexDirection: 'row',
        padding:0,
        flex: 1,
    },
    remarksContainer: {
       flexDirection: 'row',
    },
    remarksTitle: {
        color: '#000',
        fontWeight: 'bold'
    },
    remarkTextColor: {
        color: '#000'
    },
    remarkPlaceholder: {
        color: '#777',
        fontStyle: 'italic'
    },
    cancelOrderText: {
        color:'#FF6E6E',
        fontWeight: 'bold',
        fontSize: 15
    }

});
const mapStateToProps = (state) => {
    const { orders, table, hasTables, remarks } = state.menu;
    const { orderNumber, displayCalculator } = state.orderHistory;
    const { password, FCMToken } = state.user;

    let totalPrice = 0;
    let numDishes = 0;
    let processedOrders = [];
    if(orders){
        processedOrders = orders.map(item => {
            totalPrice += item.price * item.quantity;
            numDishes += item.quantity;
            return item;
        });
    }

    return { password, orders: processedOrders, totalPrice, numDishes, table, hasTables, remarks, orderNumber, FCMToken, displayCalculator }
};

export default connect(mapStateToProps, {confirmOrder, removeFromCart, cancelOrder, editCartItem})(OrdersOverlay);