import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { selectItem } from '../actions'
import   SelectedMenuItem  from './SelectedMenuItem'
import {View, Text, StyleSheet, StatusBar, ScrollView, TouchableWithoutFeedback} from 'react-native';
class SelectedMenuItemsOverlay extends Component {

    componentWillMount() {
    }
    renderSelectedMenuItem() {
        const { selectedItem } = this.props;
        if(selectedItem) {
            return <SelectedMenuItem selectedItem = {selectedItem} />
        }
    }
    render() {
        return (
            <TouchableWithoutFeedback onPress={() => {
                Actions.pop()
            }}>
            <View style={styles.container}>
                    <StatusBar />
                    <ScrollView horizontal vertical>
                        <View style={{flexDirection: 'column', flexWrap: 'wrap',height:300,}}>
                            { this.renderSelectedMenuItem() }
                        </View>
                    </ScrollView>
            </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    row: {
      flexDirection: 'row',
      alignSelf: 'stretch',
        alignItems: 'stretch',
        justifyContent: 'space-between'
    },
    container: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 60,
        right: 0,
        backgroundColor: "rgba(0,0,0,0.9)",
        justifyContent: "flex-start",
        alignItems: "flex-start",
    },
    title: {
        color: '#FFF',
        fontSize: 32,
        padding: 10,
    },
    cardContainer:{
        marginRight: 40,
        marginBottom: 20
    }

});
const mapStateToProps = (state) => {
    const {selectedItem} = state.menu;
    return { selectedItem };
};
export default connect(mapStateToProps, {selectItem})(SelectedMenuItemsOverlay);