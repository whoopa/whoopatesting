import React, {Component} from 'react';
import { connect } from 'react-redux';
import {addRemarks} from "../actions";
import {Remarks} from "../../../components";
import {View, StyleSheet, StatusBar, ScrollView} from 'react-native';

class RemarksOverlay extends Component {
    componentWillMount() {
    }
    render() {
        const { remarks,addRemarks } = this.props;
        const { container,remarksContainer } = styles;
        console.log('overlay props',this.props,addRemarks);
        return (
            <View style={container}>
                <StatusBar />
                <ScrollView horizontal vertical>
                    <View style={remarksContainer}>
                        <Remarks onSubmit={addRemarks} initialValue={remarks} />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "rgba(0,0,0,0.9)",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    remarksContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    });
const mapStateToProps = (state) => {
    const { remarks } = state.menu;
    return {remarks};
};
export default connect(mapStateToProps,{addRemarks})(RemarksOverlay);