import React from 'react';
import apiConfig from '../../../services/api/config';
import {connect} from 'react-redux';
import {selectSize, toggleCustomization, toggleTakeAway, changeQuantity, addToCart, updateOrderItem, editedCartItem} from "../actions";
import {View, TouchableOpacity, TouchableWithoutFeedback, StyleSheet} from 'react-native';
import {Toggle, ItemSize, MenuItemCustomization, Quantity, Spinner, AppText} from '../../../components';
import I18n from '../../../translations/index';
import numeral from 'numeral';
import {Actions} from 'react-native-router-flux';
import { CachedImage } from 'react-native-cached-image';

class SelectedMenuItem extends React.Component {

    renderButton(editMode, processing){
        if(editMode){
            if(processing){
                return (
                    <View style={styles.buttonLoading}>
                        <Spinner size="large"/>
                    </View>
                );
            }
            return <AppText style={styles.buttonText}>{I18n.t('edit')}</AppText>
        }
        return <AppText style={styles.buttonText}>{I18n.t('add')}</AppText>
    }

    renderItemSize(itemSizes) {
        let {selectSize, currentOrder} = this.props;
        return itemSizes.map((size, i) => {
            return (
                <View key={size.id} style={{flexDirection: 'row'}}>
                    <ItemSize id={size.id} name={size.name} price={size.price} onPress={() => selectSize(size.id)}
                              selectedSize={currentOrder.size}/>
                </View>
            );
        });
    }

    renderCustomization(itemCustomizations) {
        let {toggleCustomization, currentOrder} = this.props;

        //Sort by number of customizations first
        const sorted = Object.keys(itemCustomizations).sort((a,b) => (itemCustomizations[a].length > itemCustomizations[b].length));

        return sorted.map( cid => {
            const customizations = itemCustomizations[cid];
            const selected = (currentOrder.customizations ? currentOrder.customizations.map(c => c.id ) : null);
            return (
                <View key={`CustomizationSet${cid}`} style={customizations.length > 1 ? styles.optionsSet : styles.optionsSingle}>
                    <MenuItemCustomization
                        customizations={customizations}
                        selected={selected}
                        onPress={(customization, price) => toggleCustomization(customization, price, cid)}/>
                </View>
            );
        });
    }

    confirmationOverlay(e) {
        const {password, currentOrder, price, updateOrderItem, FCMToken } = this.props;
        console.log("Price Diff",currentOrder.oldPrice,price);
        if(currentOrder.oldPrice < price){
            Actions.push('confirmationOverlay', {
                message: I18n.t('collect')+' '+ numeral((price-currentOrder.oldPrice)).format('$0,0.00'),
                confirmText: I18n.t('yes'),
                cancelText: I18n.t('no'),
                onConfirmation: ()=> updateOrderItem(password, currentOrder, price, true, FCMToken),
                onCancel: ()=> updateOrderItem(password, currentOrder, price, false, FCMToken),
                showCalculator: false,
                style: {left: (e.nativeEvent.pageX - e.nativeEvent.locationX), top:(e.nativeEvent.pageY - e.nativeEvent.locationY), position: 'absolute' }
            });
        }else{
            updateOrderItem(password, currentOrder, price, false);
        }
    }

    render() {
        const {selectedItem, currentOrder, password, price, addToCart, toggleTakeAway, changeQuantity, editedCartItem, updateOrderItem, FCMToken} = this.props;
        let action = () => addToCart(price);

        const editMode = ((currentOrder.orderItemId !== null) || (currentOrder.orderIndex  !== null));
        if(editMode){
            if(currentOrder.orderItemId  !== null){
                //Edit from Orders Tab
                action = currentOrder.isPaid ? this.confirmationOverlay.bind(this) : (()=>{
                    updateOrderItem(password, currentOrder, price, false, FCMToken)
                });
            }else{
                //Edit from cart
                action = () => { editedCartItem(currentOrder.orderIndex, price) };
            }
        }

        let icon = require('../../../assets/images/defaultMenu.png');
        if(selectedItem.icon){
            icon = {uri: `${apiConfig.assetUrl}/${selectedItem.icon}`};
        }
        return (
                <View style={[styles.row]}>
                    {/*Add TouchableWithoutFeedback to prevent overlay tap detection*/}
                    <TouchableWithoutFeedback>
                        <View style={styles.thumbnailContainerStyle}>
                            <CachedImage source={icon} style={styles.iconImage}/>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback>
                        <View style={styles.container}>
                            <View style={styles.elevated}>
                                <View style={[styles.rowFill, {paddingBottom: 6}]}>
                                    <AppText style={styles.headerTitleTextStyle}>{selectedItem.name}</AppText>
                                    <View style={styles.buttonContainer}>
                                        <TouchableOpacity style={styles.buttonStyle}
                                                          onPress={action}>
                                            {this.renderButton(editMode, currentOrder.processing)}
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={[styles.rowFill]}>
                                    <AppText
                                        style={styles.headerPriceTextStyle}>{numeral(price * currentOrder.quantity).format('$0,0.00')}</AppText>
                                    <Toggle onLeft={!this.props.currentOrder.takeAway} leftText={ I18n.t('dineIn') }
                                            rightText={ I18n.t('takeAway')} onChange={toggleTakeAway}/>
                                </View>
                                <View style={styles.divider}/>
                                <View style={styles.rowFillCenter}>
                                    <View style={styles.row}>
                                        {this.renderItemSize(selectedItem.sizes)}
                                    </View>
                                    <Quantity maxValue="15" initialValue={currentOrder.quantity} onChange={changeQuantity}/>
                                </View>
                            </View>
                            <View style={styles.optionsContainer}>
                                <View style={styles.optionsDiv}>
                                    {this.renderCustomization(selectedItem.customization_sets)}
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
        )
    }
}
const styles = StyleSheet.create({
    iconImage: {
        width: 110,
        height: 110,
        alignSelf: 'center'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    rowFill: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    rowFillCenter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    thumbnailContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRightWidth: 0,
        padding: 5,
        paddingRight: 0,
        marginRight: 0,
        backgroundColor: '#FFF',
    },
    container: {
        backgroundColor: '#FFF',
        // width: 690,
        width: 650,
        elevation: 5,
    },
    headerTitleTextStyle: {
        paddingTop: 10,
        fontSize: 30,
        color: '#000',
        flexWrap:'wrap',
        flex:1,
    },
    headerPriceTextStyle: {
        fontSize: 20,
        paddingTop: 4.5,
        color: '#000'
    },
    buttonContainer: {
        flex: 1,
        paddingTop: 10,
    },
    buttonStyle: {
        backgroundColor: '#FF6E6E',
        borderRadius: 10,
        elevation: 5,
        alignSelf: 'flex-end',
    },
    buttonText: {
        fontSize: 35,
        alignSelf: 'center',
        padding: 30,
        paddingTop: 5,
        paddingBottom: 5,
        color: '#FFF',
        lineHeight: 48
    },
    buttonLoading: {
        alignSelf: 'center',
        padding: 40,
        paddingTop: 10,
        paddingBottom: 10,
        height: 58
    },
    divider: {
        height: 0,
        borderWidth: 1,
        marginBottom: 10,
        borderColor: 'rgba(0,0,0,0.2)',
    },
    elevated: {
        elevation: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
        backgroundColor: '#FFF'
    },
    optionsContainer: {
        flexDirection: 'row',
        padding: 20,
        paddingLeft: 10,
        flexGrow: 1,
        height: '100%',
        flexWrap: 'wrap'
    },
    optionsDiv: {
        height: 250,
        flexWrap: 'wrap',
    },
    optionsSingle: {
        margin: 5
    },
    optionsSet: {
        backgroundColor: '#FFF',
        margin: 5,
        // elevation:5
    }

});
const mapStateToProps = function(state){
    let {currentOrder, takeAwayPrice} = state.menu;
    let {password, FCMToken} = state.user;
    let price = currentOrder.size.price;
    if (currentOrder.customizations) {
        currentOrder.customizations.map(customization => {
            price += parseFloat(customization.price)
        });
    }
    if(currentOrder.takeAway){
        price += takeAwayPrice;
    }
    //Item prices should not be multiplied by quantity
    //price *= currentOrder.quantity;
    console.log("Pricing:", price, currentOrder.quantity);
    return {currentOrder, price, password, takeAwayPrice, FCMToken};
};
export default connect(mapStateToProps, {
    selectSize,
    toggleCustomization,
    toggleTakeAway,
    changeQuantity,
    addToCart,
    updateOrderItem,
    editedCartItem
})(SelectedMenuItem);