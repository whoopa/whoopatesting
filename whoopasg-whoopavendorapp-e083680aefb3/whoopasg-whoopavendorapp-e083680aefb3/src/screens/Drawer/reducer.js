import * as actionTypes from './actionTypes';
import * as simplifiedActionTypes from '../Simplified/actionTypes';
import * as menuActionTypes from '../Menu/actionTypes';
import {groupBy} from '../../utilities/ObjectUtils';

const initialState = {
    displayCalculator: true,
    selected: null,
    items: null,
    allItems: null,
    toggle: true,
    loading: false,
    orderNumber: null,
    orderNumberLoading: false,
    ordersUpdatedAt: false,
    dateRange: null,
    startDate: new Date(),
    endDate: new Date(),
    currentView: 'listing',
    allMenu: null,
    selectedMenu: null,
    allCustomizations: null,
    selectedCustomizations: null,
    tableHead:{
        menu_item:'Dish',
        quantity:'Qty',
        price: 'Price',
        time:'Time',
        update_timestamp:'Date',
        take_away:'Dine',
        vendor_order_number:'Queue'
    },
    selectedTableHead:{
        menu_item:'Dish',
        quantity:'Qty',
        price: 'Price',
        time:'Time',
        update_timestamp:'Date',
        take_away:'Dine',
        vendor_order_number:'Queue'
    }
};


export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TOGGLE_CALCULATOR_DISPLAY:
            return {
                ...state,
                displayCalculator: !state.displayCalculator
            };
        case actionTypes.ORDER_HISTORY_LODAING:
            return {
                ...state,
                items: [],
                loading: true
            };
        case actionTypes.ORDER_HISTORY:
            let data = [];
            let toggle;
            let menuData = [];
            let customizationsData = [];
            if(action.payload){
                action.payload.map(order => {
                    order.items.map(item => {
                        item.update_timestamp = order.update_timestamp;
                        data.push(item);
                    });
                });
            }
            if(data){
                data.map((menu) => {
                    menu.customizations.map((customization) => {
                       customizationsData.push(customization.customization);
                    });
                    menuData.push({
                        'menu_item': menu.menu_item,
                        'customizations': menu.customizations,
                    });
                });
                if(customizationsData.length > 0){
                    customizationsData = groupBy(customizationsData, 'name')
                }else{
                    customizationsData = null;
                }
                if(menuData.length > 0){
                    menuData = groupBy(menuData, 'menu_item');
                }else{
                    menuData = null;
                }
            }
            return {
                ...state,
                loading: false,
                items: data,
                allItems: data,
                selected: null,
                toggle: true,
                allMenu : menuData,
                selectedMenu: menuData,
                allCustomizations: customizationsData,
                // selectedCustomizations: customizationsData,
            };
        case actionTypes.ORDER_SORT:
            let itemData = [];
            state.items.map(item => {
                itemData.push(item);
            });
            let column = action.payload;
            if(state.toggle){
                if(state.selected === null){
                    toggle = false;
                }
                toggle = (state.selected !== action.payload);
            }
            if(!state.toggle){
                toggle = true;
            }
            itemData = itemData.sort((a,b)=>{
                if(toggle){
                    return a[column] > b[column] ? 1 : -1;
                }else{
                    return a[column] < b[column] ? 1 : -1;
                }
            });
            return {
                ...state,
                items:itemData,
                selected: action.payload,
                toggle: toggle
            };
        case actionTypes.CLOSE_CALENDAR_OVERLAY:
            const {dateRange, startDate, endDate } = action.payload;
            return {
                ...state,
                dateRange: dateRange? dateRange : state.dateRange,
                startDate: startDate? startDate : state.startDate,
                endDate: endDate? endDate : state.endDate,
            };
        case actionTypes.CHANGE_VIEW:
            return {
                ...state,
                currentView : action.payload
            };

        case actionTypes.APPLY_FILTERS:
            let filterItems = [];
            state.allItems.map((item)=>{
                if(action.payload.selectedCustomizations){
                    const selectedCustomizationNames = Object.keys(action.payload.selectedCustomizations);
                    if(item.customizations.length > 0){
                        const matching = item.customizations.filter(orderCustomization => (selectedCustomizationNames.includes(orderCustomization.customization.name)));
                        if(matching.length === selectedCustomizationNames.length){
                            filterItems.push(item);
                        }
                    }
                }else{
                    if(action.payload.selectedMenu.hasOwnProperty(item.menu_item)){
                        filterItems.push(item);
                    }
                }
            });
            return {
                ...state,
                items: filterItems,
                selectedMenu: action.payload.selectedMenu,
                allCustomizations: action.payload.allCustomizations,
                selectedCustomizations: action.payload.selectedCustomizations
            };
        case actionTypes.ORDER_NUMBER_LOADING:
            return {
                ...state,
                orderNumberLoading: true
            };
        case actionTypes.ORDER_NUMBER_LOADED:
            return {
                ...state,
                orderNumber: action.payload,
                orderNumberLoading: false
            };
        case actionTypes.ORDER_NUMBER_FAILED:
            return {
                ...state,
                orderNumberLoading: false
            };
        case menuActionTypes.CONFIRMING_ORDER:
        case simplifiedActionTypes.CONFIRMING_ORDER:
            return {
                ...state,
                orderNumber: null
            };
        case actionTypes.LOGOUT:
            return initialState;
        default:
            return state;
    }
};