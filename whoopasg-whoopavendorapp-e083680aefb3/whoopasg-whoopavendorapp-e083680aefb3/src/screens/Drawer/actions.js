import * as actionTypes from './actionTypes';
import * as menuActionTypes from '../Menu/actionTypes'
import * as orderActionTypes from '../Orders/actionTypes'
import * as simplifiedActionTypes from '../Simplified/actionTypes'
import API from '../../services/api'
import {onLoad as menuOnLoad} from '../Menu/actions'
import {onLoad as ordersOnLoad} from '../Orders/actions'
import { Actions } from 'react-native-router-flux'
import moment from 'moment';

let timeout = null;
export const logout = (password) => {
    return (dispatch) => {
        dispatch({type: actionTypes.LOGOUT});
        API.instance().callSecure(password, 'analytics/vendors', {action: 2}, "post")
            .then(response => {console.log("Logged out successfully")})
            .catch(e => {console.log(e)});
    };
};

export const versionUpdated = () => {
    return {type: actionTypes.VERSION_UPDATED};
};

export const reloadData = (password) => {
    return (dispatch) => {
        dispatch({type: actionTypes.RELOAD_DATA});
        dispatch({type: menuActionTypes.LOADING});
        API.instance().callSecure(password, 'options')
            .then(response => menuOnLoad(dispatch, response))
            .catch(onError);

        dispatch({type: orderActionTypes.ORDERS_LOADING});
        API.instance().callSecure(password, 'orders?filters[complete]=0')
            .then(response => ordersOnLoad(dispatch, response))
            .catch(onError);
    };
};

export const putOffline = (password) => {
    return (dispatch) => {
        dispatch({type: actionTypes.OFFLINE});
        API.instance().callSecure(password, 'analytics/vendors', {action: 3}, "post")
            .then(response => {console.log("Logged as offline")})
            .catch(e => {console.log(e)});
    };
};
export const putOnline = (password) => {
    return (dispatch) => {
        API.instance().callSecure(password, 'analytics/vendors', {action: 4}, "post")
            .then(response => {console.log("Logged as online")})
            .catch(e => {console.log(e)});
        Actions.pop();
    };
};

export const reportLocation = (password, location, uniqueId) => {
    return (dispatch) => {
        API.instance().callSecure(password, 'analytics/vendors', {action: 5, data:JSON.stringify({location, device:{uniqueId: uniqueId}})}, "post")
            .then(response => {console.log("Logged location successfully")})
            .catch(e => {console.log(e)});
    };
};
const onError = (e) => {
    console.log(e);
    Actions.push('notificationOverlay', {message: e.toString(), button: 'OK'})
};

export const getOrderHistory = (password)=>{
    return getOrderFilter(password, new Date(), new Date());
};
export const getOrderFilter = (password,startDate,endDate)=>{
    const data = [];
    const start = moment(startDate).format("YYYY-MM-DD");
    const end = moment(endDate).format("YYYY-MM-DD");
    data['filters'] = [];
    data['filters']['createdBetween']="2017-10-01,2017-10-07";
    return (dispatch) => {
        dispatch({
            type: actionTypes.ORDER_HISTORY_LODAING,
        });
        API.instance().callSecure(password, 'orders?filters[createdBetween]='+start+','+end+'')
            .then(response => {
                dispatch({
                    type: actionTypes.ORDER_HISTORY,
                    payload: response
                })
            })
            .catch(onError);
    };
};

export const fetchOrder = (password, orderId) => {
    console.log("Fetching Order: ", orderId);
    if(timeout){
        clearTimeout(timeout);
        timeout = null;
    }
    return (dispatch) => {
        API.instance().callSecure(password, `orders/${orderId}`)
            .then(response => onOrderFetch(dispatch, response))
            .catch(e => {
                console.log(e);
                Actions.push('notificationOverlay', {message: `Error fetching order (orderId): ${e.message}.\nRetrying in 5 seconds.`, button: 'OK'});
                //Retry to get order in 5 seconds
                timeout = setTimeout(function(){fetchOrder(password, orderId);}, 5000);
            })
    };
};

const onOrderFetch = (dispatch, response) => {
    dispatch({
        type: actionTypes.ORDER_RECEIVED,
        payload: response
    });
};


export const getOrderNumber = (password) => {
    return (dispatch) => {
        dispatch({
            type: actionTypes.ORDER_NUMBER_LOADING,
        });
        API.instance().callSecure(password, 'order/number')
            .then(response => orderNumberLoaded(dispatch,response))
            .catch(e => {orderNumberFailed(dispatch, e)})
    };
};


const orderNumberLoaded = (dispatch, response) => {
    dispatch({
        type: actionTypes.ORDER_NUMBER_LOADED,
        payload: response
    });
};
const orderNumberFailed = (dispatch, e) => {
    dispatch({
        type: actionTypes.ORDER_NUMBER_FAILED
    });
};


export const sortOrder = (name) => {
    return {
        type: actionTypes.ORDER_SORT,
        payload: name
    };
};
export const orderTabClicked = () => {
    return {
        type: actionTypes.ORDER_TAB_CLICKED,
    };
};

export const closeCalendarOverlay = (dateRange,startDate,endDate) => {
    Actions.pop();
    return {
        type: actionTypes.CLOSE_CALENDAR_OVERLAY,
        payload:{
            'dateRange': dateRange ? dateRange : null,
            'startDate': startDate,
            'endDate': endDate,
        }
    };
};

export const fcmTokenLoaded = (fcmToken, password, uniqueId) => {
    return (dispatch) => {
        dispatch({
            type: actionTypes.FCM_TOKEN_LOADED,
            fcmToken,
        });
        API.instance().callSecure(password, 'analytics/device', {uniqueId, fcmToken}, 'patch')
            .then(response => console.log("Device Updated", response))
            .catch(e => console.error("Device Update Failed", e))
    };
};
export const fcmTokenLoading = () => {
    return {
        type: actionTypes.FCM_TOKEN_LOADING
    };
};
export const toggleCalculatorDisplay = () => {
    return {
        type: actionTypes.TOGGLE_CALCULATOR_DISPLAY
    };
};


export const resetFCM = () => {
    return {
        type: actionTypes.RESET_FCM
    };
};

export const changeView = (view) => {
    return {
        type: actionTypes.CHANGE_VIEW,
        payload: view
    };
};

export const applyFilters = (selectedMenu,selectedCustomizations,allCustomizations) => {
    Actions.pop();
    return {
        type: actionTypes.APPLY_FILTERS,
        payload:{
            'selectedMenu': selectedMenu,
            'selectedCustomizations': selectedCustomizations,
            'allCustomizations': allCustomizations
        }
    };
};

