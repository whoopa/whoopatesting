import React, {Component} from 'react';
import {StyleSheet, View, ToastAndroid} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import DeviceInfo from 'react-native-device-info'
import {
    logout,
    putOffline,
    reloadData,
    reportLocation,
    versionUpdated,
    putOnline,
    fcmTokenLoading,
    fcmTokenLoaded,
    resetFCM,
    fetchOrder,
    getOrderNumber,
    toggleCalculatorDisplay
} from './actions'
import {TextNav, GeoLocationWatch, Toggle, AppText} from '../../components'
import I18n from '../../translations';
import {APP_VERSION} from '../Login/reducer';
import FCM, {FCMEvent} from 'react-native-fcm';

class Drawer extends Component {

    state = {
      ordersBeingLoaded: []
    };

    componentWillMount() {
        this.notificationListener = null;
    }

    componentDidMount() {
        this.check();
    }

    componentDidUpdate() {
        this.check();
    }

    componentWillUnmount() {
        console.log("Menu will UnMount");
        // stop listening for events
        if (this.props.FCMTopic) {
            console.log("unsubscribeFromTopic");
            FCM.unsubscribeFromTopic(this.props.FCMTopic);
        }
        if (this.notificationListener) {
            this.notificationListener.remove();
            this.notificationListener = null;
        }

        this.props.resetFCM();
    }

    check() {
        const {isLoggedIn, version, versionUpdated, orderNumberLoading, orderNumber, password,
            fcmTokenLoading, fcmTokenLoaded, FCMLoading, FCMToken, multiDevice} = this.props;
        if (version !== APP_VERSION) {
            versionUpdated();
        } else {
            if (!isLoggedIn) {
                console.log("Drawer > Login");
                Actions.reset('auth');
            }else{
                this.listenForNotifications();
                if (multiDevice && !FCMLoading && !FCMToken) {
                    fcmTokenLoading();
                    FCM.getFCMToken().then(token => {
                        console.log('getFCMToken', token);
                        navigator.geolocation.getCurrentPosition(
                            this.sendLocationInfo.bind(this),
                            (error)=>{console.log(error.message)},
                            {enableHighAccuracy: false, timeout: 25000, maximumAge: 60000}
                        );
                        fcmTokenLoaded(token, password, DeviceInfo.getUniqueID());
                        // store fcm token in your server
                    });
                }
                if(orderNumber === null && !orderNumberLoading){
                    this.props.getOrderNumber(password);
                }
            }
        }

    }

    listenForNotifications() {
        if (this.props.multiDevice && this.props.FCMTopic && this.notificationListener === null) {
            console.log("Subscribing to ", this.props.FCMTopic);
            FCM.subscribeToTopic(this.props.FCMTopic);
            this.notificationListener = FCM.on(FCMEvent.Notification, this.handleNotification.bind(this));

            // initial notification contains the notification that launchs the app. If user launchs app by clicking banner, the banner notification info will be here rather than through FCM.on event
            // sometimes Android kills activity when app goes to background, and when resume it broadcasts notification before JS is run. You can use FCM.getInitialNotification() to capture those missed events.
            FCM.getInitialNotification().then(notif => {
                console.log('Init Notif', notif);
            });
        }
    }

    async handleNotification(notif) {
        console.log("Got Notification", notif);
        const {FCMToken, fetchOrder, password} = this.props;

        const createdByToken = notif.createdByDevice;
        const orderId = notif.orderId;
        const action = notif.action;
        //Only process if this param is found
        if (createdByToken) {
            //Only process if not created by this device
            if (createdByToken !== FCMToken) {
                console.log("Token did not match");
                this.loadOrder(orderId);
            }
        } else if (orderId) {
            // Special case handling if FCM token not received for some reason.
            // Process Order Anyway.
            this.loadOrder(orderId);
        } else if (action) {
            switch (action) {
                case 'location':
                    navigator.geolocation.getCurrentPosition(this.sendLocationInfo.bind(this));
                    break;
            }
        }
    }

    loadOrder(orderId){
        const {fetchOrder, password} = this.props;
        //fetch the updated order
        if(!this.state.ordersBeingLoaded.includes(orderId)){
            this.setState({ordersBeingLoaded: [...this.state.ordersBeingLoaded, orderId]});
            ToastAndroid.show(I18n.t('orderReceived'), ToastAndroid.SHORT);
            fetchOrder(password, orderId);
        }
    }

    sendLocationInfo(position){
        const {password, reportLocation} = this.props;

        const {latitude, longitude} = position.coords;

        reportLocation(password, {latitude, longitude}, DeviceInfo.getUniqueID());
    }

    render() {
        const {logout, putOffline, reloadData, password, processing, putOnline, simplifiedApp, displayCalculator,
            toggleCalculatorDisplay, unsaved} = this.props;
        return (
            <View style={[styles.viewContainer, this.props.sceneStyle]}>
                <GeoLocationWatch onLocation={this.sendLocationInfo.bind(this)}/>
                <View style={{flexDirection: 'column', borderBottomWidth: 0, flex: 1}}>
                    {simplifiedApp ? null :
                        <TextNav items={[{name: I18n.t('cart'), id: -1}]}
                                 onPress={() => {
                                     Actions.pop();
                                     Actions.push('menuOrdersOverlay');
                                 }}/>
                    }
                    <TextNav items={[{name: I18n.t('orderReport'), id: -1}]} onPress={() => {
                        Actions.push('orderReport')
                    }}/>
                    <TextNav items={[{name: I18n.t('putOfflineButton'), id: 1}]}
                             onPress={() => {
                                 putOffline(password);
                                 Actions.pop();
                                 Actions.push('offlineOverlay', {
                                     message: I18n.t('offlineMessage'),
                                     button: I18n.t('putOnlineButton'),
                                     action: () => {
                                         putOnline(password)
                                     },
                                     opaque: true,
                                     processing: processing
                                 });
                             }}/>
                    <TextNav items={[{name: I18n.t('logoutButton'), id: 2}]}
                             onPress={() => {
                                 if(unsaved && Object.keys(unsaved).length > 0){
                                     Actions.push('confirmationOverlay', {
                                         message: I18n.t('pendingOrders'),
                                         confirmText: I18n.t('no'),
                                         cancelText: I18n.t('logoutButton'),
                                         onConfirmation: () => {
                                         },
                                         onCancel: () => {
                                             logout(password)
                                         },
                                         showCalculator: false,
                                         style: { justifyContent: 'center' },
                                     });
                                 }else{
                                     logout(password)
                                 }
                             }}/>
                    <View style={styles.row}>
                        <AppText>{I18n.t('changeCalculatorTitle')}</AppText>
                        <Toggle onLeft={!displayCalculator}
                                leftText={I18n.t('no')} rightText={I18n.t('yes')}
                                onChange={toggleCalculatorDisplay}
                                noElevation
                        />
                    </View>
                    {/*<TextNav items={[{name: 'Reload', id: 3}]} onPress={()=>{reloadData(password)}} />*/}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    notice: {
        padding: 10,
        margin: 5,
        borderRadius: 5,
        backgroundColor: '#A66',
    },
    noticeText: {
        color: '#FFF',
        textAlign: 'center'
    },
    viewContainer: {
        flex: 1,
        backgroundColor: '#fff'
    },
    container: {
        padding: 15,
        height: 45,
        overflow: 'hidden',
        alignSelf: 'flex-start',
    },
    textStyle: {
        fontSize: 18,
        color: '#555',
    },
    nameContainer: {
        padding: 15,
        height: 45,
        overflow: 'hidden',
        alignSelf: 'flex-start',
    },
    name: {
        fontSize: 22,
        color: '#555',
        fontWeight: '400',
    },
    row:{
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 10,
        borderBottomColor: '#777',
        borderBottomWidth: 1,
    }
});


function mapStateToProps(state) {
    const {orderNumberLoading, orderNumber, displayCalculator} = state.orderHistory;
    return {
        FCMTopic: state.user.FCMTopic,
        FCMToken: state.user.FCMToken,
        FCMLoading: state.user.FCMLoading,
        password: state.user.password,
        version: state.user.version,
        processing: state.user.processing,
        simplifiedApp: state.user.simplifiedApp,
        multiDevice: state.user.multiDevice,
        isLoggedIn: (state.user.authenticated === true),
        unsaved: state.user.simplifiedApp ? state.simplified.unsaved : state.kitchen.unsaved,
        orderNumberLoading, orderNumber, displayCalculator
    }
}

export default connect(
    mapStateToProps,
    {
        logout,
        putOffline,
        reloadData,
        reportLocation,
        versionUpdated,
        putOnline,
        fcmTokenLoading,
        fcmTokenLoaded,
        resetFCM,
        fetchOrder,
        getOrderNumber,
        toggleCalculatorDisplay
    })(Drawer);