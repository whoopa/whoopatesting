import React from 'react';
import {View, Text, TouchableOpacity, ScrollView, StyleSheet, Image, TouchableWithoutFeedback} from 'react-native';
import {Table, Rows,Row} from 'react-native-table-component';
import {connect} from 'react-redux';
import moment from 'moment';
import I18n from '../../../translations'
import {getOrderHistory, getOrderFilter} from '../../Drawer/actions'
import {groupBy} from '../../../utilities/ObjectUtils';
import {Spinner,ChartReport,AppText} from "../../../components";
import { Actions } from 'react-native-router-flux';

class OrderReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            today: moment(new Date()).format('Do MMM, YYYY'),
            isLoading: true,
        };
    }

    componentWillMount() {
        const {password, items,toggleArrow } = this.props;
        if (password && items === null) {
            this.props.getOrderHistory(password);
        }
        if(toggleArrow) {
            Actions.push('calendarOverlay');
        }
    }
    renderToday() {
        const { today } = this.state;
        const { dateRange } = this.props;
        if (dateRange !== null) {
            return dateRange;
        }
        return today;
    }

    renderTableData() {
        const { tableDataWrapper, tableFirstColumn, tableColumns } = styles;
        const { items } = this.props;
        if (items && items.length > 0) {
            return this.props.items.map(item => {
                let customizationNames = [];
                item.customizations.map((customize) => {
                    customizationNames.push(customize.customization.name);
                });
                if((customizationNames.length > 0)){
                    customizationNames = " ("+customizationNames.join(', ')+")";
                }else{
                    customizationNames = '';
                }
                return (
                    <View key={item.id} style={tableDataWrapper}>
                        <View style={tableFirstColumn}>
                            <AppText style={{fontWeight: 'bold'}}>{item.menu_item + customizationNames}</AppText>
                        </View>
                        <View style={tableColumns}>
                            <AppText>{item.quantity}</AppText>
                        </View>
                        <View style={tableColumns}>
                            <AppText>{item.price}</AppText>
                        </View>
                        <View style={tableColumns}>
                            <AppText>{moment(item.update_timestamp * 1000).format("LT")}</AppText>
                        </View>
                        <View style={tableColumns}>
                            <AppText>{moment(item.update_timestamp * 1000).format("Do MMM")}</AppText>
                        </View>
                        <View style={tableColumns}>
                            <AppText>{item.take_away ? 'Out' : 'In'}</AppText>
                        </View>
                        <View style={tableColumns}>
                            <AppText>{item.vendor_order_number}</AppText>
                        </View>
                    </View>
                );
            });
        }
        return <View style={{alignSelf: 'center'}}><AppText>{I18n.t('noData')}</AppText></View>;
    }

    renderSummary(sumData, cat) {
        if (sumData && sumData.length > 0) {
            sumData.sort(function (a, b) {
                return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0);
            });
            return sumData.map((data, i) => {
                return (
                    <AppText key={i}>{data.name}: {cat === 'count' ? '$' + data.price.toFixed(2) : data.quantity}</AppText>
                );
            });
        }
        return null;
    }
    renderView(){
        const { currentView, items, startDate, endDate, selectedMenu } = this.props;
            switch(currentView) {
                case 'listing':
                    return this.renderTableData();
                case 'graph':
                    return <ChartReport items={items} startDate={startDate} endDate={endDate} selectedMenu={selectedMenu} />;
                default:
                    return this.renderTableData();
            }

    }
    render() {
        const { items, loading } = this.props;
        let totalPrice = 0;
        let quantity = 0;
        let itemData = [];
        let sumData = [];

        if (items && items.length > 0) {
            items.map((item) => {
                totalPrice = totalPrice += item.price * item.quantity;
                quantity = quantity += item.quantity;
                itemData.push({
                    'menu_item': item.menu_item,
                    'price': item.price,
                    'quantity': item.quantity
                });
            });
            itemData = groupBy(itemData, 'menu_item');
            if (itemData) {
                Object.keys(itemData).map(key => {
                    sumData.push(
                        {
                            'name': key,
                            'price': itemData[key].reduce((prevVal, elem) => prevVal + elem.price, 0),
                            'quantity': itemData[key].reduce((prevVal, elem) => prevVal + elem.quantity, 0)
                        }
                    );
                });
            }
        }

        return (
            <View style={styles.scrollContainer}>
                <View style={styles.containerLeft}>
                        <View style={[styles.dateField]}>
                            <TouchableOpacity onPress={ () => Actions.push('calendarOverlay') }>
                                <View style={{height: 18, flexDirection: 'row'}}>
                                    <View style={styles.fieldDate}>
                                        <AppText style={[styles.dateFieldText, {alignSelf: 'flex-start'}]}>
                                            {this.renderToday()}
                                        </AppText>
                                    </View>
                                    <View style={styles.fieldArrow}>
                                        <AppText style={[styles.dateFieldText, {alignSelf: 'flex-end'}]}><Image
                                            source={require("../../../assets/images/arrow-right.png")}/></AppText>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>
                    <View style={styles.salesReportContainer}>
                        <View style={styles.salesReportHeader}>
                            <AppText style={styles.salesReportCountText}>${totalPrice.toFixed(2)}</AppText>
                            <AppText>{I18n.t('salesCount')}</AppText>
                        </View>
                        <View style={styles.salesReportBody}>
                        <ScrollView style={{flex:1}}>
                            {this.renderSummary(sumData, 'count')}
                        </ScrollView>
                        </View>
                    </View>
                    <View style={styles.salesReportContainer}>
                        <View style={styles.salesReportHeader}>
                            <AppText style={styles.salesReportCountText}>{quantity}</AppText>
                            <AppText>{I18n.t('salesVolume')}</AppText>
                        </View>
                        <View style={styles.salesReportBody}>
                            <ScrollView>
                                {this.renderSummary(sumData, 'volume')}
                            </ScrollView>
                        </View>
                    </View>
                </View>
                <View style={[styles.containerRight]}>
                    <ScrollView>
                        {loading ? <Spinner/> : this.renderView()}
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollContainer: {
        backgroundColor: '#FFF',
        paddingTop: 0,
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'row'

    },
    containerLeft: {
        height: '100%',
        width: 300,
        padding: 15,
    },
    containerRight: {
        borderLeftColor: '#FF6E6E',
        height: '100%',
        flex: 1,
        borderLeftWidth: 1,
        paddingLeft:40,paddingTop:10,paddingBottom:10
    },
    dateField: {
        borderBottomWidth: 1,
        width: '100%',
        borderBottomColor: '#FF6E6E',
        flexDirection: 'row',
        flex: 0,
        height: 30,
        // position:'relative'
    },
    dateFieldText: {
        fontSize: 18,
        color: '#FF6E6E'
    },
    fieldDate: {
        width: 230,
        justifyContent: 'center'
    },
    fieldArrow: {
        width: 40,
        justifyContent: 'center'
    },
    container1: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        marginTop: 100,
    },
    salesReportContainer: {
        flexDirection: 'column',
        width: '100%',
        flexGrow: 1,
        flex:1
    },
    salesReportHeader: {
        height: 100,
        width: '100%',
        borderBottomColor: '#FF6E6E',
        borderBottomWidth: 1
    },
    salesReportCountText: {
        fontSize: 55,
        color: '#FF6E6E'
    },
    salesReportBody: {
        // height:140,
        width: '100%',
        paddingTop: 5,
        flex:1
    },
    tableDataWrapper: {
        flexDirection:'row',
        padding:10
    },
    tableFirstColumn: {
        flex:3,
        flexWrap:'wrap'
    },
    tableColumns: {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
});
const mapStateToProps = (state) => {
    const {password} = state.user;
    const {items, loading, columnSelection, selectedTableHead, dateRange, currentView, startDate, endDate,selectedMenu } = state.orderHistory;
    return { password, items, loading, columnSelection, selectedTableHead, dateRange, currentView, startDate, endDate,selectedMenu};
};
export default connect(mapStateToProps, {getOrderHistory, getOrderFilter})(OrderReport);