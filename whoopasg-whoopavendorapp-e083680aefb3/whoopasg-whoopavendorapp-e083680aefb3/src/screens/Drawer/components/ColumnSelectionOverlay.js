import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, StyleSheet,TouchableOpacity,TouchableWithoutFeedback,Image,ScrollView} from 'react-native';
import {applyFilters} from '../actions';
import { Actions } from 'react-native-router-flux';
import {groupBy} from "../../../utilities/ObjectUtils";
import {AppText} from '../../../components'
import I18n from '../../../translations';

class ColumnSelectionOverlay extends Component {
    state = {
        selectedTableHead: null,
        selectedMenu: null,
        selectedCustomizations : null,
        allCustomizations: null,
    };
    componentWillMount(){
        const { selectedTableHead,selectedMenu,selectedCustomizations,allCustomizations } = this.props;
        this.setState({selectedTableHead});
        this.setState({selectedMenu});
        this.setState({selectedCustomizations});
        this.setState({allCustomizations});

    }
    onEnter() {
         this.props.applyFilters(this.state.selectedMenu,this.state.selectedCustomizations,this.state.allCustomizations);
    }
    renderDishSelectAll(){
            return (
                <TouchableOpacity onPress={()=> this.onDishChange(null)} style={styles.selectAllWrapper}>
                    <AppText style={styles.selectAll}>{I18n.t('selectAll')}</AppText>
                </TouchableOpacity>
            );
    }
    renderCustomizationSelectAll(){
        const { selectedCustomizations } = this.state;
        if(selectedCustomizations && Object.keys(selectedCustomizations).length > 0) {
            return (
                <TouchableOpacity onPress={()=> this.clearSelection()} style={styles.selectAllWrapper}>
                    <AppText style={styles.selectAll}>{I18n.t('clearSelection')}</AppText>
                </TouchableOpacity>
            );
        }
        return null;
    }
    clearSelection() {
        this.setState({selectedCustomizations : null});
    }
    onDishChange(key){;
        let selectedMenu = {...this.state.selectedMenu};
        let cArr = [];
        if(key){
            if(selectedMenu.hasOwnProperty(key)){
                delete selectedMenu[key];
            }else{
                selectedMenu[key] = this.props.allMenu[key];
            }
            this.setState({selectedMenu});
        }else{
            this.setState({selectedMenu: this.props.allMenu});
        }
        Object.keys(selectedMenu).map((key, i) => {
            selectedMenu[key].map((item) => {
                if(item.customizations.length > 0){
                    item.customizations.map((customization) => {
                        cArr.push({
                            'name' : customization.customization.name
                        })
                    })
                }
             })
        });
        cArr = groupBy(cArr, 'name');
        this.setState({allCustomizations: cArr})
    }
    onCustomizationChange(key){
        let selectedCustomizations = {...this.state.selectedCustomizations};
            if(selectedCustomizations.hasOwnProperty(key)){
                delete selectedCustomizations[key];
            }else{
                selectedCustomizations[key] = this.props.allCustomizations[key];
            }
            if(Object.keys(selectedCustomizations).length > 0) {
                this.setState({selectedCustomizations});
            }else{
                this.setState({selectedCustomizations : null});
            }
    }
    renderDish() {
            let exists = false;
            if(this.props.allMenu){
                return Object.keys(this.props.allMenu).map((key, i) => {
                    if (this.state.selectedMenu) {
                        exists = this.state.selectedMenu.hasOwnProperty(key);
                    }
                    return (
                        <TouchableOpacity style={[styles.options]} key={key} onPress={()=> this.onDishChange(key,this.props.allMenu[key])} >
                            <View style={[styles.optionsCircle,exists ? styles.selectedBg : null]}>
                            </View>
                            <View style={styles.optionsName}>
                                <AppText style={[{textAlign:'center'},exists ? styles.selectedText : null]}>{key}</AppText>
                            </View>
                        </TouchableOpacity>

                    );
                });
            }
        return null;
    }
    renderCustomizations() {
        let exists = false;
        const { allCustomizations, selectedCustomizations } = this.state;
        if(allCustomizations){
            return Object.keys(allCustomizations).map((key, i) => {
                if (selectedCustomizations) {
                    exists = selectedCustomizations.hasOwnProperty(key);
                }
                return (
                    <TouchableOpacity style={[styles.options]} key={key} onPress={()=> this.onCustomizationChange(key)} >
                        <View style={[styles.optionsCircle,exists ? styles.selectedBg : null]}>
                        </View>
                        <View style={styles.optionsName}>
                            <AppText style={[{textAlign:'center'},exists ? styles.selectedText : null]}>{key}</AppText>
                        </View>
                    </TouchableOpacity>

                );
            });
        }
        return null;
    }

    render() {
        return (
                <View style={styles.container}>
                    <View style={{width:500}}>
                        <View style={styles.dishView}>
                            <Image source={require("../../../assets/images/menuIconColor.png")}/>
                            <AppText style={styles.dishViewText}>Dish</AppText>
                        </View>
                        <TouchableWithoutFeedback style={{zIndex:999999}} onPress={() => Actions.pop()}>
                            <View style={[styles.closeBtn]}>
                                    <Image source={require("../../../assets/images/closeBtn.png")}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={styles.row}>

                                <View style={styles.collapseViewHeader}>
                                    <View style={styles.collapseNameWrapper}>
                                        <AppText style={styles.collapseViewName}>{I18n.t('dish')}</AppText>
                                    </View>
                                    {this.renderDishSelectAll()}
                                </View>
                                <ScrollView style={{flex:1}}>
                                    <View style={styles.collapseViewContainer}>
                                       {this.renderDish()}
                                    </View>
                                </ScrollView>
                                <View style={styles.collapseViewHeader}>
                                    <View style={styles.collapseNameWrapper}>
                                        <AppText style={styles.collapseViewName}>{I18n.t('customizations')}</AppText>
                                    </View>
                                    {this.renderCustomizationSelectAll()}
                                </View>
                                <ScrollView style={{flex:1}}>
                                    <View style={styles.collapseViewContainer}>
                                        {this.renderCustomizations()}
                                    </View>
                                </ScrollView>
                            <View>
                               <TouchableOpacity onPress={() => this.onEnter()} style={styles.addBtn}>
                                   <AppText style={styles.addBtnText}>{I18n.t('add')}</AppText>
                               </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        top:70,
        height:520,
        width:500,
        backgroundColor:'#FFF',
        borderRadius:10,
        flexDirection: 'column',
        padding: 20,
        borderTopLeftRadius:0,
        // borderWidth:2,
    },
    container: {
        height:'100%',
        width:'100%',
        left:0,
        top:0,
        position:'absolute',
        backgroundColor: "rgba(0,0,0,0.6)",
        // zIndex: 9999,
        paddingLeft:300
    },
    dishView:{
        height:60,
        width:120,
        position:'absolute',
        left:0,
        top:10,
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        backgroundColor:'#FFF',
        flexDirection: 'row',
        paddingTop:12,
        paddingLeft:9,
    },
    dishViewText: {
        paddingLeft:15,
        paddingTop:3.5,
        color:'#FF6E6E'
    },
    closeBtn: {
        height:50,
        width:50,
        borderRadius:100,
        alignSelf:'flex-end',
        position:'absolute',
        top:45,
        right:-25,
        // right:140,
        zIndex:999999,
        backgroundColor:'#FF6E6E',
        justifyContent:'center',
        alignItems: 'center'
    },
    options:{
        // height:70,
        width:70,
        alignItems:'center',
        marginTop:20,
        padding:1
    },
    optionsCircle:{
        width:20,
        height:20,
        borderWidth:1,
        borderRadius:100,
        borderColor: '#FF6E6E'
    },
    optionsName:{
        flexWrap:'wrap',
        paddingTop:5,
        alignSelf: 'center'
    },
    selectedBg:{
        backgroundColor:'#FF6E6E',
    },
    selectedText:{
        color:'#FF6E6E',
    },
    collapseViewContainer:{
        flexDirection:'row',
        // flex:1,
        flexGrow:1,
        flexWrap:'wrap',
        // padding:15,
    },
    collapseViewHeader:{
        borderBottomWidth:1,
        borderBottomColor:'#FF6E6E',
        flexDirection:'row',
        marginTop:10
    },
    collapseNameWrapper:{
        width:350
    },
    collapseViewName:{
        color:'#FF6E6E',
        fontSize:17,
        paddingLeft:5
    },
    selectAllWrapper:{
        alignItems:'flex-end',flex:1
    },
    selectAll:{
        paddingRight:5
    },
    addBtn:{
        width:150,height:50,alignItems:'center',justifyContent:'center',backgroundColor:'#FF6E6E',alignSelf:'flex-end',borderRadius:50
    },
    addBtnText:{
        color: '#FFF',
        fontSize: 20,
    }
});
const mapStateToProps = (state) => {
    const { tableHead,selectedTableHead,items,selectedMenu,allMenu,selectedCustomizations,allCustomizations } = state.orderHistory;
    return { tableHead,selectedTableHead,items,selectedMenu,allMenu,selectedCustomizations,allCustomizations };
};
export default connect(mapStateToProps,{ applyFilters })(ColumnSelectionOverlay);