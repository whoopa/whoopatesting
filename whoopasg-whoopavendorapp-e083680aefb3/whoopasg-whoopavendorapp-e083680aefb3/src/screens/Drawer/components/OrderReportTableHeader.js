import React from 'react';
import { View, Text,TouchableOpacity,Image,StyleSheet } from 'react-native';
import {connect} from 'react-redux';
import {sortOrder,menuColumnSelection} from '../actions';
import {AppText} from '../../../components'

class OrderReportTableHeader extends React.Component {
    renderArrow(key){
        const { selected, toggle } = this.props;
        if(key === selected){
            if(toggle){
                return <Image source={require("../../../assets/images/circle-down-arrow.png")}  />
            }
            return <Image source={require("../../../assets/images/circle-up-arrow.png")} style={{marginTop:-1}}/>
        }

    }
    renderData(){
        const { itemContainer, titleText, circle,bgColor,timeDate } = styles;
        const { sortOrder, selected, tableHead } = this.props;
        return Object.keys(tableHead).map((key,i) => {
                return (
                    <TouchableOpacity key={i} onPress={()=>sortOrder(key)} style={[itemContainer,i === 0 ? {paddingLeft: 2,flex:2}:'',key === 'time' || key === 'update_timestamp' ? {justifyContent:'center'}: null ]}>
                        <AppText style={[titleText]}>{tableHead[key]}</AppText>
                        <View style={[circle,key === selected ? bgColor : '' ]}>
                            <View style={{alignItems:'center'}}>
                                {this.renderArrow(key)}
                            </View>
                        </View>
                    </TouchableOpacity>
                );
        });
    }
    render() {
        const { container } = styles;
        const {menuColumnSelection} = this.props;
        return(
            <View style={container}>
                <TouchableOpacity style={styles.menuSelectionIcon} onPress={menuColumnSelection}>
                    <Image source={require("../../../assets/images/menuIcon.png")}/>
                </TouchableOpacity>
                {this.renderData()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
        height:70,
        flexDirection:'row',
        backgroundColor: '#FF6E6E',
        paddingLeft:5,
        paddingRight:10,
        flex:1,
        marginBottom:20
    },
    itemContainer:{
        flex:1,
        flexDirection: 'row',
        alignItems:'center',
        // borderWidth:2,
    },
    circle: {
        width:20,
        height:20,
        borderRadius: 100,
        marginLeft:5,
        borderColor:'#FFF',
        borderWidth: 1
    },
    titleText:{
        color: '#FFF'
    },
    bgColor:{
        backgroundColor:'#FFF'
    },
    timeDate:{
        marginRight:15,marginLeft:5
    },
    menuSelectionIcon:{
        height:30,
        width:30,
        alignSelf:'center',
        justifyContent: 'center'
    }
});
const mapStateToProps = (state) => {
    const { orders, selected,toggle, tableHead } = state.orderHistory;
    return {orders, selected, toggle, tableHead};
};
export default connect(mapStateToProps,{sortOrder,menuColumnSelection})(OrderReportTableHeader);