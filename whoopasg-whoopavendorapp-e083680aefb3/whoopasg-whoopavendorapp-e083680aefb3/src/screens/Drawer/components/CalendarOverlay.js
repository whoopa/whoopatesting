import React from 'react';
import {View, Text, TouchableOpacity, ScrollView, StyleSheet, Image, TouchableWithoutFeedback} from 'react-native';
import {getOrderFilter,closeCalendarOverlay} from '../../Drawer/actions'
import {connect} from 'react-redux';
import moment from 'moment';
import CalendarPicker from 'react-native-calendar-picker';
import {AppText} from '../../../components'
import I18n from '../../../translations';

class CalendarOverlay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedEndDate: null,
            selectedStartDate: null,
            currentDate: new Date().getDate() + 30,
            today: moment(new Date()).format('Do MMM, YYYY'),
        };
        this.onStartDateChange = this.onStartDateChange.bind(this);
        this.onEndDateChange = this.onEndDateChange.bind(this);
    }

    onStartDateChange(date, type) {
        this.setState({
            selectedStartDate: date,
            selectedEndDate: date,
        });
    }

    onEndDateChange(date, type) {
        this.setState({
            selectedEndDate: date,
        });
    }

    closeOverlay(){
        const {password,closeCalendarOverlay,getOrderFilter} = this.props;
        const {selectedStartDate, selectedEndDate} = this.state;
        if(password && selectedStartDate && selectedEndDate ){
            if(selectedStartDate <= selectedEndDate){
                const startDate =  selectedStartDate.toString();
                const endDate = selectedEndDate.toString();
                let dateRange = moment(startDate).format('Do MMM') + ' - ' + moment(endDate).format('Do MMM,YYYY');
                getOrderFilter(password, selectedStartDate, selectedEndDate);
                closeCalendarOverlay(dateRange,selectedStartDate,selectedEndDate);
            }
        }
    }
    renderToday() {
        const { today } = this.state;
        const { dateRange } = this.props;
        const {selectedStartDate,selectedEndDate} = this.state;
        if(selectedStartDate && selectedEndDate ){
            return moment(selectedStartDate).format('Do MMM') + ' - ' + moment(selectedEndDate).format('Do MMM,YYYY');
        }else if (dateRange !== null) {
            return dateRange;
        }
        return today;
    }

    render(){
        const {closeCalendarOverlay} = this.props;
        let now = new Date();
        if(this.state.selectedEndDate){
            now = this.state.selectedEndDate
        }else{
            now.setDate(now.getDate() + 30);
        }
        const next = <Text><Image source={require("../../../assets/images/calendar-left-arrow.png")} /></Text>;
        const prev = <Text><Image source={require("../../../assets/images/calendar-right-arrow.png")} /></Text>;
        const maxDate = moment(this.state.selectedStartDate).add(2, 'months').toDate();
        return <View style={styles.container}>
            <View style={styles.showDates}>
                    <View style={styles.fieldDate}>
                        <AppText style={[styles.dateFieldText, {alignSelf: 'flex-start'}]}>
                            {this.renderToday()}
                        </AppText>
                    </View>
                    <View style={styles.fieldArrow}>
                        <AppText style={[styles.dateFieldText, {alignSelf: 'flex-end', marginLeft: 20}]}> <Image
                            source={require("../../../assets/images/down-arrow.png")} /> </AppText>
                    </View>
                </View>
            <View>
                <TouchableWithoutFeedback  onPress={()=> closeCalendarOverlay()}>
                    <View style={[styles.closeBtn]}>
                        <Image source={require("../../../assets/images/closeBtn.png")}/>
                    </View>
                </TouchableWithoutFeedback>

                <View style={styles.row}>
                    <View style={{flexDirection:'row'}}>
                        <View style={styles.calendarWrapper}>
                            <View style={styles.calendarHeader}>
                                <AppText style={styles.calendarHeaderText}>{I18n.t('from')}</AppText>
                            </View>
                            <CalendarPicker
                            onDateChange={this.onStartDateChange}
                            width={300}
                            todayBackgroundColor='#FFF'
                            selectedDayColor="#FF6E6E"
                            weekdays={['S', 'M', 'T', 'W', 'T', 'F', 'S']}
                            previousTitle={next}
                            nextTitle={prev}
                            />
                        </View>
                        <View style={styles.calendarWrapper}>
                            <View style={styles.calendarHeader}>
                                <AppText style={styles.calendarHeaderText}>{I18n.t('to')}</AppText>
                            </View>
                            <CalendarPicker
                                onDateChange={this.onEndDateChange}
                                width={300}
                                initialDate={now}
                                todayBackgroundColor='#FFF'
                                selectedDayColor="#FF6E6E"
                                maxDate={maxDate}
                                weekdays={['S', 'M', 'T', 'W', 'T', 'F', 'S']}
                                previousTitle={next}
                                nextTitle={prev}
                            />
                        </View>
                    </View>
                    <View style={styles.selectWrapper}>
                        <TouchableOpacity style={styles.selectBtn} onPress={()=> this.closeOverlay()}>
                            <AppText style={styles.nextStyle}>{I18n.t('selectDates')}</AppText>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    }
}
const styles = StyleSheet.create({
    row: {
        top:115,
        backgroundColor:'#FFF',
        borderRadius:10,
        borderTopLeftRadius:0,
        left:10,
        flexDirection:'column',
        padding:20,
    },
    container: {
        height:'100%',
        width:'100%',
        left:0,
        top:0,
        position:'absolute',
        backgroundColor: "rgba(0,0,0,0.6)",
        zIndex: 9999,
        alignItems:'flex-start',
    },
    closeBtn: {
        height:50,
        width:50,
        borderRadius:100,
        // borderWidth: 1,
        alignSelf:'flex-end',
        // borderColor: '#FF6E6E',
        position:'absolute',
        top: 90,
        right:-35,
        // left:670,
        zIndex:6,
        backgroundColor:'#FF6E6E',
        justifyContent:'center',
        alignItems: 'center'
    },
    nextStyle:{
        fontSize:20,
        color: '#FF6E6E',
    },
    calendarWrapper:{
        paddingLeft:20,
        paddingRight:20
    },
    selectWrapper:{
        alignItems:'flex-end',
        padding:20,
        paddingRight:50
    },
    selectBtn:{
        height:60,
        width:200,
        borderWidth: 2,
        borderColor: '#FF6E6E',
        alignItems:'center',
        justifyContent:'center',
        borderRadius: 50,
    },
    calendarHeader:{
        borderBottomWidth:2,
        alignItems:'center',
        padding:10,
        borderBottomColor:'#FF6E6E',
        marginBottom:5
    },
    calendarHeaderText:{
        color:'#FF6E6E',
        fontSize:20,
        fontWeight: 'bold'
    },
    fieldDate: {
        width: 230,
        justifyContent: 'center',
        borderBottomWidth:1,
        borderBottomColor:'#FF6E6E',
    },
    dateFieldText: {
        fontSize: 18,
        color: '#FF6E6E',

    },
    fieldArrow: {
        width: 40,
        justifyContent: 'center',
        borderBottomWidth:1,
        borderBottomColor:'#FF6E6E',
    },
    showDates:{
        height:45,
        width:290,
        position:'absolute',
        top:70,
        left:10,
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        paddingLeft:5,
        paddingRight:5,
        paddingTop:4,
        flexDirection:'row',
        backgroundColor:'#FFF'
    }
});
const mapStateToProps = (state) => {
    const {password} = state.user;
    const { dateRange } = state.orderHistory;
    return {password,dateRange};
};
export default connect(mapStateToProps, {getOrderFilter,closeCalendarOverlay})(CalendarOverlay);