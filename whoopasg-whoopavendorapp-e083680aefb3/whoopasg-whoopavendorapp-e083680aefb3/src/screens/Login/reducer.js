import * as actionTypes from './actionTypes';
import * as drawerActionTypes from '../Drawer/actionTypes';

export const APP_VERSION = '3.6';

const initialState = {
    FCMToken: null,
    FCMTopic: null,
    FCMLoading: false,
    version: APP_VERSION,
    authenticated: false,
    simplifiedApp: false,
    multiDevice: false,
    password: null, //'ovlomtKUcf' 'ovyup4n9j2'
    processing: false,
    failureCount: 0,
//    refreshing: false,
//    accessToken: null,
//    refreshToken: null,
//    tokenExpiry: null,
    error: null,
//    pendingData: null,
//    pendingCall: null,
//    refreshed: null,
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case drawerActionTypes.FCM_TOKEN_LOADING:
            return {
                ...state,
                FCMLoading: true
            };
        case drawerActionTypes.FCM_TOKEN_LOADED:
            return {
                ...state,
                FCMToken: action.fcmToken,
                FCMLoading: false
            };
          case actionTypes.LOGIN:
            return {
              ...state,
              error: null,
              processing: true
            };
          case actionTypes.LOGIN_RESET:
            return {
              ...state,
              processing: false
            };
        case actionTypes.LOGIN_FAIL:
            return {
                ...state,
                processing: false,
                failureCount: (state.failureCount+1),
                error: action.payload
            };
        case actionTypes.LOGIN_RESET_FAILURE:
            return {
                ...state,
                failureCount: 0,
                error: null
            };
        case actionTypes.LOGIN_MULTIPLE_FAILED:
            return {
                ...state,
                error: 'Multiple failed attempts. Please wait for 30 seconds to try again.'
            };
        case actionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                processing: false,
                authenticated: action.payload.authenticated,
                simplifiedApp: action.payload.simplifiedApp,
                multiDevice: action.payload.multiDevice,
                FCMTopic: action.payload.FCMTopic,
                failureCount: 0,
                // accessToken: action.payload.access_token,
                // refreshToken: action.payload.refresh_token,
                // tokenExpiry: now.getTime() + (parseInt(action.payload.expires_in) * 1000),
                error: null
            };
        case actionTypes.LOGIN_EMAIL_CHANGED:
            return {
                ...state,
                email: action.payload
            };
        case actionTypes.LOGIN_PASSWORD_CHANGED:
            return {
                ...state,
                password: action.payload
            };
        // case actionTypes.REFRESH_TOKEN:
        //     return {
        //         ...state,
        //         refreshing: true,
        //     };
        // case actionTypes.REFRESH_SUCCESS:
        //     return {
        //         ...state,
        //         refreshing: false,
        //         processing: false,
        //         authenticated: true,
        //         accessToken: action.payload.access_token,
        //         refreshToken: action.payload.refresh_token,
        //         tokenExpiry: (now.getTime() + ((parseInt(action.payload.expires_in) - (60*60)) * 1000)),
        //         error: null,
        //     };
        // case actionTypes.REFRESH_AND_CALL:
        //     return {
        //         ...state,
        //         pendingData: action.payload,
        //         pendingCall: action.onSuccess,
        //         refreshed: 0,
        //     };
        // case actionTypes.REFRESH_AND_CALL_REFRESHED:
        //     return {
        //         ...state,
        //         processing: false,
        //         authenticated: true,
        //         accessToken: action.payload.access_token,
        //         refreshToken: action.payload.refresh_token,
        //         tokenExpiry: now.getTime() + (parseInt(action.payload.expires_in) * 1000),
        //         error: null,
        //         refreshed: 1,
        //     };
        // case actionTypes.REFRESH_CALL:
        //     return {
        //         ...state,
        //         pendingData: null,
        //         pendingCall: null,
        //         refreshed: null
        //     };
        // case actionTypes.REFRESH_FAILED:
        //     return {
        //         ...initialState,
        //         password: state.password,
        //         error: "Refresh Failed: "+action.payload
        //     };
        case drawerActionTypes.VERSION_UPDATED:
            return {
                ...initialState,
                error: 'Version was updated. Please login again.'
            };
        case drawerActionTypes.LOGOUT:
            return {
                ...initialState,
                // Keep FCM data for unsubscription from topic
                FCMTopic: state.FCMTopic,
                FCMToken: state.FCMToken
            };
        case drawerActionTypes.RESET_FCM:
            return {
                ...state,
                FCMTopic: null,
                FCMToken: null
            };
        default:
            return state;
    }
};