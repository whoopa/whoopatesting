import React, {Component} from 'react';
import {View, StyleSheet, StatusBar, Image} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import DeviceInfo from 'react-native-device-info'
import {KeyPad, AppText, Spinner} from '../../components';
import {loginUser, emailChanged, passwordChanged, resetFailure, multipleFailedAttempts, reset} from './actions';
import {versionUpdated} from '../Drawer/actions';
import {APP_VERSION} from './reducer'
import I18n from '../../translations'

class Login extends Component {
    timer = null;

    componentWillMount () {
        this.props.reset()
    }
    componentDidMount() {
        console.log('drawer componentDidMount');
        this.checkState();
    }

    componentDidUpdate() {
        console.log('drawer componentDidUpdate');
        this.checkState();
        if (this.props.failureCount >= 5) {
            this.props.multipleFailedAttempts();
            this.timer = setTimeout(this.props.resetFailure.bind(this), 30000);
        } else {
            if (this.timer !== null) {
                clearTimeout(this.timer);
            }
        }
    }

    checkState() {
        console.log('check state', this.props.authenticated);
        if(this.props.version !== APP_VERSION){
            this.props.versionUpdated();
        }else if (this.props.authenticated) {
            if (this.props.simplifiedApp) {
                console.log("Login > Simplified");
                Actions.replace('simplifiedApp');
            } else {
                console.log("Login > Main");
                Actions.replace('main');
            }
        }
    }

    renderError() {
        if (this.props.error) {
            return (
                <AppText style={styles.error}>
                    {this.props.error.toString()}
                </AppText>
            );
        }
    }

    renderButton() {
        if (this.props.failureCount < 5) {
            // return (
            //     <Button
            //         title={I18n.t('login')}
            //         onPress={() => {
            //             Actions.push('loginKeypad')
            //         }}
            //         processing={this.props.processing}>
            //         {I18n.t('loginButton')}
            //     </Button>
            // );
            return (
                <KeyPad
                    secure
                    compact
                    processing={this.props.processing}
                    onEnter={(password) => {
                        const now = new Date();
                        let ts = now.getTime();
                        let pass = parseInt(password);
                        let encoded = "";
                        while (ts > 0 && pass > 0) {
                            encoded += ts % 10;
                            encoded += pass % 10;
                            ts = parseInt(ts / 10);
                            pass = parseInt(pass / 10);
                        }
                        if (pass > 0) {
                            encoded += pass;
                        }
                        this.props.passwordChanged(encoded);
                        this.props.loginUser(encoded, {
                            uniqueId: DeviceInfo.getUniqueID(),
                            manufacturer: DeviceInfo.getManufacturer(),
                            model: DeviceInfo.getModel(),
                            name: DeviceInfo.getDeviceName(),
                            appVersion: DeviceInfo.getVersion(),
                            locale: DeviceInfo.getDeviceLocale()
                        });
                    }}
                />
            );
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar hidden/>
                <View style={styles.topBar}>
                    {/*<Image style={styles.logo} source={require("../../assets/images/logo.png")}/>*/}
                    <View>
                        <AppText style={styles.topBarText}>{I18n.t('loginTitle')}</AppText>
                        <AppText style={styles.version}>{this.props.version} ({DeviceInfo.getUniqueID()})</AppText>
                    </View>
                </View>
                {this.props.refreshing ? (<View style={styles.container}><Spinner /><AppText>Refreshing</AppText></View>) : (
                    <View style={styles.row}>
                        <View>
                        </View>
                        <View style={{alignItems: 'center'}}>
                            <View style={styles.box}>
                                {/*<Input*/}
                                {/*placeholder={I18n.t('password')}*/}
                                {/*onSubmitEditing={this.onSubmit.bind(this)}*/}
                                {/*onChangeText={this.onPasswordChange.bind(this)}*/}
                                {/*value={this.props.password}*/}
                                {/*password*/}
                                {/*/>*/}
                                {this.renderButton()}
                            </View>
                            { this.renderError() }
                        </View>
                    </View>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#EEE'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },

    box: {
        width: 320,
        alignItems: 'center',
        backgroundColor: '#EEE'
    },

    error: {
        padding: 10,
        color: '#FFF',
        backgroundColor: '#A44',
        borderRadius: 5,
        margin: 10
    },

    topBar: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        margin: 20
    },

    topBarText: {
        color: '#FF6E6E',
        fontSize: 36,
        fontWeight: 'bold',
    },

    logo: {},
    version: {
        color: '#777',
        fontSize: 10,
        fontWeight: 'bold',
        textAlign: 'right'
    }
});

const mapStateToProps = (state) => {
    const {password, error, processing, version, authenticated, failureCount, simplifiedApp} = state.user;
    return {password, error, processing, version, authenticated, failureCount, simplifiedApp};
};

export default connect(mapStateToProps, {
    loginUser,
    emailChanged,
    passwordChanged,
    resetFailure,
    multipleFailedAttempts,
    versionUpdated,
    reset
})(Login);