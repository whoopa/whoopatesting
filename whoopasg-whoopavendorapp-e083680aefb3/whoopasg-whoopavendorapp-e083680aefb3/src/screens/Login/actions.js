import * as actionTypes from './actionTypes';
import API from '../../services/api'

export const loginUser = (password, device) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.LOGIN });

        // API.authenticate(password)
        //     .then(response => loginUserSuccess(dispatch, response))
        //     .catch((e) => {loginUserFailed(dispatch, e)});

        API.checkPassword(password, {device:JSON.stringify(device)})
            .then(response => loginUserSuccess(dispatch, response))
            .catch((e) => {loginUserFailed(dispatch, e)});
    };
};

const loginUserSuccess = (dispatch, response) => {
    console.log("Login Response", response);
    dispatch({
        type: actionTypes.LOGIN_SUCCESS,
        payload: response
    });
};

const loginUserFailed = (dispatch, error) => {
    dispatch({
        type: actionTypes.LOGIN_FAIL,
        payload: error
    });
};

export const emailChanged = (text) => {
    return {
        type: actionTypes.LOGIN_EMAIL_CHANGED,
        payload: text
    };
};

export const passwordChanged = (text) => {
    return {
        type: actionTypes.LOGIN_PASSWORD_CHANGED,
        payload: text
    };
};
export const resetFailure = () => {
    return {
        type: actionTypes.LOGIN_RESET_FAILURE
    };
};
export const multipleFailedAttempts = () => {
  return {
    type: actionTypes.LOGIN_MULTIPLE_FAILED
  };
};
export const reset = () => {
  return {
    type: actionTypes.LOGIN_RESET
  };
};


// export const refreshAuthToken = (token) => {
//     return (dispatch) => {
//         dispatch({ type: actionTypes.REFRESH_TOKEN });
//         API.refresh(token)
//             .then(response => refreshSuccess(dispatch, response))
//             .catch((e) => {refreshFailed(dispatch, e)});
//     };
// };
// export const refreshAuthTokenAndCall = (token) => {
//     return (dispatch) => {
//         API.refresh(token)
//             .then(response => refreshAndCallSuccess(dispatch, response))
//             .catch((e) => {refreshFailed(dispatch, e)});
//     };
// };

// const refreshSuccess = (dispatch, response) => {
//     dispatch({
//         type: actionTypes.REFRESH_SUCCESS,
//         payload: response
//     });
//     Actions.pop();
//     Actions.main();
// };
//
// const refreshAndCallSuccess = (dispatch, response) => {
//     dispatch({
//         type: actionTypes.REFRESH_AND_CALL_REFRESHED,
//         payload: response
//     });
// };
//
// const refreshFailed = (dispatch, error) => {
//     dispatch({
//         type: actionTypes.REFRESH_FAILED,
//         payload: error
//     });
// };
