import * as actionTypes from './actionTypes';
import * as drawerActionTypes from '../Drawer/actionTypes';
import * as loginActionTypes from '../Login/actionTypes';
import update from 'immutability-helper';
import { normalize, schema, arrayOf } from 'normalizr';
import {transformOrder} from "../../utilities/ObjectUtils";

const initialState = {
    categories: null,
    takeAwayPrice:0,
    menuItems: null,
    orders: null,
    selectedItem: null,
    selectedCategory: null,
    currentOrder: null,
    menuLoading: false,
    ordersLoading: false,
    menuError: false,
    ordersError: false,
    unsaved: null,
};

export const menuItemSchema = new schema.Entity('menuItems');
export const categorySchema = new schema.Entity('categories', {
    menu_items: [menuItemSchema]
});
const categoriesSchema = new schema.Array(categorySchema);

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.EDIT_ITEM:
            return {
                ...state,
                selectedItem: action.payload
            };
        case actionTypes.MENU_LOADING:
            return {
                ...state,
                menuLoading: true
            };
        case actionTypes.MENU_FAILED:
            return {
                ...state,
                menuLoading: false,
                menuError: action.error
            };
        case actionTypes.MENU_LOADED:
            let normalizedData = normalize(action.payload.menu, categoriesSchema);
            return {
                ...state,
                ...normalizedData.entities,
                takeAwayPrice: parseFloat(action.payload.takeAwayPrice),
                menuLoading: false,
                menuError: false
            };
        case actionTypes.ORDERS_LOADING:
            return {
                ...state,
                ordersLoading: true
            };
        case actionTypes.ORDERS_FAILED:
            return {
                ...state,
                ordersLoading: false,
                ordersError: action.error
            };
        case actionTypes.ORDERS_LOADED:
            orders = action.payload;
            if(orders === null){
                orders = [];
            }
            return {
                ...state,
                orders,
                ordersLoading: false,
                ordersError: false
            };
        case actionTypes.CATEGORY_SELECTED:
            return {
                ...state,
                selectedCategory: action.payload
            };
        case actionTypes.QUANTITY_CHANGED:
            let currentOrder = {...state.currentOrder};
            if(action.quantity === 0){
                currentOrder = update(currentOrder, {$unset: [action.item.id]});
            }else{
                if(!currentOrder[action.item.id]){
                    currentOrder[action.item.id] = {
                        size: action.item.sizes[0].id,
                        takeAway: false,
                        price: action.item.price,
                        customizations: []
                    }
                }
                currentOrder[action.item.id] = {
                    ...currentOrder[action.item.id],
                    quantity: action.quantity
                };
            }
            return {
                ...state,
                currentOrder
            };
        case actionTypes.EDIT_ITEM_CONFIRMED:
            currentOrder = {...state.currentOrder};
            const {id, takeAway, size, price, quantity, customizations} = action.customizedItem;
            currentOrder[id] = {takeAway, size, price, quantity, customizations};
            return {
                ...state,
                currentOrder
            };
        case actionTypes.TOGGLE_SOLD_OUT:
            return {
                ...state,
                menuItems:{
                    ...state.menuItems,
                    [action.menuItemId]: {
                        ...state.menuItems[action.menuItemId],
                        sold_out: action.soldOut
                    }
                }
            };
        case actionTypes.ORDER_CLOSING:
            return {
                ...state,
                orders: state.orders.map(order => {if(action.id === order.id) order.closing = true; return order;})
            };
        case actionTypes.ORDER_CLOSING_FAILED:
            return {
                ...state,
                orders: state.orders.map(order => {if(action.id === order.id) order.closing = false; return order;})
            };
        case actionTypes.ORDER_CLOSED:
            return {
                ...state,
                orders: state.orders.filter(order => (order.id !== action.id))
            };
        case actionTypes.CONFIRMING_ORDER:
            return {
                ...state,
                currentOrder: null,
                unsaved: {
                    ...state.unsaved,
                    [action.uid]: transformOrder(action.uid, action.order) //Convert order data into the same format as seen in the API
                }
            };
        case actionTypes.CONFIRMING_ORDER_FAILED:
            return {
                ...state,
            };
        case drawerActionTypes.ORDER_RECEIVED:
            let orders = [];
            if(state.orders){
                orders = state.orders.map(order=>order);
            }
            orders.push(action.payload);
            return {
                ...state,
                orders
            };
        case actionTypes.ORDER_CONFIRMED:
            orders = [];
            if(state.orders){
                orders = state.orders.map(order=>order);
            }
            orders.push(action.payload);
            return {
                ...state,
                unsaved: update(state.unsaved, {$unset: [action.uid]}),
                orders
            };
        case loginActionTypes.REFRESH_FAILED:
        case loginActionTypes.LOGIN_FAIL:
        case drawerActionTypes.VERSION_UPDATED:
        case drawerActionTypes.RELOAD_DATA:
        case drawerActionTypes.LOGOUT:
            return {
                ...initialState
            };
        default:
            return state;
    }
};