import React, {Component} from 'react';
import {View, StyleSheet, StatusBar, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {TabNav, Spinner, AppText, Button} from '../../components'
import {MenuItem, SubmitButton, Order} from './components'
import {loadMenu, loadOrders, selectCategory, onQuantityChange, orderCompleted, editItem, toggleSoldOut} from './actions'
import I18n from '../../translations';
import {getDisplayCategories, getDisplayItems, getOrderSummary, getOrdersData} from './selectors'

class Simplified extends Component {


    componentWillMount() {
        this.notificationListener = null;
    }

    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate() {
        this.loadData();
    }

    loadData(force = false) {
        const {categories, orders, password, authenticated, loadMenu, menuLoading, loadOrders, ordersLoading, ordersError, menuError} = this.props;
        if (authenticated && password !== null) {
            if (categories === null && !menuLoading && (!menuError || force)) {
                loadMenu(password);
            }
            if (orders === null && !ordersLoading && (!ordersError || force)) {
                console.log("Loading Orders because: ")
                loadOrders(password);
            }
        }
    }

    renderError(error){
        return (
            <View style={{flex: 1, alignSelf:'center'}}>
                <AppText style={{color: '#CC1E1E'}}>{error}</AppText>
                <Button onPress={()=>this.loadData(true)}>
                    Retry
                </Button>
            </View>
        );
    }

    renderMenu() {
        const {password, menuItems, menuLoading, onQuantityChange, editItem, toggleSoldOut, menuError} = this.props;
        if (menuLoading) {
            return <Spinner/>
        }
        if (menuError) {
            return this.renderError(menuError);
        }
        if (menuItems) {
            return menuItems.map(item => <MenuItem item={item} key={`MenuItem${item.id}`}
                                                   onPress={()=>editItem(item.id)}
                                                   onSoldOut={(soldOut) => toggleSoldOut(password, item.id, !item.sold_out)}
                                               onQuantityChange={onQuantityChange}/>);
        }
        return <AppText style={{flex: 1}}>{I18n.t('noData')}</AppText>;
    }

    renderOrders() {
        const {orders, ordersLoading, password, orderCompleted, ordersError} = this.props;
        if (ordersLoading) {
            return <Spinner/>
        }
        if (ordersError) {
            return this.renderError(ordersError);
        }
        if (orders && orders.length > 0) {
            return orders.map(order => <Order order={order} key={`Order${order.id}`} onComplete={()=>orderCompleted(password, order)}/>);
        }
        return <AppText style={{flex: 1, alignSelf:'center'}}>{I18n.t('noOrdersAdded')}</AppText>;
    }


    renderUnsaved() {
        const {unsaved} = this.props;
        if (unsaved) {
            return Object.keys(unsaved).map(key =>
                <Order order={unsaved[key]} key={`Order${key}`} />);
        }
    }

    render() {
        const {categories, selectCategory, selectedCategory, currentOrder} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <StatusBar hidden/>
                    <View style={[styles.section, styles.elevated]}>
                        <TabNav items={categories} selected={selectedCategory}
                                onPress={selectCategory}/>
                        <ScrollView style={styles.sectionLeft}>
                            {this.renderMenu()}
                        </ScrollView>
                    </View>
                    <View style={[styles.section]}>
                        <TabNav />
                        <ScrollView style={styles.sectionRight}>
                            {this.renderOrders()}
                            {this.renderUnsaved()}
                        </ScrollView>
                    </View>
                </View>
                <SubmitButton {...currentOrder} onPress={()=>Actions.push('orderConfirmationOverlay')} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#FFF'
    },
    elevated: {
        backgroundColor: '#FFF',
        elevation: 10,
    },
    section: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'flex-start'
    },
    sectionLeft: {
        flex: 0,
        marginRight: 75,
    },
    sectionRight: {
        flex: 0,
        marginLeft: 75
    }
});

const mapStateToProps = (state) => {
    let {selectedCategory, menuLoading, ordersLoading, unsaved, menuError, ordersError} = state.simplified;
    let {password, authenticated} = state.user;
    return {
        categories: getDisplayCategories(state.simplified),
        menuItems: getDisplayItems(state.simplified),
        orders: getOrdersData(state.simplified),
        currentOrder: getOrderSummary(state.simplified),
        selectedCategory,
        password,
        authenticated,
        menuLoading,
        ordersLoading,
        unsaved, menuError, ordersError
    };
};


export default connect(mapStateToProps, {
    loadMenu,
    loadOrders,
    selectCategory,
    onQuantityChange,
    orderCompleted,
    editItem,
    toggleSoldOut
})(Simplified);

