import { createSelector } from 'reselect'
import I18n from '../../translations'

const getSelectedCategory = (state) => state ? state.selectedCategory : null;
const getSelectedMenuItem = (state) => state ? state.selectedItem : null;
const getCategories = (state) => state ? state.categories : null;
const getMenuItems = (state) => state ? state.menuItems : null;
const getCurrentOrder = (state) => state ? state.currentOrder : null;
const getOrders = (state) => state ? state.orders : null;
const getTakeAwayPrice = (state) => state ? state.takeAwayPrice : 0;

export const getDisplayCategories = createSelector(
    [ getCategories ],
    (categories) => {
        if(categories){
            let ret = Object.keys(categories).map(key => ({id: key, name: categories[key].category_name}));
            ret.unshift({id: null, name: I18n.t('all')});
            return ret;
        }
        return null;
    }
);
export const getDisplayItems = createSelector(
    [ getSelectedCategory, getCategories, getMenuItems, getCurrentOrder, getTakeAwayPrice ],
    (selectedCategory, categories, menuItems, currentOrder, takeAwayPrice) => {
        if(categories){
            if(selectedCategory === null){
                return Object.keys(menuItems).map(key => buildMenuItem(menuItems[key], currentOrder, takeAwayPrice));
            }
            if(categories[selectedCategory]){
                return categories[selectedCategory].menu_items.map(key =>buildMenuItem(menuItems[key], currentOrder, takeAwayPrice))
            }
        }
        return null;
    }
);

export const getOrderSummary = createSelector(
    [ getMenuItems, getCurrentOrder, getTakeAwayPrice ],
    (menuItems, currentOrder, takeAwayPrice) => {
        let quantity = 0;
        let amount = 0;
        if(currentOrder){
            Object.keys(currentOrder).map((menuItemId)=>{
                const itemQuantity = currentOrder[menuItemId].quantity;
                const itemPrice = calcItemPrice({...menuItems[menuItemId], ...currentOrder[menuItemId]}, takeAwayPrice);
                quantity += itemQuantity;
                amount += (itemQuantity * itemPrice);
            });
        }
        return {quantity, amount};
    }
);

export const getOrder = createSelector(
    [ getMenuItems, getCurrentOrder ],
    (menuItems, currentOrder) => {
        if(currentOrder){
            return Object.keys(currentOrder).map((menuItemId)=>{
                const {takeAway, size, quantity, price, customizations} = currentOrder[menuItemId];
                const name = menuItems[menuItemId].name;
                return{name, menuItemId, quantity, price: price, size, takeAway, customizations };
            });
        }
        return null;
    }
);

export const getTransformableOrder = createSelector(
    [ getMenuItems, getCurrentOrder ],
    (menuItems, currentOrder) => {
        if(currentOrder){
            return Object.keys(currentOrder).map((menuItemId)=>{
                const {takeAway, size, quantity, price, customizations} = currentOrder[menuItemId];
                const item = menuItems[menuItemId];
                const menuItemName = item.name;
                const selectedSize = item.sizes.filter(s => (s.id === size))[0];
                const customizationInfo = [];
                if(customizations){
                    Object.keys(item.customization_sets).map(key => {
                        const set = item.customization_sets[key];
                        customizationInfo.push(...(set.filter(c => customizations.includes(c.id))));
                    });
                }
                return{
                    id: menuItemId,
                    menuItemName,
                    price: price.toString(),
                    quantity,
                    sizeName: selectedSize.name,
                    status: 0,
                    takeAway: takeAway ? 1 : 0,
                    customizationInfo
                };
            });
        }
        return null;
    }
);

export const getOrdersData = createSelector(
    [ getOrders ],
    (orders) => {
        if(orders){
            return orders.map((order)=>{
                order.price = order.items.reduce((price, item)=>(price + item.price*item.quantity), 0);
                return order;
            });
        }
        return null;
    }
);

export const getSelectedItem = createSelector(
    [ getCurrentOrder, getMenuItems, getSelectedMenuItem, getTakeAwayPrice ],
    ( currentOrder, menuItems, selectedMenuItem, takeAwayPrice) => {
        return buildMenuItem(menuItems[selectedMenuItem], currentOrder, takeAwayPrice);
    }
);

const buildMenuItem = (item, currentOrder, takeAwayPrice) => {
    let ret = {...item};
    if(currentOrder && currentOrder[item.id]){
        ret.quantity = currentOrder[item.id].quantity;
        ret.size = currentOrder[item.id].size;
        ret.takeAway = currentOrder[item.id].takeAway;
        ret.customizations = currentOrder[item.id].customizations;
        ret.price = calcItemPrice(ret, takeAwayPrice);
    }else{
        ret.quantity = 0;
        ret.size = item.sizes[0].id;
        ret.takeAway = false;
        ret.customizations = [];
        ret.price = item.sizes[0].price;
    }
    return ret;
};

export const calcItemPrice = (item, takeAwayPrice) => {
    const selectedSize = item.sizes.filter(size => (size.id === item.size))[0];
    let price = selectedSize.price;

    if(item.takeAway){
        price += takeAwayPrice;
    }

    const customizationPricing = {};
    Object.keys(item.customization_sets).map(cid=>{
        const set = item.customization_sets[cid];
        set.map(customization =>{
            customizationPricing[customization.id] = parseFloat(customization.price);
        })
    });

    price = item.customizations.reduce((total, id) => {
        return total + customizationPricing[id];
    }, price);

    return price;
};