import * as actionTypes from './actionTypes';
import { Actions } from 'react-native-router-flux';
import API from '../../services/api'

export const loadMenu = (password) => {
    return (dispatch) => {
        dispatch({type: actionTypes.MENU_LOADING});

        API.instance().callSecure(password, 'options')
            .then(response => onMenuLoaded(dispatch, response))
            .catch(e => {onMenuError(dispatch, e)})
    };
};
export const loadOrders = (password) => {
    return (dispatch) => {
        dispatch({type: actionTypes.ORDERS_LOADING});

        API.instance().callSecure(password, 'orders?filters[complete]=0')
            .then(response => onOrdersLoaded(dispatch, response))
            .catch(e => {onError(dispatch, e, actionTypes.ORDERS_FAILED)})
    };
};

export const confirmOrder = (password, FCMToken, items, transformableItems, orderNumber) => {
    return (dispatch) => {
        const data = {
            "complete": 0,
            "FCMToken": FCMToken,
            "orderNumber": orderNumber,
            "items": items.map(item => ({
                "takeAway": item.takeAway ? 1 : 0,
                "quantity": item.quantity,
                "price": item.price.toString(),
                "status": 0,
                "menuItem": item.menuItemId,
                "size": item.size,
                "customizations": item.customizations ? item.customizations : null
            }))
        };
        const uid =  new Date().getTime();
        dispatch({type: actionTypes.CONFIRMING_ORDER, uid, order: {
            ...data,
            items: transformableItems
        }});
        saveOrder(password, dispatch, data, uid);
    };
};

const saveOrder = (password, dispatch, data, uid) => {
    API.instance().callSecure(password, 'orders', data, 'post')
        .then(response => onConfirmation(dispatch, response, uid))
        .catch(e => {
            console.log("Retrying after order saving failed. ", e);
            saveOrder(password, dispatch, data, uid)
        })
};

const onConfirmation = (dispatch, response, uid) => {
    dispatch({
        type: actionTypes.ORDER_CONFIRMED,
        payload: response,
        uid
    });
};

export const orderCompleted = (password, order) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.ORDER_CLOSING, id: order.id});
        API.instance().callSecure(password, `orders/${order.id}`, {complete: true, price: order.price, paidAtStage: 4}, 'patch')
            .then(response => {dispatch({ type: actionTypes.ORDER_CLOSED, id: order.id})})
            .catch(e => {onError(dispatch, e, actionTypes.ORDER_CLOSING_FAILED)})
    };
};

export const onMenuLoaded = (dispatch, response) => {
    dispatch({
        type: actionTypes.MENU_LOADED,
        payload: response
    });
};

const onMenuError = (dispatch, error) => {
    dispatch({
        type: actionTypes.MENU_FAILED,
        payload: error
    });
};

export const onOrdersLoaded = (dispatch, response) => {
    dispatch({
        type: actionTypes.ORDERS_LOADED,
        payload: response
    });
};
const onError = (dispatch, error, type) => {
    console.log(error);
    dispatch({type, error});
    Actions.push('notificationOverlay', {message: error.toString(), button: 'OK'});
};

export const onQuantityChange = (item, quantity) => {
    return {
        type: actionTypes.QUANTITY_CHANGED,
        item, quantity
    };
};

export const selectCategory = (id) => {
    return {
        type: actionTypes.CATEGORY_SELECTED,
        payload: id
    };
};

export const editItem = (id) => {
    Actions.push('editItemOverlay');
    return{
        type: actionTypes.EDIT_ITEM,
        payload: id
    };
};

export const onConfirmCustomization = (customizedItem) => {
    return {
        type: actionTypes.EDIT_ITEM_CONFIRMED,
        customizedItem
    };
};

export const toggleSoldOut = (password, menuItemId, soldOut) => {
    const data = {
        menuItemId,
        soldOut: soldOut ? 1 : 0
    };
    API.instance().callSecure(password, 'options/item/markSoldOut', data, 'post')
        .then(response => console.log("toggleSoldOut", menuItemId, soldOut, response))
        .catch(e => console.log("ERROR in toggleSoldOut", menuItemId, soldOut, e));

    return {
        type: actionTypes.TOGGLE_SOLD_OUT,
        menuItemId,
        soldOut
    }
};