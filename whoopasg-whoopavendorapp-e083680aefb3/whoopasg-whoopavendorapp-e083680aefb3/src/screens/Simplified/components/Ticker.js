import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {AppText} from '../../../components'

const Ticker = (props) => {
    const { value, onChange } = props;
    return (
        <View style={[styles.container, props.style]}>
            <TouchableOpacity onPress={()=>value > 0 ? onChange(value-1) : null}>
                <AppText style={[styles.button,(value === 0 ? styles.disabled : styles.less)]}>-</AppText>
            </TouchableOpacity>
            <AppText style={[styles.value, (value === 0 ? styles.valueFaded : null)]}>{value}</AppText>
            <TouchableOpacity onPress={()=>onChange(value+1)}>
                <AppText style={[styles.button,styles.more]}>+</AppText>
            </TouchableOpacity>
        </View>
    );
};
const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: 'row',
        marginRight: 10,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    value:{
        width: 50,
        textAlign: 'center',
        fontSize: 25,
        fontWeight: 'bold',
        color: '#777'
    },
    valueFaded: {
        color: '#CCC'
    },
    button:{
        width: 44,
        height: 44,
        borderRadius: 22,
        borderWidth: 1,
        fontSize: 32,
        lineHeight: 32,
        fontWeight: 'bold',
        textAlign: 'center',
        alignSelf: 'center'
    },
    disabled:{
        color: '#CCC',
        borderColor: '#CCC',
    },
    less:{
        color: '#777',
        borderColor: '#777',
    },
    more:{
        color: '#FF6E6E',
        borderColor: '#FF6E6E',
    },
});

export {Ticker};