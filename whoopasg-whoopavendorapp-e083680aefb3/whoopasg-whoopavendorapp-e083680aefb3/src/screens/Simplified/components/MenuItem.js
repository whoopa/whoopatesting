import React from 'react';
import {View, Image, StyleSheet, TouchableWithoutFeedback} from 'react-native';
// import apiConfig from '../../../services/api/config';
// import {CachedImage} from 'react-native-cached-image';
import numeral from 'numeral';
import {Ticker} from './Ticker'
import {AppText, Toggle} from '../../../components'
import I18n from '../../../translations';

class MenuItem extends React.Component {
    state = {
        showToggle: false
    };

    toggleShowToggle() {
        this.setState({
            showToggle: !this.state.showToggle
        })
    }

    render() {
        const {item, onQuantityChange, onPress, onSoldOut} = this.props;
        const {showToggle} = this.state;
        if (item) {
            return (
                <TouchableWithoutFeedback onLongPress={this.toggleShowToggle.bind(this)} style={{flex:1}}>
                <View style={styles.container} {...this.props}>
                    {/*<View style={styles.thumbnailContainer}>*/}
                    {/*{item.icon ?*/}
                    {/*<CachedImage source={{uri: `${apiConfig.assetUrl}/${item.icon}`}} style={styles.thumbnail}/>*/}
                    {/*:*/}
                    {/*<Image source={require('../../../assets/images/defaultMenu.png')} style={styles.thumbnail}/>*/}
                    {/*}*/}
                    {/*</View>*/}
                        <View style={[styles.infoContainer, item.sold_out ? {opacity: 0.5} : null]}>
                            <AppText style={styles.title}>{item.name}</AppText>
                            <View style={styles.row}>
                                <AppText style={styles.price}>{numeral(item.price).format('$0.00')}</AppText>
                                {item.quantity ?
                                    <TouchableWithoutFeedback onPress={onPress} style={{marginLeft: 10}}>
                                        <Image source={require('../../../assets/images/edit.png')}
                                               style={styles.editIcon}/>
                                    </TouchableWithoutFeedback>
                                    : null}
                            </View>
                        </View>
                    {item.sold_out ? null :
                        <View style={styles.quantityContainer}>
                            <Ticker value={item.quantity} onChange={(quantity) => {
                                onQuantityChange(item, quantity)
                            }}/>
                        </View>
                    }
                    {showToggle ?
                        <View style={styles.toggleContainer}>
                            <TouchableWithoutFeedback onPress={()=>{this.toggleShowToggle(item.id)}}>
                                <View style={styles.closeButton}>
                                    <AppText style={styles.closeButtonText}>X</AppText>
                                </View>
                            </TouchableWithoutFeedback>
                            <Toggle onLeft={!item.sold_out} noElevation
                                    leftText={I18n.t('available')} rightText={I18n.t('soldOut')}
                                    onChange={()=>{
                                        this.toggleShowToggle();
                                        onSoldOut()
                                    }} />
                        </View>
                        : null}
                </View>
                </TouchableWithoutFeedback>
            );
        }
        return null;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        marginLeft: 10,
        marginRight: 10,
        borderBottomColor: '#DDD'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    thumbnailContainer: {
        flex: 0,
        padding: 10,
        paddingLeft: 0,
    },
    thumbnail: {
        width: 75,
        height: 75,
        borderRadius: 5,
    },
    editIcon: {
        marginLeft: 10,
        width: 25,
        height: 25,
    },
    infoContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        flex: 1,
    },
    title: {
        color: '#000',
        fontSize: 22,
        fontWeight: 'bold'
    },
    price: {
        color: '#000',
        fontSize: 20,
    },
    quantityContainer: {
        flex: 0,
    },
    toggleContainer:{
        position: 'absolute',
        top:0, bottom:0, left:0, right: 0,
        backgroundColor: 'rgba(255,255,255,0.9)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeButton:{
        backgroundColor: '#FF6E6E',
        width: 30,
        height:30,
        borderRadius: 30,
        position: 'absolute',
        top: 2,
        right: 2,
    },
    closeButtonText:{
        fontWeight: 'bold',
        color: '#FFF',
        fontSize: 25,
        lineHeight: 25,
        textAlign: 'center',
    }
});

export {MenuItem};