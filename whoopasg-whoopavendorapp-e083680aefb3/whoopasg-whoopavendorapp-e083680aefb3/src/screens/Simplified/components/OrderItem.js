import React from 'react';
import {View, StyleSheet} from 'react-native';
import numeral from 'numeral';
import {AppText, Customization} from '../../../components'

function renderCustomizations(customizations){
    const lastIdx = customizations.length - 1;
    return customizations.map((c, i)=>{
        return <Customization
            style={{width: 50}}
            iconStyle={{borderWidth:0, borderRightWidth: (i === lastIdx) ? 0 : 1, borderColor: '#CCC', width: 50}}
            customization={c.customization}
            state={0}
            size="small"
            key={`Customization${c.id}`}/>
    });
}

const OrderItem = (props) => {
    const { item } = props;
    if(item){
        return (
            <View style={styles.container} {...this.props}>
                <AppText style={styles.title}>{item.menu_item}</AppText>
                <AppText style={styles.quantity}>{item.size} x {numeral(item.quantity).format('00')}</AppText>
                <View style={styles.row}>
                {renderCustomizations(item.customizations)}
                </View>
            </View>
        );
    }
};
const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginBottom: 10,
    },
    title:{
        color: '#000',
        fontSize: 22,
        fontWeight: 'bold'
    },
    quantity:{
        color: '#000',
        fontSize: 20,
    },
    row:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start'
    }
});

export {OrderItem};