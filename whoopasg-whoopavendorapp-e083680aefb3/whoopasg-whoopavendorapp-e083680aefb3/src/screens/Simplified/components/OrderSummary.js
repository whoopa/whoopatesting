import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import numeral from 'numeral'
import {Actions} from 'react-native-router-flux'
import I18n from '../../../translations'
import {Button, Spinner, AppText} from '../../../components'

const renderItems = function (items) {
    return Object.keys(items).map(key => {
        const item = items[key];
        return (
            <View style={styles.row} key={`OrderItem${item.menuItemId}`}>
                <AppText style={styles.itemName}>{item.name}</AppText>
                <AppText style={styles.itemPrice}>{numeral(item.price).format('$0.00')} x</AppText>
                <AppText style={styles.itemQuantity}>{numeral(item.quantity).format('00')}</AppText>
            </View>
        );
    })
};

const OrderSummary = (props) => {
    const {order, orderNumber, onConfirmation} = props;
    return (
        <View style={styles.container} {...this.props}>
            {orderNumber ?
                <AppText style={styles.orderNumber}>{orderNumber}</AppText>
                : <Spinner />
            }
            <View style={styles.divider}/>
            <ScrollView>
            {renderItems(order)}
            </ScrollView>
            <View style={styles.divider}/>
            <View style={styles.row}>
                <AppText style={styles.totalLabel}>{I18n.t('total')}:</AppText>
                <AppText style={styles.totalValue}>{numeral(order.reduce((price, item)=> (price + item.price*item.quantity), 0)).format('$0.00')}</AppText>
            </View>
            <View style={styles.row}>
                <Button onPress={Actions.pop} title=""
                        className="buttonRoundedSecondary" textClassName="buttonSecondaryText">
                    {I18n.t('cancel')}
                </Button>
                <Button onPress={onConfirmation} title=""
                        className="buttonRounded" textClassName="buttonText">
                    {I18n.t('confirm')}
                </Button>
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        width: 360,
        backgroundColor: '#FFF',
        borderRadius: 10,
        padding: 20,
        paddingTop: 10,
        marginTop:10,
        marginBottom:10,
    },
    orderNumber: {
        fontSize: 70,
        lineHeight: 80,
        paddingBottom: 10,
        color: '#000',
        textAlign: 'center'
    },
    divider: {
        height: 1,
        backgroundColor: '#CCC',
        margin: 10,
        marginLeft:0,
        marginRight:0,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        marginBottom: 10,
    },
    itemName: {
        flex: 1,
        fontSize: 32,
        flexWrap: 'wrap',
        color: '#000',
    },
    itemPrice: {
        flexShrink: 1,
        fontSize: 16,
        marginBottom: 3,
        color: '#AAA'
    },
    itemQuantity: {
        flexShrink: 1,
        fontSize: 32,
        color: '#000',
        marginLeft: 5,
        alignItems: 'flex-end'
    },
    totalLabel: {
        flexGrow: 1,
        fontSize: 16,
        textAlign: 'right',
        color: '#AAA'
    },
    totalValue: {
        flexShrink: 1,
        fontSize: 28,
        marginLeft: 5,
        color: '#000'
    },
});

export {OrderSummary};