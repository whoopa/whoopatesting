import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {View, StyleSheet} from 'react-native';
import {OrderSummary} from './OrderSummary'
import {Calculator} from '../../../components'
import {confirmOrder} from '../actions'
import {getOrder, getTransformableOrder} from '../selectors'

class OrderConfirmationOverlay extends Component {
    render() {
        const {orderNumber, currentOrder, price, password, FCMToken, confirmOrder, displayCalculator, transformableOrderItems} = this.props;
        return (
            <View style={styles.overlay}>
                <View style={[styles.container, displayCalculator ? styles.alignRight : styles.alignCenter]}>
                    <OrderSummary order={currentOrder} orderNumber={orderNumber} onConfirmation={() => {
                        Actions.pop();
                        confirmOrder(password, FCMToken, currentOrder, transformableOrderItems, orderNumber);
                    }}/>
                    {displayCalculator ?
                        <Calculator total={price}/>
                        : null}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    overlay: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        paddingTop: 70,
        paddingBottom: 40,
        backgroundColor: "rgba(0,0,0,0.9)",
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    alignRight:{
        justifyContent: 'flex-end',
    },
    alignCenter:{
        justifyContent: 'center',
    }

});
const mapStateToProps = (state) => {
    const {password, FCMToken} = state.user;
    const currentOrder = getOrder(state.simplified);
    const transformableOrderItems = getTransformableOrder(state.simplified);
    const {orderNumber, displayCalculator} = state.orderHistory;
    const price = currentOrder ? currentOrder.reduce((price, item) => (price + item.price*item.quantity), 0) : 0;
    return {currentOrder, price, password, FCMToken, orderNumber, displayCalculator, transformableOrderItems};
};

export default connect(mapStateToProps, {confirmOrder})(OrderConfirmationOverlay);