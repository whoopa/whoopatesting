import React from 'react';
import {View, StyleSheet, TouchableOpacity, Animated} from 'react-native';
import {Spinner, AppText} from '../../../components'
import numeral from 'numeral';

class SubmitButton extends React.Component{
    // state = {
    //     left: new Animated.Value(0),  // Initial value for opacity: 0
    // };

    onButtonPress(){
        // Animated.timing(
        //     this.state.left,
        //     {
        //         toValue: 120,
        //         duration: 1000,
        //     }
        // ).start();
        this.props.onPress();
    }
    componentDidUpdate() {
        // const {quantity} = this.props;
        // if(quantity === 0){
        //     Animated.timing(
        //         this.state.left,
        //         {
        //             toValue: 0,
        //             duration: 1000,
        //         }
        //     ).start();
        // }
    }

    render(){
        // let { left } = this.state;
        const {quantity, amount, confirming} = this.props;
        return (
            <TouchableOpacity style={styles.container} onPress={this.onButtonPress.bind(this)} disabled={quantity === 0 || confirming}>
                <View style={styles.amount}>
                    <AppText style={styles.amountText}>{numeral(amount).format('$0.00')}</AppText>
                </View>
                <View style={[styles.quantity]}>
                    {confirming ?
                        <Spinner/>
                        : <AppText style={styles.quantityText}>{numeral(quantity).format('00')}</AppText>
                    }
                </View>
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 0,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginLeft: -80,
        flexDirection: 'row',
        padding: 20,
        paddingTop: 5,
        paddingBottom: 15
    },
    amount: {
        paddingLeft: 30,
        width: 140,
        height: 50,
        borderRadius: 25,
        borderWidth: 2,
        borderColor: '#FF6E6E',
        backgroundColor: '#FFF',
        elevation: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    amountText: {
        fontSize: 20,
        color: '#777',
    },
    quantity: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: 60,
        height: 60,
        borderRadius: 30,
        elevation: 5,
        backgroundColor: '#FF6E6E',
        alignItems: 'center',
        justifyContent: 'center'
    },
    quantityText: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#FFF'
    },
});

export {SubmitButton};