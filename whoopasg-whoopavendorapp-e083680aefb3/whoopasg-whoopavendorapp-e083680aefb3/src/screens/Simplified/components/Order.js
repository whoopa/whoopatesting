import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import numeral from 'numeral';
import {OrderItem} from './OrderItem'
import I18n from '../../../translations'
import { Spinner, AppText } from '../../../components'

const renderItems = function (items) {
    if (items) {
        return items.map(item => <OrderItem item={item} key={`OrderItem${item.id}`}/>)
    }
};
const Order = (props) => {
    const {order, onComplete} = props;
    return (
        <View style={styles.container} {...this.props}>
            <View style={styles.itemContainer}>
                {renderItems(order.items)}
            </View>
            <View style={styles.summaryContainer}>
                <View style={styles.infoContainer}>
                    <AppText style={styles.orderNumber}>{order.order_number}</AppText>
                    <View>
                        <AppText style={styles.total}>{I18n.t('total')}</AppText>
                        <AppText style={styles.price}>{numeral(order.price).format('$0.00')}</AppText>
                    </View>
                </View>
                { order.loading ? <Spinner style={{alignSelf: 'flex-start'}} /> :
                <TouchableOpacity style={[styles.button, order.is_online_order ? styles.onlineOrder : null]} onPress={onComplete} disabled={order.closing}>
                    {order.closing ? <Spinner /> :
                    <AppText style={styles.buttonText}>{I18n.t('serveButton')}</AppText>
                    }
                </TouchableOpacity>
                }
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        marginLeft: 10,
        marginRight: 10,
        borderBottomColor: '#DDD'
    },
    itemContainer: {
        padding: 10,
        paddingBottom: 0,
        flex: 1,
    },
    summaryContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        padding: 10,
    },
    infoContainer: {
        flex: 1,
        justifyContent: 'space-between',
    },
    orderNumber: {
        flex:1,
        color: '#000',
        fontSize: 22,
        fontWeight: 'bold'
    },
    total: {
        color: '#AAA',
        fontSize: 16,
    },
    price: {
        color: '#000',
        fontSize: 20,
    },
    button: {
        width: 70,
        height: 70,
        borderRadius: 40,
        backgroundColor: '#FF6E6E',
        justifyContent: 'center',
        elevation: 5,
    },
    onlineOrder:{
        backgroundColor: '#88FF88'
    },
    buttonText: {
        fontSize: 22,
        color: '#FFF',
        textAlign: 'center'
    }
});

export {Order};