import React from 'react';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {onConfirmCustomization} from '../actions'
import {getSelectedItem, calcItemPrice} from '../selectors'
import {AppText, Toggle, RadioGroup, Customization, Button} from '../../../components'
import {Ticker} from './Ticker'
import numeral from 'numeral'
import I18n from '../../../translations'

class EditItemOverlay extends React.Component {
    state = {};

    componentWillMount() {
        if(this.props.item){
            this.setState({
                ...this.props.item
            });
        }
    }

    onSizeChange(size){
        this.setState({
            ...this.state,
            size
        }, this.calcPrice.bind(this))
    }
    onQuantityChange(quantity){
        this.setState({
            ...this.state,
            quantity
        }, this.calcPrice.bind(this))
    }
    onTakeAwayToggle(){
        this.setState({
            ...this.state,
            takeAway: !this.state.takeAway
        }, this.calcPrice.bind(this))
    }

    onCustomizationToggle(cid,id){
        const item = this.state;
        let customizations = [];
        if(this.state.customizations){
            customizations = [...this.state.customizations];
        }

        const index =  customizations.indexOf(id);
        if(index >= 0){
            //If already exists, remove
            customizations.splice(index, 1);
        }else{
            //else remove existing ones from the set
            const set = item.customization_sets[cid];
            set.map(customization => {
                const index =  customizations.indexOf(customization.id);
                if(index >= 0){
                    customizations.splice(index, 1);
                }
            });
            //and add this one
            customizations.push(id);
        }
        this.setState({
            ...this.state,
            customizations
        }, this.calcPrice.bind(this));

    }

    calcPrice(){
        const price = calcItemPrice(this.state, this.props.takeAwayPrice);

        this.setState({
            ...this.state,
            price
        });
    }

    renderCustomization(cid, id, customization){
        const item = this.state;
        const isSelected = (item.customizations && item.customizations.includes(id)) ? 1 : 0;
        return <Customization
            style={{width: 90}}
            customization={customization}
            state={isSelected}
            onPress={()=>{this.onCustomizationToggle(cid, id)}}
            key={`Customization${id}`}/>
    }

    renderCustomizations(){
        const item = this.state;
        let customizationSets = Object.keys(item.customization_sets).map(cid=>{
            let set = {data: item.customization_sets[cid]};
            set.cid = cid;
            return set;
        });
        customizationSets = customizationSets.sort((a,b)=>(a.data.length < b.data.length));

        return customizationSets.map(set=>{
            if(set.data.length === 1){
                return this.renderCustomization(set.cid, set.data[0].id, set.data[0].customization)
            }else{
                return (
                    <View style={styles.customizationGroup} key={`Group${set.cid}`}>
                        {set.data.map(c => this.renderCustomization(set.cid, c.id, c.customization))}
                    </View>
                );
            }
        });
    }
    render() {
        const item = this.state;
        const {onConfirmCustomization} = this.props;

        let width = 400;
        const customizationSets = Object.keys(item.customization_sets).map(id=>(item.customization_sets[id]));
        // sort Customizations by number of customizations
        if(customizationSets.length > 0){
            const columns = customizationSets.reduce((columns, c)=>{
                if(c.length === 1){
                    return columns + 0.5;
                }
                return columns + Math.ceil(c.length / 2);
            }, 0);
            width = Math.max(width, Math.ceil(columns)*115);
        }
        return (
            <View  style={styles.overlay}>
            <View style={styles.container}>
                <View style={[styles.box, {width}]}>
                    <View style={styles.rowSpaced}>
                        <AppText style={styles.title}>{item.name}</AppText>
                    </View>
                    <View style={styles.rowSpaced}>
                        <AppText style={styles.price}>{numeral(item.price * item.quantity).format('$0.00')}</AppText>
                        <Toggle onLeft={!item.takeAway} noElevation
                                leftText={I18n.t('dineIn')} rightText={I18n.t('takeAway')}
                                onChange={this.onTakeAwayToggle.bind(this)}/>
                    </View>
                    <View style={styles.divider}/>

                    <View style={styles.rowSpaced}>
                        <RadioGroup items={item.sizes} value={item.size}
                                    onChange={this.onSizeChange.bind(this)}
                        />
                        <Ticker value={item.quantity} style={{flex: 0}}
                                onChange={this.onQuantityChange.bind(this)}
                        />
                    </View>
                    <View style={styles.divider}/>
                    {customizationSets.length > 0 ?
                    <View style={styles.cols}>
                        {this.renderCustomizations()}
                    </View>
                        : null}
                    <Button title="" onPress={()=>{onConfirmCustomization(this.state); Actions.pop();}}>
                        {I18n.t('confirm')}
                    </Button>
                </View>
                <TouchableOpacity style={styles.closeButton} onPress={Actions.pop}>
                    <AppText style={{color: '#FFF', fontSize: 20}}>X</AppText>
                </TouchableOpacity>
            </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    overlay: {
        position: "absolute",
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: "rgba(0,0,0,0.7)",
    },
    container: {
        padding: 15,
        flex: 1,
        margin: 25,
    },
    box: {
        borderRadius: 10,
        backgroundColor: '#FFF',
        padding: 30,
    },
    closeButton: {
        width: 30,
        height: 30,
        backgroundColor: '#FF6E6E',
        borderRadius: 15,
        position: 'absolute',
        top: 0,
        right: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rowSpaced: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        fontSize: 28,
        fontWeight: 'bold',
        color: '#000'
    },
    price: {
        fontSize: 22,
        color: '#777'
    },
    divider: {
        height: 1,
        backgroundColor: '#CCC',
        margin: 15,
        marginLeft: 0,
        marginRight: 0,
    },
    cols: {
        padding: 10,
        height: 200,
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        marginBottom: 10,
    },
    customizationGroup:{
        borderRightWidth: 1,
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        paddingRight: 5,
        marginRight: 5,
        borderColor: '#CCC',
        height: 180,
    },
});
const mapStateToProps = (state) => {
    const item = getSelectedItem(state.simplified);
    const {takeAwayPrice} = state.simplified;
    return {item, takeAwayPrice};
};

export default connect(mapStateToProps, {onConfirmCustomization})(EditItemOverlay);