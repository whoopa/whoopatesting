export const MENU_LOADING = 'simplified/menu/loading';
export const MENU_FAILED = 'simplified/menu/failed';
export const MENU_LOADED = 'simplified/menu/loaded';
export const ORDERS_LOADING = 'simplified/orders/loading';
export const ORDERS_FAILED = 'simplified/orders/failed';
export const ORDERS_LOADED = 'simplified/orders/loaded';
export const CONFIRMING_ORDER = 'simplified/order/confirming';
export const ORDER_CONFIRMED = 'simplified/order/confirm';
export const CONFIRMING_ORDER_FAILED = 'simplified/order/confirming/failed';
export const CATEGORY_SELECTED = 'simplified/category';
export const QUANTITY_CHANGED = 'simplified/menu/quantity';
export const ORDER_CLOSING = 'simplified/order/closing';
export const ORDER_CLOSED = 'simplified/order/closed';
export const ORDER_CLOSING_FAILED = 'simplified/order/closing/failed';
export const EDIT_ITEM = 'simplified/item/edit';
export const EDIT_ITEM_CONFIRMED = 'simplified/item/edit/confirm';
export const TOGGLE_SOLD_OUT = 'simplified/item/sold_out/toggle';

