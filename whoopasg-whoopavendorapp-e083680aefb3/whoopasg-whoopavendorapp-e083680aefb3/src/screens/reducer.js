import { reducer as loginReducer } from './Login/reducer';
import { reducer as menuReducer } from './Menu/reducer';
import { reducer as orderReducer } from './Orders/reducer';
import { reducer as orderHistoryReducer } from './Drawer/reducer';
import { reducer as simplifiedReducer } from './Simplified/reducer';

import { ActionConst } from 'react-native-router-flux';
const initialState = {};

const screenReducer = (state = initialState, action) => {
    switch (action.type) {
        // focus action is dispatched when a new screen comes into focus
        case ActionConst.FOCUS:
            return action.routeName;
        default:
            return state;
    }
};

export const reducer = {
    user: loginReducer,
    menu: menuReducer,
    kitchen: orderReducer,
    screen: screenReducer,
    orderHistory: orderHistoryReducer,
    simplified: simplifiedReducer
};