import React, { Component } from 'react';
import { View, Text } from 'react-native';

class GeoLocationWatch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            latitude: null,
            longitude: null,
            error: null,
        };
    }

    componentDidMount() {
        this.watchId = navigator.geolocation.watchPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });

                this.props.onLocation(position);
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 200000, maximumAge: 100000, distanceFilter: 10 },
        );
        console.log('Location Watch Started: ', this.watchId);
    }

    componentWillUnmount() {
        console.log('Location Watch Stopping: ', this.watchId);
        navigator.geolocation.clearWatch(this.watchId);
    }

    render() {
        return (
            <View />
        );
    }
}

export {GeoLocationWatch};