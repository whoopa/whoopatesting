import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = ({ size, style }) => {
    return (
        <View style={[styles.spinnerStyle, style]}>
            <ActivityIndicator size={size || 'small'} color='#333' />
        </View>
    );
};

const styles = {
    spinnerStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    }
};

export { Spinner };