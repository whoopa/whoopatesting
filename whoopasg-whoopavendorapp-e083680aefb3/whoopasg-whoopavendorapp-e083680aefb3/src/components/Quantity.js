import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import I18n from '../translations';
import numeral from 'numeral';
import {AppText} from './AppText'

class Quantity extends React.Component {
    state = {
        quantity: 1
    };

    componentWillMount(){
        this.setState({quantity: this.props.initialValue})
    }

    decQ() {
        if(this.state.quantity > 1){
            this.setState({quantity: this.state.quantity - 1},()=>{this.props.onChange(this.state.quantity);})
        }
    }
    incQ (maxValue){
        if(this.state.quantity < maxValue){
            this.setState({quantity: this.state.quantity + 1},()=>{this.props.onChange(this.state.quantity);})
        }
    }
    render() {
        const { container, textStyle, buttonContainer, quantityContainer, quantityTextStyle, title } = styles;
        const { maxValue } = this.props;
        return(
            <View>
              <View style={ container }>
                  <TouchableOpacity style={buttonContainer} onPress={() => this.decQ()}>
                      <AppText style={[textStyle,{paddingBottom: 40}]}>_</AppText>
                  </TouchableOpacity>
                  <View style={quantityContainer}>
                      <AppText style={quantityTextStyle}>{numeral(this.state.quantity).format('00')}</AppText>
                  </View>
                  <TouchableOpacity style={buttonContainer} onPress={() => this.incQ(maxValue)}>
                      <AppText style={[textStyle,{paddingBottom: 5}]}>+</AppText>
                  </TouchableOpacity>
              </View>
                <AppText style={title}>{I18n.t('quantity')} </AppText>
            </View>
        );
    }
}
const styles = {
    container: {
        height:60,
        width:200,
        borderWidth: 2,
        borderRadius: 50,
        flexDirection: 'row',
        borderColor: '#d9d9d9',
        padding:4,
    },
    buttonContainer: {
        width:60,
        alignSelf:'center',
        alignItems: 'center',
    },
    quantityContainer: {
        justifyContent: 'center',
          width:70,
          backgroundColor: '#000',
        paddingBottom: 3
    },
    quantityTextStyle:{
        fontSize: 35

        ,
        fontWeight: 'bold',
        color: '#FFF',
        alignSelf: 'center'
    },
    textStyle : {
        alignSelf:'center',
        fontSize:40,
        color: '#d9d9d9',
        fontWeight: 'bold'
    },
    title: {
        alignSelf:'center',
        fontSize: 20
    }
};
export {Quantity}