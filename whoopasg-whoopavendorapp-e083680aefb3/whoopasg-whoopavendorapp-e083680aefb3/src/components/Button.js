import React from 'react';
import { Spinner } from './Spinner';
import { AppText } from './AppText';
import {TouchableOpacity, StyleSheet, ActivityIndicator} from 'react-native';
const renderContent = (processing, children, textClassName) => {
    if(processing){
        return <Spinner />
    }
    if (typeof children === 'string' || children instanceof String){
        return <AppText style={textClassName ? styles[textClassName] : styles.buttonText}>{children}</AppText>
    }
    return children;
};
const Button = ({children, onPress, processing, className, textClassName}) => {
    return (
        <TouchableOpacity style={className ? styles[className] : styles.button} onPress={onPress}>
            {renderContent(processing, children, textClassName)}
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        padding: 10,
        elevation: 10,
        backgroundColor: '#FF6E6E',
        borderRadius: 5,
    },
    buttonWhite: {
        minWidth: 80,
        flex:0,
        padding: 10,
        elevation: 10,
        backgroundColor: '#FFF',
        borderRadius: 5,
        zIndex: 2,
        alignItems: 'center'
    },
    buttonSmall: {
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
        elevation: 6,
        backgroundColor: '#FF6E6E',
        borderRadius: 5,
    },
    buttonFill: {
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
        elevation: 5,
        backgroundColor: '#FF6E6E',
        borderRadius: 5,
        flex: 1
    },
    buttonRounded: {
        padding: 10,
        elevation: 5,
        backgroundColor: '#FF6E6E',
        borderRadius: 50,
        flex: 1,
        margin: 5
    },
    buttonRoundedSecondary: {
        padding: 10,
        borderColor: '#AAA',
        borderRadius: 50,
        borderWidth: 1,
        flex: 1,
        margin: 5
    },
    buttonText: {
        color: '#FFFFFF',
        textAlign: 'center'
    },
    buttonWhiteText: {
        color: '#FF6E6E',
        textAlign: 'center'
    },
    buttonSecondaryText: {
        color: '#AAA',
        textAlign: 'center'
    }
});

export { Button };