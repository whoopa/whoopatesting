import React from 'react';
import {Text} from 'react-native';

const AppText = (props) => {
    return (
        <Text style={[styles.font, props.style]}>
            {props.children}
        </Text>
    );
};

const styles = {
    font: {
        fontFamily: 'Nunito'
    }
};

export {AppText};