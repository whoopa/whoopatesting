import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {AppText} from './AppText'

class RadioGroup extends React.Component{
    renderButton(key, label){
        const isSelected = (parseInt(key) === this.props.value);
        return (
            <TouchableOpacity onPress={()=>this.props.onChange(key)} style={styles.button} key={`Radio${key}`}>
                <View style={isSelected ? styles.selected : styles.deselected} />
                <AppText style={isSelected ? styles.selectedText : styles.deselectedText} >{label}</AppText>
            </TouchableOpacity>
        );
    }
    renderButtons(){
        const {items} = this.props;
        return items.map(item => {
            return this.renderButton(item.id, item.name);
        });
    }
    render(){
        return (
            <View style={[styles.container, this.props.style]}>
                {this.renderButtons()}
            </View>
        );
    }
}

const styles = {
    container:{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    button:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 15,
        marginLeft: 15
    },
    selected: {
        borderRadius: 8,
        width: 16,
        height: 16,
        elevation: 5,
        backgroundColor: '#FF6E6E'
    },
    deselected: {
        borderRadius: 8,
        width: 16,
        height: 16,
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#777'
    },
    selectedText:{
        fontSize: 22,
        color: '#FF6E6E',
        marginLeft: 10
    },
    deselectedText:{
        fontSize: 22,
        color: '#999',
        marginLeft: 10
    },
};

export {RadioGroup};