import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import {AppText} from './AppText'

const Toggle = ({onLeft, leftText, rightText, onChange, noElevation}) => {
    return (
        <View style={styles.row}>
            <AppText style={onLeft ? styles.selectedText : styles.text}>{leftText.replace(' ','\n')}</AppText>
            <TouchableOpacity onPress={onChange}>
                <View style={[styles.toggle, (onLeft ? styles.onLeft : styles.onRight)]}>
                    <View style={[styles.toggleHandle, noElevation ? null : styles.elevation]} />
                </View>
            </TouchableOpacity>
            <AppText style={onLeft ? styles.text : styles.selectedText}>{rightText.replace(' ','\n')}</AppText>
        </View>
    );
};

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    toggle: {
        height: 35,
        width: 60,
        borderWidth: 1,
        backgroundColor: '#FFF',
        borderColor: '#FF6E6E',
        flexDirection: 'row',
        borderRadius: 35,
        margin: 5,
        alignItems: 'center'
    },
    toggleHandle: {
        height: 36,
        width: 40,
        backgroundColor: '#FF6E6E',
        borderRadius: 30,
    },
    elevation: {
        elevation: 10,
    },
    onLeft:{
        justifyContent: 'flex-start'
    },
    onRight:{
        justifyContent: 'flex-end'
    },
    text: {
        fontSize: 16,
        color: '#666',
        lineHeight: 20,
        textAlign: 'center'
    },
    selectedText: {
        fontSize: 16,
        color: '#FF6E6E',
        lineHeight: 20,
        textAlign: 'center'
    },
});

export {Toggle};