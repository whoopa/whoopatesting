import React from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import moment from 'moment';
import { VictoryLine,VictoryChart,VictoryGroup,VictoryTheme,VictoryAxis,VictoryContainer } from "victory-native";
import {groupBy} from '../utilities/ObjectUtils';
import {AppText} from './AppText';
import I18n from '../translations';

class ChartReport extends React.Component {
    state = {
        menuData: null,
        colors : null,
        items : null
    };
    componentWillMount() {
        const { items } = this.props;
        this.setState({items});
        this.renderData(items);
    }
    componentWillReceiveProps(newProps){
        let { items } = newProps;
        this.setState({items});
        this.renderData(items);
    }
    renderData(items){
        let menuData = [];
        items = groupBy(items, 'menu_item');
        // console.log('render',items);
        let colors = [];
        if(items && Object.keys(items).length > 0){
            Object.keys(items).map((key,i) => {
                let colorSymbol = '#' +  Math.random().toString(16).substr(-6);
                colors.push(colorSymbol);
                menuData.push({
                    'menu_item' : key,
                    'color' : colorSymbol,
                });
            });
            this.setState({menuData});
            this.setState({colors});
            this.setState({items});
        }else{
            this.setState({menuData: null});
            this.setState({colors: null});
        }
    }
    renderGraph() {
        // console.log('graph',this.state.items);
        let { items } = this.state;
        let { startDate, endDate  } = this.props;
        let xAxis = [];
        // let yAxis = ['10','20','30','40','50','60','70','80','90','100'];
        let currentDate = moment(startDate);
        let stopDate = moment(endDate);
        while (currentDate <= stopDate) {
            xAxis.push(moment(currentDate   ).format('D MMM') );
            currentDate = moment(currentDate).add(1, 'days');
        }
        const { colors } = this.state;
        if(items && Object.keys(items).length > 0){
            return Object.keys(items,colors).map((key,i) => {
                let data = [{x: 0, y: 0}];
                let sortedData = [];
                items[key].map((item) => {
                    // console.log('items',item);
                    sortedData.push({
                        'manu_item': item.menu_item,
                        'date': moment(item.update_timestamp * 1000).format("D MMM"),
                        'quantity': item.quantity
                    });
                });
                sortedData = groupBy(sortedData,'date');
                // console.log('dummy',sortedData);
                Object.keys(sortedData).map((key) => {
                    let qty = sortedData[key].reduce((prevVal, elem) => prevVal + elem.quantity, 0);
                    // console.log('qty',qty);
                    // console.log('key',key,'qty',qty);
                    data.push({
                        x: key,
                        y: qty
                    });
                });
                if (data) {
                    return (
                        <VictoryGroup key={i} color={colors[i]} data={data}>
                            <VictoryLine categories={{x: xAxis}}/>
                            {/*<VictoryScatter/>*/}
                        </VictoryGroup>
                    );
                }
            });
        }
        return (
            <VictoryGroup>
                <VictoryLine categories={{x: xAxis}}/>
            </VictoryGroup>
        );

    }
    renderMenuName() {
        let { menuData } = this.state;
        if(menuData){
            return menuData.map((menu,i) => {
                return (
                    <View key={i} style={[styles.options]}>
                        <View style={[styles.optionsCircle,{backgroundColor: menu.color}]}>
                        </View>
                        <View style={styles.optionsName}>
                            <AppText>{menu.menu_item}</AppText>
                        </View>
                    </View>
                );
            });
        }
        return null;
    }
    // clearChart(){
    //     this.setState({menuData: null});
    //     this.setState({items: null});
    // }
    render() {
        return(
            <ScrollView>
                <ScrollView>
                    <View style={[styles.menuNameWrapper]}>
                        {this.renderMenuName()}
                    </View>
                </ScrollView>
                <View  style={{ flexWrap: "wrap"}}>
                    <View style={styles.chartHeaderWrapper}>
                        <AppText style={styles.chartVolume}>{I18n.t('volume')}</AppText>
                        <View style={styles.chartDateRange}>
                            <AppText>{moment(this.props.startDate).format('DD/MM/YYYY')} - {moment(this.props.endDate).format('DD/MM/YYYY')}</AppText>
                        </View>
                    </View>
                    <VictoryChart padding={{ top:-50, left: 30, right: 40 }}
                        style={{ parent: { maxWidth: "100%",maxHeight: "100%" } }}
                        fixLabelOverlap={true}
                        theme={VictoryTheme.material}
                        containerComponent={<VictoryContainer standalone="false"/>}
                    >
                        <VictoryAxis fixLabelOverlap={true} style={{axis: {stroke: "#FF6E6E"},ticks: {stroke: "#FF6E6E"}}}/>
                        <VictoryAxis style={{ axis: {stroke: "#FF6E6E"}, ticks: {stroke: "none"}}} dependentAxis/>
                        {this.renderGraph()}
                    </VictoryChart>
                </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    options:{
        alignItems:'center',
        paddingTop:5,
        paddingBottom:5,
        flexDirection:'row',
    },
    optionsCircle:{
        width:20,
        height:20,
        borderRadius:100,
        marginRight:5
    },
    optionsName:{
        alignSelf: 'center',
        flexWrap: 'wrap',
        width:180,
    },
    chartHeaderWrapper:{
        paddingTop:30,
        paddingLeft:20,
        flexDirection:'row'
    },
    chartVolume:{
        color: "#FF6E6E"
    },
    chartDateRange:{
        flex:1,
        alignItems:'center',
        paddingRight:60
    },
    menuNameWrapper:{
        flexDirection:"row",
        flexWrap: 'wrap'
    }
});
export {ChartReport};