import React from 'react';
import { View, TouchableWithoutFeedback, StyleSheet, ScrollView} from 'react-native';
import { Actions } from 'react-native-router-flux';

class CategoryMenu extends React.Component {
    render(){
        const {container} = styles;
        return (
            <TouchableWithoutFeedback onPress={(evt)=>{Actions.menuOrdersOverlay()}}>
                <View style={container}>
                    <ScrollView>
                        {this.props.children}
                    </ScrollView>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 60,
        right: 0,
        backgroundColor: "rgba(0,0,0,0.9)",
    }
});
export { CategoryMenu };