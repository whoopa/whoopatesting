import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import { Spinner } from './Spinner';
import { AppText } from './AppText';
class KeyPad extends Component {
    state = {
        keyPadText : null,
    };
    componentWillMount() {
        const { initialValue } = this.props;
        if(initialValue){
            this.setState({
                keyPadText : initialValue,
            })
        }
    }
    addNumber(number) {
        if(this.state.keyPadText && this.state.keyPadText.length > 10){
            return;
        }
        this.setState({keyPadText: this.state.keyPadText === null ? String(number) : this.state.keyPadText + number })
    }
    removeNumber() {
        if(this.state.keyPadText && this.state.keyPadText.length > 1){
            this.setState({keyPadText:  this.state.keyPadText.substring(0, this.state.keyPadText.length - 1) })
        }else{
            this.setState({keyPadText: null})
        }
    }
    renderBox() {
        const { borderTopStyle, borderLeftStyle, borderRightStyle, boxText, boxButtonStyle } = styles;
        const {processing} = this.props;
        let numbers = [];
        for(let number = 1; number <= 9; number++) {
            numbers.push(<View key={number} style={[styles.box,borderTopStyle,borderLeftStyle, number % 3 === 0 ? borderRightStyle : null]}>
                        <TouchableOpacity style={boxButtonStyle} onPress={() => this.addNumber(number)} disabled={processing}>
                            <AppText style={[boxText]}>{number}</AppText>
                        </TouchableOpacity>
                    </View>);
        }
        return numbers;
    }
    render() {
        const { borderTopStyle, borderBottomStyle,borderLeftStyle, box, title, boxText, boxButtonStyle, borderNoneStyle } = styles;
        const { onEnter, secure, processing, compact } = this.props;
        let valueToDisplay = this.state.keyPadText;
        if(secure && valueToDisplay){
            valueToDisplay = "";
            for(let i=0; i<this.state.keyPadText.length; i++){
                valueToDisplay += "*";
            }
        }
        if(valueToDisplay === null){
            valueToDisplay = '-';
        }
        return (
            <View style={compact ? styles.compactContainer : styles.container}>
                <View style={{padding:20,alignItems:'center'}}>
                    {processing ? <Spinner /> :
                    <AppText style={compact ? styles.compact : title}>{valueToDisplay}</AppText>
                    }
                </View>
                <View style={styles.keyContainer}>
                    {this.renderBox()}
                    <View style={[box,borderLeftStyle,borderBottomStyle,borderTopStyle]}>
                        <TouchableOpacity style={[boxButtonStyle,{paddingTop:20}]} onPress={() => this.removeNumber()} disabled={processing}>
                            <AppText style={{color:'#FFF',fontSize: 20}}>Back</AppText>
                        </TouchableOpacity>
                    </View>
                    <View style={[box,borderLeftStyle,borderBottomStyle,borderTopStyle]}>
                        <TouchableOpacity style={boxButtonStyle} onPress={() => this.addNumber(0)}  disabled={processing}>
                            <AppText style={boxText}>0</AppText>
                        </TouchableOpacity>
                    </View>
                    <View style={[box,borderNoneStyle]}>
                        <TouchableOpacity style={[boxButtonStyle,{paddingTop:20}]} onPress={() => onEnter(this.state.keyPadText)}  disabled={processing}>
                            <AppText style={{color:'#00cc00',fontSize: 20}}>Enter</AppText>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height:450,
        width:320,
        backgroundColor: '#FF6E6E',
        alignSelf: 'center',
        borderWidth: 4,
        borderColor: '#FFF',
        borderRadius: 50,
        // flexDirection: 'row'
    },
    compactContainer: {
        height:400,
        width:320,
        backgroundColor: '#FF6E6E',
        alignSelf: 'center',
        borderWidth: 4,
        borderColor: '#FFF',
        borderRadius: 50,
        // flexDirection: 'row'
    },
    title: {
      color: '#FFF',
        fontSize: 32,
    },
    compact:{
        color: '#FFF',
        fontSize: 32,
        height: 20,
        lineHeight: 28,
    },
    keyContainer: {
        paddingLeft:0,
        paddingRight:0,
        alignItems : 'center',
        justifyContent: 'center',
        flexDirection:'row',
        flex:1,
        flexWrap: 'wrap',
    },
    box: {
        height:80,
        width:80,
        borderWidth:1,
        borderColor: '#FFF',
        alignItems:'center',
        justifyContent: 'center',
        margin:0
    },
    boxButtonStyle: {
        width: 70,
        height:70,
        alignItems:'center',
        paddingTop: 12
    },
    boxText : {
        color: '#FFF',
        fontSize: 32,
        fontWeight: 'bold'
    },
    borderTopStyle: {
        borderTopWidth:0
    },
    borderLeftStyle: {
        borderLeftWidth:0
    },
    borderRightStyle: {
        borderRightWidth:0
    },
    borderBottomStyle: {
        borderBottomWidth:0
    },
    borderNoneStyle: {
        borderWidth: 0
    }

});
export {KeyPad};