import React from 'react';
import {TouchableWithoutFeedback, StyleSheet, View} from 'react-native';
import { CachedImage } from 'react-native-cached-image';
import apiConfig from '../services/api/config';
import {AppText} from './AppText'

const Customization = ({ customization, state, onPress, size, style, iconStyle }) => {
    let icon = "";
    switch(state){
        case 0:
            icon = (size === 'big') ? require('../assets/images/defaultCustomizationBig.png') : require('../assets/images/defaultCustomization.png');
            if(customization.icon){
                icon = {uri: `${apiConfig.assetUrl}/${customization.icon}`};
            }
            break;
        case 1:
            icon = (size === 'big') ? require('../assets/images/defaultCustomizationSelectedBig.png') : require('../assets/images/defaultCustomizationSelected.png');
            if(customization.selected_icon){
                icon = {uri: `${apiConfig.assetUrl}/${customization.selected_icon}`};
            }else if(customization.icon){
                icon = {uri: `${apiConfig.assetUrl}/${customization.icon}`};
            }
            break;
        case 2:
            icon = require('../assets/images/defaultCustomizationInverted.png');
            if(customization.ordered_icon){
                icon = {uri: `${apiConfig.assetUrl}/${customization.ordered_icon}`};
            }else if(customization.icon){
                icon = {uri: `${apiConfig.assetUrl}/${customization.icon}`};
            }
            break;
    }
    return (
        <TouchableWithoutFeedback onPress={onPress ? onPress : null} key={'Customization'+customization.id}>
            <View style={[style, styles.container, size ? styles[`${size}Container`] : null]}>
                <View style={[(state!==2) ? styles.circle : null, (state===1) ? styles.circleAfter : null, size ? styles[`${size}Circle`] : styles.circleRegular, iconStyle]}>
                    <CachedImage
                        style={[size ? styles[`${size}Icon`] : styles.icon, (state===1) ? styles.iconAfter : null]}
                        source={icon} />
                </View>
                <AppText style={[styles.label, (state===1) ? styles.labelAfter : null, size ? styles[`${size}Label`] : null] }>{ customization.name }</AppText>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    container: {
        margin:5,
        alignItems: 'center',
    },
    bigContainer: {
        padding:5,
    },
    smallContainer: {
        margin:1,
    },
    icon: {
        width: 40,
        height:40,
        borderRadius: 50
    },
    bigIcon: {
        width: 60,
        height:60,
        borderRadius: 60
    },
    smallIcon: {
        width: 30,
        height:30
    },
    iconAfter:{},
    smallCircle:{
        width: 40,
        height: 40,
    },
    circleRegular:{
        width: 40,
        height: 40,
        borderRadius: 40,
    },
    bigCircle:{
        width: 60,
        height: 60,
        borderRadius: 32,
    },
    circle: {
        backgroundColor: '#FFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: '#FFF',
    },
    circleAfter: {
        elevation: 10,
        borderColor: '#000',
        backgroundColor: '#000'
    },
    label : {
        alignSelf: 'center',
        fontSize: 17,
        flexWrap: 'wrap',
        textAlign: 'center',
        lineHeight: 20,
    },
    bigLabel : {
    },
    smallLabel : {
        alignSelf: 'center',
        fontSize: 14,
        flexWrap: 'wrap',
        textAlign: 'center',
        lineHeight: 16,
    },
    labelAfter : {
        elevation: 10,
        color: '#000'
    }
});

export { Customization };