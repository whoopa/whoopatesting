import React from 'react';

import {View, TouchableWithoutFeedback, Text, StyleSheet} from 'react-native';
import {AppText} from './AppText'

class ItemSize extends React.Component{
    state = {
        selected : false,
    };
    render() {
        const { name, price, id, onPress,selectedSize } = this.props;
        const { circle, circleText, priceText, circleAfter, circleTextAfter, priceTextAfter } = styles;
        return(
                <TouchableWithoutFeedback onPress={onPress}>
                    <View>
                        <View style={[circle,selectedSize ? (selectedSize.id === id ? circleAfter: ''): '' ]} >
                            <AppText style={[circleText,selectedSize ? (selectedSize.id === id ? circleTextAfter : ''): '']} >{ name }</AppText>
                        </View>
                        <AppText style={[priceText,selectedSize ? (selectedSize.id === id ? priceTextAfter : '') : ''] }>${ price }</AppText>
                    </View>
                </TouchableWithoutFeedback>
        );
    }
}
const styles = {
    circle: {
        width: 60,
        height: 60,
        borderRadius: 100/2,
        borderWidth: 2,
        backgroundColor: '#FFF',
        borderColor: '#d9d9d9',
        marginLeft: 20
    },
    circleAfter: {
        backgroundColor: '#000',
    },
    circleText: {
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 30,
        paddingTop: 6
    },
    circleTextAfter: {
        color: '#FFF'
    },
    priceText : {
        alignSelf: 'center',
        fontSize: 20,
        paddingLeft:15
    },
    priceTextAfter : {
        color: '#000'
    }
};

export {ItemSize};