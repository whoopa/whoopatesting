import React from 'react';
import {ScrollView, View, StyleSheet, Image, Text} from 'react-native';
import apiConfig from '../services/api/config';
import I18n from '../translations'
import {Customization} from './Customization'
import {DoubleTapHandler} from './DoubleTapHandler'
import {Button} from './Button'
import {AppText} from './AppText'
import numeral from 'numeral';

const renderCustomizations = (customizations,items) => {
    if (customizations) {

        return customizations.map(c => <Customization
            customization={c.customization}
            state={items.status === 1 ? 2 : 0}
            key={'customization' + c.id}
        />);
    }
};

const getButtonLabel = (item) => {
    switch(item.status){
        case 0: return I18n.t('editButton');
        case 1: return I18n.t('serveButton');
        case 2: return <Image source={require('../assets/images/revert.png')} style={{height: 20}} />;
    }
};

const getButtonAction = (item, onEdit, onServe, onUndo) => {
    switch(item.status){
        case 0: return onEdit;
        case 1: return onServe;
        case 2: return onUndo;
    }
};

const OrderItemCard = ({item, onEdit, onServe, onUndo, altTitle, onTap, onDoubleTap, elevation, enableButtons = true}) => {
    let {container, overlay, title, options, columnContainer, columnGap, x, quantity} = styles;
    const isServed = item.status === 2;
    const buttonAction = getButtonAction(item, onEdit, onServe, onUndo);
    return (
        <DoubleTapHandler onTap={isServed ? null : onTap} onDoubleTap={isServed ? null : onDoubleTap}>
            <View style={[container, styles[`status${item.status}`], {elevation}]}>
                <View style={columnContainer}>
                    <AppText style={title}>{altTitle ? altTitle : item.menu_item}</AppText>
                    {enableButtons && buttonAction ? (
                    <Button
                        className="buttonWhite"
                        textClassName="buttonWhiteText"
                        title="" onPress={buttonAction}>
                        {getButtonLabel(item)}
                    </Button>
                    ) :null}
                </View>
                <View style={columnContainer}>
                    <AppText style={options}>{item.size} / {I18n.t((item.take_away ? 'takeAway' : 'dineIn'))}</AppText>
                    <AppText style={x}>x</AppText>
                    <AppText style={quantity}>{numeral(item.quantity).format('00')}</AppText>
                </View>
                <View style={[columnContainer, columnGap]}>
                    {renderCustomizations(item.customizations,item)}
                </View>
                {isServed ? (
                    <View style={overlay}>
                    </View>
                ) : null
                }
            </View>
        </DoubleTapHandler>
    );
};

const styles = StyleSheet.create({
    overlay: {
        position: 'absolute',
        width: 300,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255,255,255,0.8)',
        zIndex:1
    },
    container: {
        width: 300,
        flex: 0,
        padding: 20,
        elevation: 4,
    },
    status0: {
        backgroundColor: '#FFF'
    },
    status1: {
        backgroundColor: '#FF6E6E'
    },
    status2: {
        backgroundColor: '#FFF'
    },
    columnContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-end',
        flexWrap: 'wrap'
    },
    title: {
        fontWeight: 'bold',
        color: '#000',
        flex: 1,
        fontSize: 28
    },
    quantity: {
        fontWeight: 'bold',
        color: '#000',
        flex: 0,
        fontSize: 28,
        lineHeight: 30,
    },
    x: {
        flex: 0,
        fontSize: 18,
        color: '#000',
        lineHeight:40,
        marginRight: 10,
    },
    options: {
        color: '#000',
        flex: 2,
        fontSize: 20
    },
    takeAway: {
        padding: 3,
        paddingLeft: 10,
        borderBottomLeftRadius: 20,
        borderTopLeftRadius: 20,
        fontSize: 20,
        alignSelf: 'center'
    },
    takeAway0: {
        backgroundColor: '#FF6E6E',
        color: '#FFF',
    },
    takeAway1: {
        color: '#FF6E6E',
        backgroundColor: '#333'
    },
    takeAway2: {
        color: '#FF6E6E',
        backgroundColor: '#333'
    },
    columnGap: {
        marginTop: 10
    }
});

export {OrderItemCard};