import React, {Component} from 'react';
import {ScrollView, View, StyleSheet, TouchableWithoutFeedback, Dimensions} from 'react-native';
import ReactNativeComponentTree from 'react-native/Libraries/Renderer/shims/ReactNativeComponentTree';
import I18n from '../translations'
import {OrderItemCard} from './OrderItemCard'
import {Spinner} from './Spinner'
import {Button} from './Button'
import {AppText} from './AppText'
import {Actions} from 'react-native-router-flux'
import numeral from 'numeral';


const renderItems = (unpaidAmount, items, onTap, onDoubleTap, onButtonPress, onEdit, onComplete) => {
    const length = items.length;
    const numServed = items.filter(item => (item.status === 2)).length;
    const isLast = (length - numServed === 1);

    //Sort to push completed items at the bottom
    const sorted = items.sort((a, b) => {
        const ax = a.status === 2 ? 1 : 0;
        const bx = b.status === 2 ? 1 : 0;

        if(ax > bx) return 1;
        if(ax < bx) return -1;

        return a.id > b.id ? 1 : -1;
    });
    if (length) {
        return sorted.map((item, index) => <OrderItemCard
            item={item}
            elevation={(length + 1 - index)}
            key={'orderItem' + item.id}
            onEdit={onEdit ? (evt) => { onEdit(item) } : null}
            onServe={onButtonPress ? (e) => {
                if (isLast) {
                    const {width} = Dimensions.get('window');
                    const target = ReactNativeComponentTree.getInstanceFromNode(e.currentTarget);
                    target.measure((x,y,w,h,pageX,pageY)=>{
                        if(unpaidAmount === 0){
                            Actions.push('confirmationOverlay', {
                                message: I18n.t('confirmServedMessage'),
                                confirmText: I18n.t('yes'),
                                cancelText: I18n.t('no'),
                                onConfirmation: () => {
                                    onButtonPress(item);
                                    onComplete();
                                },
                                onCancel: () => {
                                },
                                showCalculator: false,
                                style: {right: (width - pageX - w), top:(pageY), position: 'absolute' }
                            });
                        }else{
                            Actions.push('confirmationOverlay', {
                                message: I18n.t('collect')+" "+numeral(unpaidAmount).format('$0.00'),
                                confirmText: I18n.t('yes'),
                                cancelText: I18n.t('no'),
                                onConfirmation: () => {
                                    onButtonPress(item);
                                    onComplete();
                                },
                                onCancel: () => {
                                    onButtonPress(item);
                                },
                                showCalculator: false,
                                style: {right: (width - pageX - w), top:(pageY), position: 'absolute' }
                            });
                        }
                    });
                } else {
                    onButtonPress(item);
                }
            }: null}
            onUndo={onButtonPress ? (evt) => { onButtonPress(item) } : null }
            onTap={ onTap ? (evt) => { onTap(item) } : null}
            onDoubleTap={ onDoubleTap ? (evt) => { onDoubleTap(item.menu_item_id) } : null }
        />)
    }
    return <Spinner />
};

const renderPrice = (items, price, paid, isPaid, onPress, onPay, processing) => {
    if(processing){
        return <Spinner size="large" />;
    }
    let complete = false;
    if (items && items.length > 0) {
        complete = (items.filter(item => {
                return item ? (item.status !== 2) : false;
            })).length === 0;
    }

    if (complete) {
        return (
            <Button className="buttonSmall" onPress={onPress} processing={processing} title="">
                <AppText style={styles.price}>{I18n.t('collect')} {numeral(price - paid).format('$0.00')}</AppText>
            </Button>
        );
    }

    if(isPaid){
        return (
            <View style={styles.column2}>
                <AppText style={styles.total}>{I18n.t('total')}</AppText>
                <AppText style={styles.price}>{numeral(price).format('$0.00')}</AppText>
                <AppText style={styles.paid}>{I18n.t('paid')}</AppText>
            </View>
        );
    }
    return (
        <Button className="buttonSmall" onPress={onPay} processing={processing} title="">
            <View style={{alignItems: 'center', width:90}}>
                <AppText style={styles.total}>{I18n.t('collect')}</AppText>
                <AppText style={styles.price}>{numeral(price - paid).format('$0.00')}</AppText>
            </View>
        </Button>
    )
};

const renderRemarks = (order) => {
    if (order.remarks) {
        return (
            <View style={[styles.remarksContainer, {elevation: (order.items.length + 2)}]}>
                <AppText style={styles.table}>{I18n.t('remarks')}</AppText>
                <AppText>{order.remarks}</AppText>
            </View>
        )
    }
    return null;
};
const confirmComplete = (onConfirmation, e) => {
    const {width} = Dimensions.get('window');
    const target = ReactNativeComponentTree.getInstanceFromNode(e.currentTarget);
    target.measure((x,y,w,h,pageX,pageY)=>{
        Actions.push('confirmationOverlay', {
            message: I18n.t('confirmPaid'),
            onConfirmation,
            onCancel: () => {
            },
            confirmText: I18n.t('yes'),
            cancelText: I18n.t('no'),
            showCalculator: false,
            style: {right: (width - pageX - w), top:(pageY), width: (280), position: 'absolute' }
        });
    });
};

class OrderCard extends Component {

    state = {collapsed: null};

    componentWillReceiveProps(nextProps) {
        if (nextProps.collapsible) {
            this.setState({collapsed: true});
        }
    }

    render() {
        const {order, onTap, onDoubleTap, onComplete, onButtonPress, onEdit, collapsible, elevation, onPay} = this.props;
        const {container, titleContainer, title, table, total, column, onlineOrder} = styles;
        let unpaidAmount = order.price - parseFloat(order.paid);
        if(unpaidAmount < 0){
            unpaidAmount = 0; //Handle edited orders where price becomes less
        }
        const isPaid = parseFloat(order.paid) >= order.price;
        const editCall = order.is_online_order ? null : onEdit;

        let containerStyle = container;
        if (collapsible) {
            containerStyle = [container, {elevation, marginTop: 0}]
        }

        return (
            <View style={containerStyle}>
                <TouchableWithoutFeedback onPress={() => {
                    collapsible ? this.setState({collapsed: !this.state.collapsed}) : false
                }}>
                    <View style={[titleContainer, order.is_online_order ? onlineOrder : null]}>
                        <View style={column}>
                            <AppText style={title}>{order.order_number ? order.order_number : '-'}</AppText>
                            <AppText
                                style={table}>{order.table_number ? I18n.t('table') + " " + order.table_number : ""}</AppText>
                        </View>
                        {renderPrice(order.items, order.price, order.paid, isPaid,
                            onComplete ? (e) => {confirmComplete(onComplete, e)} : null ,
                            onPay ? (e) => { confirmComplete(onPay, e)} : null ,
                            order.loading)}
                    </View>
                </TouchableWithoutFeedback>
                {this.state.collapsed ? null : (
                    <View style={{flex: -1}}>
                        {renderRemarks(order)}
                        <ScrollView>
                            {renderItems(unpaidAmount, order.items, onTap, onDoubleTap, onButtonPress, editCall, onComplete)}
                        </ScrollView>
                    </View>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
        width: 300,
        elevation: 3,
        flex: 0,
        marginLeft: 13,
        marginRight: 12,
        marginTop: 20
    },
    titleContainer: {
        backgroundColor: '#FFF',
        elevation: 10,
        padding: 10,
        paddingTop: 0,
        paddingBottom: 10,
        alignItems: 'flex-end',
        flexDirection: 'row',
        height: 75
    },
    onlineOrder: {
      backgroundColor: '#FF6E6E',
    },
    remarksContainer: {
        padding: 20,
        backgroundColor: '#FFF'
    },
    column: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    column2: {
        flex: 2,
        justifyContent: 'flex-end',
        paddingRight: 5,
    },
    title: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 30,
    },
    table: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 18,
    },
    price: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'right'
    },
    total: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 14,
        textAlign: 'right'
    },
    paid: {
        color: '#C44',
        fontWeight: 'bold',
        fontSize: 42,
        letterSpacing:-1,
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderColor: '#C44',
        position: 'absolute',
        height: 50,
        right: 0,
        lineHeight: 45,
        backgroundColor: 'rgba(255,255,255,0.7)'
    },
});
export {OrderCard};