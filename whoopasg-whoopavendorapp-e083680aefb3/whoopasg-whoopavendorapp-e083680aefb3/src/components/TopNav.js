import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {Actions} from 'react-native-router-flux'
import I18n from '../translations'
import {AppText} from './AppText'

const TopNav = ({orderTabClicked}) => {
    const {container, row2, activeButton, row, bottomBar, bottomBarAlt, button, buttonText, buttonTextAlt, buttonSmall, buttonSmallSelected, buttonSelected, buttonSelectedOrder, buttonImage, buttonImage2} = styles;

    const ordersTab = Actions.currentScene.includes('orders');
    const menuTab = Actions.currentScene.includes('menu');
    const drawerTab = Actions.currentScene.includes('DrawerOpen');
    return (
        <View style={container}>
            <View style={row}>
                <TouchableOpacity style={menuTab ? buttonSelected : button} onPress={Actions.menu}>
                    <View style={row2}>
                        <AppText style={buttonText}>{I18n.t('menuTitle')}</AppText>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={ordersTab ? buttonSelectedOrder : button} onPress={()=>{
                    if(Actions.currentScene === '_orders') {
                        orderTabClicked()
                    }
                    Actions.orders();
                }}>
                    <View style={row2}>
                        { ordersTab ? (
                            <TouchableOpacity style={activeButton} onPress={()=>{Actions.push('ordersProcessingItems')}}>
                                <Image style={buttonImage2} source={require("../assets/images/menu.png")}/>
                            </TouchableOpacity>
                        ) : null }
                        <AppText style={ordersTab ? buttonTextAlt : buttonText}>{I18n.t('ordersTitle')}</AppText>
                    </View>
                </TouchableOpacity>
                <View style={styles.separator} />
                <TouchableOpacity style={drawerTab ? buttonSmallSelected : buttonSmall} onPress={Actions.drawerOpen}>
                    <Image style={buttonImage} source={require("../assets/images/menu.png")}/>
                </TouchableOpacity>
            </View>
            <View style={ordersTab ? bottomBarAlt : bottomBar}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        height: 70,
        alignSelf: 'stretch',
    },
    activeButton: {
        padding: 5,
        backgroundColor: '#FF6E6E',
        width: 40,
        height: 40,
        borderRadius: 40,
        elevation: 11,
        margin: 20
    },
    row: {
        height: 60,
        flexDirection: 'row',
        elevation: 10,
        backgroundColor: '#000'
    },
    row2: {
        flexDirection: 'row'
    },
    bottomBar: {
        height: 10,
        elevation: 10,
        backgroundColor: '#FF6E6E',
    },
    bottomBarAlt: {
        height: 10,
        elevation: 10,
        backgroundColor: '#FFFFFF',
    },
    buttonSelected: {
        justifyContent: 'center',
        backgroundColor: '#FF6E6E',
        flex: 3,
    },
    buttonSelectedOrder: {
        justifyContent: 'center',
        backgroundColor: '#FFF',
        flex: 3
    },
    button: {
        justifyContent: 'center',
        flex: 3,
    },
    buttonSmall: {
        justifyContent: 'center',
        flex: 1
    },
    buttonSmallSelected: {
        backgroundColor: '#FF6E6E',
        justifyContent: 'center',
        flex: 1
    },
    buttonText: {
        textAlign: 'center',
        flexGrow: 1,
        color: '#FFF',
        fontSize: 36,
        alignSelf: 'center',
    },
    buttonTextAlt: {
        textAlign: 'center',
        flexGrow: 1,
        color: '#FF6E6E',
        fontSize: 36,
        alignSelf: 'center',
        marginRight: 80,
    },
    buttonImage: {
        width: 40,
        height: 40,
        alignSelf: 'center'
    },
    buttonImage2: {
        width: 28,
        height: 28,
        alignSelf: 'center'
    },
    separator: {
        width: 1,
        margin: 10,
        marginLeft:0,
        marginRight:0,
        flex: 0,
        backgroundColor: '#AAA'
    }
});

export {TopNav};