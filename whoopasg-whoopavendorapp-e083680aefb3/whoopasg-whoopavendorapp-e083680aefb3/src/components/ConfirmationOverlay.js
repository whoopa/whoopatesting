import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {Calculator} from "./Calculator";
import {AppText} from "./AppText";

const ConfirmationOverlay = ({message, onConfirmation, onCancel, confirmText, cancelText, style, showCalculator}) => {
    const { container, box, row, messageText, button, confirmButton, cancelButton, confirmButtonText, cancelButtonText } = styles;
    return (
        <View style={container}>
            <View style={[box, style]}>
                <AppText style={messageText}>
                    {message}
                </AppText>
                <View style={row}>
                    <TouchableOpacity style={[button,confirmButton]} onPress={()=>{Actions.pop(); onConfirmation()}}>
                        <AppText style={confirmButtonText}>{confirmText}</AppText>
                    </TouchableOpacity>
                    <TouchableOpacity style={[button,cancelButton]} onPress={() => {Actions.pop(); onCancel();}}>
                        <AppText style={cancelButtonText}>{cancelText}</AppText>
                    </TouchableOpacity>
                </View>
            </View>
            {showCalculator ? <Calculator total={showCalculator.price} style={showCalculator.style}/> : null}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "rgba(0,0,0,0.9)",
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 30,
        padding: 20,
    },
    box:{
        backgroundColor: '#FFF',
        justifyContent: 'space-between',
        padding: 10,
        borderRadius: 5
    },
    row: {
        flexDirection: 'row',
    },
    messageText: {
        fontSize: 24,
        marginBottom: 20,
    },
    button:{
        paddingLeft: 20,
        paddingRight: 20,
        padding: 10,
        elevation: 10,
        borderRadius: 5,
        margin: 10
    },
    confirmButton: {
        backgroundColor: '#FF6E6E'
    },
    cancelButton: {
        backgroundColor: '#FFFFFF',
    },
    confirmButtonText: {
        color: '#FFFFFF',
    },
    cancelButtonText: {
        color: '#333',
    }
});

export { ConfirmationOverlay };