import React from 'react';
import {TextInput, View, Text} from 'react-native';
import {AppText} from './AppText'

const Input = ({label, value, onChangeText, onSubmitEditing, placeholder, password = false, autoCorrect = false}) => {
    const {inputStyle, labelStyle, container, rowContainer} = styles;

    const renderLabel = () => {
        if (label) {
            return (
                <AppText style={labelStyle}>{label}</AppText>
            )
        }
        return null;
    };

    return (
        <View style={container}>
            <View style={rowContainer}>
                {renderLabel()}
                <TextInput
                    underlineColorAndroid='transparent'
                    secureTextEntry={password}
                    placeholder={placeholder}
                    autoCorrect={autoCorrect}
                    style={inputStyle}
                    value={value}
                    onChangeText={onChangeText}
                    onSubmitEditing={onSubmitEditing}
                />
            </View>
        </View>
    );
};

const styles = {
    inputStyle: {
        color: '#000',
        padding: 5,
        fontSize: 18,
        flex: 1,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#FF6E6E',
        marginBottom: 10
    },
    labelStyle: {
        fontSize: 18,
        flex: 1,
        paddingLeft: 20
    },
    container: {
        height: 50,
        alignItems: 'center'
    },
    rowContainer: {
        height: 50,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    }
};

export {Input};