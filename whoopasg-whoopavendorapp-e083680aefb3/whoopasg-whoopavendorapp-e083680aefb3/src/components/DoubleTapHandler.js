'use strict';

import React, {Component} from 'react';
import {TouchableWithoutFeedback, View} from 'react-native';

class DoubleTapHandler extends Component {

    timeout = null;

    componentWillUnmount(){
        //cleanup
        if(this.timeout !== null){
            clearTimeout(this.timeout);
            this.timeout = null;
        }
    }

    onPress = (e) => {
        const {onTap, onDoubleTap} = this.props;
        if(onDoubleTap){ // Check if doubleTap needs to be responded to?
            if(this.timeout === null){
                let self = this;
                this.timeout = setTimeout(()=>{
                    self.timeout = null;
                    onTap ? onTap(e) : null;
                }, 500);
            }else{
                clearTimeout(this.timeout);
                this.timeout = null;
                onDoubleTap ? onDoubleTap(e) : null;
            }
        }else{
            onTap ? onTap(e) : null;
        }
    };

    render() {
        return (<TouchableWithoutFeedback onPress={this.onPress}  {...this.props} />);
    }
}

export { DoubleTapHandler };
