import React from 'react';
import { View } from 'react-native';
import { Customization } from './Customization';

class MenuItemCustomization extends React.Component{
    renderCustomizations(){
        const { customizations, onPress, selected } = this.props;
        return customizations.map(c => {
            let isSelected = selected && selected.includes(parseInt(c.id));
            return(
                <Customization
                    size="big"
                    onPress={()=>{onPress(c, c.price)}}
                    key={`Customization${c.id}`}
                    state={isSelected ? 1 : 0}
                    customization={c.customization} />
            );
        });
    }

    render() {
        const { customizations } = this.props;
        return(
            <View style={customizations.length > 1 ? styles.fixedContainer : null}>
                {this.renderCustomizations()}
            </View>
        );
    }
}
const styles = {
    fixedContainer: {
        height: 216,
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    }
};

export {MenuItemCustomization};