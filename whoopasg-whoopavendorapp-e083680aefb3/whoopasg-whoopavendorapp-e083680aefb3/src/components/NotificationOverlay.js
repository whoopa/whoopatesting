import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {Button} from './Button';
import {Actions} from 'react-native-router-flux';
import {AppText} from './AppText'

class NotificationOverlay extends Component{
    state = {isProcessing: false};

    componentWillReceiveProps(nextProps){
        if(nextProps.processing){
            this.setState({isProcessing: nextProps.processing});
        }
    }

    render(){
        const {message, button, opaque, action} = this.props;
        const {container, box, row, messageStyle, buttonTextStyle} = styles;
        return (
            <View style={[container, opaque ? styles.opaque : styles.translucent]}>
                <View style={box}>
                    <AppText style={messageStyle}>
                        {message.toString()}
                    </AppText>
                    <View style={row}>
                        <Button onPress={() => {
                            if (action) {
                                this.setState({isProcessing: true});
                                action();
                            } else {
                                Actions.pop();
                            }
                        }} processing={this.state.isProcessing} title="">
                            <AppText style={buttonTextStyle}>{button}</AppText>
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: "center",
        alignItems: "center",
        padding: 30,
        paddingBottom: 20,
    },
    translucent: {
        backgroundColor: "rgba(0,0,0,0.9)",
    },
    opaque: {
        backgroundColor: "#000",
    },
    box: {
        backgroundColor: '#FFF',
        maxWidth: '50%',
        justifyContent: 'space-between',
        padding: 20,
        borderRadius: 5
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    messageStyle: {
        fontSize: 24,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
    }
});

export {NotificationOverlay};