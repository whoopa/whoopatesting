import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {Actions} from 'react-native-router-flux'
import I18n from '../translations'
import {AppText} from './AppText'

const TopBar = () => {
    const {container, buttonText, elevated, buttonImage, buttonSmall, backgroundImage} = styles;
    return (
        <View style={container}>
            <Image style={backgroundImage} source={require("../assets/images/nav-bg.png")}/>
            <View style={[styles.block, elevated]}>
                <Image style={backgroundImage} source={require("../assets/images/nav-bg-left.png")}/>
                <AppText style={[buttonText]}>{I18n.t('menuTitle')}</AppText>
                <Image source={require("../assets/images/logo-exclamation.png")}/>
            </View>
            <View style={styles.block}>
                <AppText style={buttonText}>{I18n.t('ordersTitle')}</AppText>
                <Image source={require("../assets/images/logo-exclamation.png")}/>
            </View>
            <TouchableOpacity style={buttonSmall} onPress={Actions.drawerOpen}>
                <Image style={buttonImage} source={require("../assets/images/menu.png")}/>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    backgroundImage: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
    },
    container: {
        height: 70,
        flexDirection: 'row',
        alignSelf: 'stretch',
        backgroundColor: '#FF6E6E',
        elevation: 5,
    },
    elevated: {
        backgroundColor: '#FFF',
        elevation: 10,
    },
    block: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFF',
        fontSize: 36,
        lineHeight: 60,
        height: 70,
        alignSelf: 'center',
    },
    buttonSmall: {
        position: 'absolute',
        justifyContent: 'center',
        right: 10,
        top: 15,
    },
    buttonImage: {
        width: 40,
        height: 40,
        alignSelf: 'center'
    },
});

export {TopBar};