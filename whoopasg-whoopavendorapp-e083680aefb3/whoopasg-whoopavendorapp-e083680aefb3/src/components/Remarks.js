import React, {Component} from 'react';
import { Actions } from 'react-native-router-flux';
import I18n  from '../translations';
import {View, StyleSheet, TouchableOpacity,TextInput} from 'react-native';
import {AppText} from './AppText'

class Remarks extends Component {
    state = {
        remarkText: null
    };
    componentWillMount() {
        const { initialValue } = this.props;
        this.setState({
            remarkText : initialValue,
        })
    }
    render() {
        const { inputStyle,buttonStyle,remarksWrapper, buttonText } = styles;
        const { onSubmit } = this.props;
        return (
            <View style={remarksWrapper}>
                <AppText>{I18n.t('remarks')}: </AppText>
                <TextInput
                    style={inputStyle}
                    multiline={true}
                    numberOfLines={10}
                    underlineColorAndroid={'transparent'}
                    value={this.state.remarkText}
                    onChangeText={(remarkText)=> this.setState({remarkText})}
                />
                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity style={buttonStyle} onPress={() => onSubmit(this.state.remarkText)}>
                        <AppText style={buttonText}>Submit</AppText>
                    </TouchableOpacity>
                    <TouchableOpacity style={buttonStyle} onPress={() => Actions.push('menuOrdersOverlay')}>
                        <AppText style={buttonText}>Cancel</AppText>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    remarksWrapper: {
        width:500,
        borderWidth:2,
        borderColor: '#FFF',
        backgroundColor: '#FFF',
        padding:10,
        paddingTop: 15,
        paddingBottom: 15,
    },
    inputStyle: {
        color: '#000',
        padding: 5,
        fontSize: 18,
        borderWidth: 1,
        borderRadius: 5,
        borderColor:'#d9d9d9',
        textAlignVertical: 'top',
    },
    buttonStyle: {
        padding: 10,
        elevation: 10,
        backgroundColor: '#FF6E6E',
        borderRadius: 5,
        width:100,
        marginTop:10,
        marginRight: 10,
    },
    buttonText: {
        fontSize: 20,
        alignSelf: 'center',
        paddingTop: 0,
        paddingBottom: 0,
        color: '#FFF'
    },
});
export {Remarks};