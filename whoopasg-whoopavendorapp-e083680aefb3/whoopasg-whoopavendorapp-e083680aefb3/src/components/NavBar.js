import React, {Component} from 'react';
import { TopBar } from './TopBar';
import { TopNav } from './TopNav';
import { OrderHeaderBar } from './OrderHeaderBar';
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux';
import {orderTabClicked,sortOrder,changeView} from '../screens/Drawer/actions';

class NavBar extends Component {    state = {scene: null};

    componentWillUpdate(){
        const scene = Actions.currentScene;
        if(scene !== this.state.scene && scene !== 'DrawerOpen' && !scene.includes('Overlay')){
            this.setState({scene})
        }
    }

    render() {
        const { toggle,selected,sortOrder,selectedTableHead, changeView,currentView} = this.props;
        //console.log("Actions.currentScene", Actions.currentScene, this.state.scene);
        switch(this.state.scene){
            case '_simplifiedApp':
                return <TopBar/>;
            case '_orderReport':
            case 'columnSelectionOverlay':
                return <OrderHeaderBar selectedHeaders={selectedTableHead} toggle={toggle} selected={selected} onSort={sortOrder} changeView={changeView} currentView={currentView} />;
            default:
                return <TopNav orderTabClicked={this.props.orderTabClicked}/>;

        }
    }
}
const mapStateToProps = (state) => {
    const {selected, toggle, selectedTableHead, changeView, currentView } = state.orderHistory;
    return {selected, toggle, selectedTableHead, changeView, currentView };
};
export default connect(mapStateToProps, {orderTabClicked,sortOrder,changeView})(NavBar);