import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text, Image} from 'react-native';
import {Actions} from 'react-native-router-flux'
import {AppText} from './AppText';
import I18n from '../translations';

class OrderHeaderBar extends React.Component{
    renderArrow(key){
        const { selected, toggle } = this.props;
        if(key === selected){
            if(toggle){
                return <Image source={require("../assets/images/circle-down-arrow.png")}  />
            }
            return <Image source={require("../assets/images/circle-up-arrow.png")} style={{marginTop:-1}}/>
        }

    }

    renderHead(){
        const { head, headText, circle, bgColor,headFirstColumn } = styles;
        const { selected,onSort, selectedHeaders, currentView } = this.props;
        if(selectedHeaders && currentView === 'listing' ){
            return Object.keys(selectedHeaders).map((key,i) => {
                if(i === 0){
                 return(
                     <TouchableOpacity key={key} onPress={()=>onSort(key)} style={[headFirstColumn]}>
                        <AppText style={headText}>{selectedHeaders[key]}</AppText>
                        <View style={{alignItems:'flex-end',flex:1,paddingRight:2}}>
                             <View style={[circle,key === selected ? bgColor : null]}>
                                 <View style={{alignItems:'center'}}>
                                     {this.renderArrow(key)}
                                 </View>
                             </View>
                        </View>
                     </TouchableOpacity>
                 );
                }else{
                   return(
                       <TouchableOpacity key={key} onPress={()=>onSort(key)} style={head}>
                           <AppText style={headText}>{selectedHeaders[key]}</AppText>
                           <View style={[circle,key === selected ? bgColor : null]}>
                               <View style={{alignItems:'center'}}>
                                   {this.renderArrow(key)}
                               </View>
                           </View>
                       </TouchableOpacity>
                   );
                }
            });
        }
        return null;
    }

    renderMenuColumnSetting(){
        const { currentView } = this.props;
        if(currentView === 'graph'){
            return <View style={{justifyContent:'center',paddingLeft:15}}>
                     <AppText style={{color:'#FFF'}}>{I18n.t('dish')}</AppText>
                   </View>
        }
        return null;
    }
    renderIcon() {
        const { changeView, currentView } = this.props;
        const icons = ['listing','graph'];
        return icons.map((icon,i)=> {
            return (<TouchableOpacity key={i} style={styles.leftHeaderCustomizationWrapper} onPress={() =>changeView(icon)}>
                        <View style={[styles.customizationIcon,currentView === icon ? styles.changeBg : null]}>
                            {this.renderIconImages(icon,currentView)}
                        </View>
                   </TouchableOpacity>);
        })
    }
    renderIconImages(icon,currentView){
        switch(icon){
            case 'listing':
                if(currentView ==='listing'){
                    return <Image source={require("../assets/images/listing-color.png")}/>
                }
                return <Image source={require("../assets/images/listing.png")}/>;
            case 'graph':
                if(currentView ==='graph'){
                    return <Image source={require("../assets/images/graph-color.png")}/>
                }
                return <Image source={require("../assets/images/graph.png")}/>;
        }

    }
    render(){
        return (
            <View style={{flexDirection: 'row'}}>
                <View style={styles.containerLeft}>
                    <View style={styles.leftHeader}>
                        <View style={styles.leftHeaderBackBtn}>
                            <TouchableOpacity onPress={()=> Actions.pop()} style={{width:70}} >
                                <Image source={require("../assets/images/back-arrow.png")}/>
                            </TouchableOpacity>
                        </View>
                        {this.renderIcon()}
                    </View>
                </View>
                <View style={styles.containerRight}>
                    <View style={[styles.container,{paddingRight:2}]}>
                            <TouchableOpacity style={styles.menuSelectionIcon} onPress={()=>Actions.push('columnSelectionOverlay')}>
                                <Image source={require("../assets/images/menuIcon.png")}/>
                            </TouchableOpacity>
                            {this.renderMenuColumnSetting()}

                        <View style={styles.headerWrapper}>
                            {this.renderHead()}
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    containerLeft: {
        height: 70,
        width: 300,
    },
    container:{
        height:70,
        flexDirection:'row',
        backgroundColor: '#FF6E6E',
        paddingLeft:4,
        // paddingRight:10,
        flex:1,
    },
    containerRight: {
        borderLeftColor: '#FF6E6E',
        height: 70,
        flex:1,
    },
    leftHeader:{
        height:70,
        backgroundColor: '#FF6E6E',
        flexDirection:'row',
        paddingLeft:10,
        paddingRight:10
    },
    leftHeaderBackBtn:{
        flex:1,
        justifyContent:'center'
    },
    leftHeaderCustomizationWrapper:{
        justifyContent:'center',
        alignItems: 'center',
        alignContent:'center',
        marginRight:10,
        // flex:1
    },
    customizationIcon:{
        width:50,
        height:50,
        borderRadius:100,
        borderWidth:1,
        borderColor: '#FFF',
        justifyContent:'center',
        alignItems:'center'
    },
    circle: {
        width:20,
        height:20,
        borderRadius: 100,
        marginLeft:5,
        borderColor:'#FFF',
        borderWidth: 1
    },
    bgColor:{
        backgroundColor:'#FFF'
    },
    menuSelectionIcon:{
        height:30,
        width:30,
        alignSelf:'center',
        justifyContent: 'center',
        paddingLeft:5
    },
    changeBg:{
        backgroundColor: '#FFF'
    },
    headerWrapper: {
        flex:1,
        flexDirection: 'row'
    },
    headFirstColumn: {
        flex:3,
        paddingLeft:15,
        alignItems: 'center',
        flexDirection:'row'
    },
    head: {
        // borderWidth:2,
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row'
    },
    headText: {
        color: '#FFF'
    }
});

export {OrderHeaderBar};