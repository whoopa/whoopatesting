import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text, ScrollView} from 'react-native';
import {Actions} from 'react-native-router-flux'
import {AppText} from './AppText'

const renderItems = function (items, onPress, selected) {
    const {buttonText, button, buttonTextSelected, selectedTab} = styles;
    if (items) {
        let view = [];
        view.push();
        items.map(item => {
            if (item.id === selected) {
                view.push(
                    <TouchableOpacity style={button} onPress={Actions.drawerOpen} key={`Cat${item.id}`}>
                        <AppText style={[buttonText, buttonTextSelected]}>{item.name}</AppText>
                        <View style={selectedTab}/>
                    </TouchableOpacity>
                );
            } else {
                view.push(
                    <TouchableOpacity style={button} onPress={() => {
                        onPress(item.id)
                    }} key={`Cat${item.id}`}>
                        <AppText style={[buttonText]}>{item.name}</AppText>
                    </TouchableOpacity>
                );
            }
        });
        return view;
    }
};
const TabNav = ({items, onPress, selected}) => {
    const {container, scroller, contentContainer, line, lineActive} = styles;
    return (
        <View style={container}>
            <View style={items ? lineActive : line}/>
            <ScrollView horizontal style={scroller}>
                <View style={contentContainer}>
                    {renderItems(items, onPress, selected)}
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 0,
        height: 55,
        padding: 10,
    },
    scroller: {
        left: 10,
        top: 10,
        position: 'absolute',
        zIndex:2
    },
    contentContainer: {
        height: 45,
        flexDirection: 'row',
        width: '100%'
    },
    buttonText: {
        marginLeft: 10,
        marginRight: 10,
        opacity: 0.35,
        flex: 1,
        textAlign: 'center',
        color: '#000',
        fontSize: 24,
        alignSelf: 'center',
    },
    buttonTextSelected: {
        opacity: 1,
    },
    selectedTab: {
        height: 6,
        borderRadius: 3,
        flex: 1,
        backgroundColor: '#FF6E6E',
        position: 'absolute',
        width: '100%',
        bottom: 0
    },
    lineActive: {
        zIndex:1,
        height: 2,
        borderRadius: 2,
        flex: 1,
        backgroundColor: 'rgba(255,110,110,0.35)',
        position: 'absolute',
        width: '100%',
        left: 10,
        bottom: 2
    },
    line: {
        zIndex:2,
        height: 2,
        borderRadius: 2,
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.2)',
        position: 'absolute',
        left: 10,
        width: '100%',
        bottom: 2
    }
});

export {TabNav};