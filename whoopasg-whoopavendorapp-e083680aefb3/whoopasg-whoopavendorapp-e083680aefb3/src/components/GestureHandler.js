'use strict';

import React, {Component} from 'react';
import {View, PanResponder} from 'react-native';

const swipeConfig = {
    velocityThreshold: 0.3,
    directionalOffsetThreshold: 80
};

function isValidSwipe(velocity, velocityThreshold, directionalOffset, directionalOffsetThreshold) {
    return Math.abs(velocity) > velocityThreshold && Math.abs(directionalOffset) < directionalOffsetThreshold;
}

class GestureHandler extends Component {
    static SWIPE_UP = 'SWIPE_UP';
    static SWIPE_DOWN = 'SWIPE_DOWN';
    static SWIPE_LEFT = 'SWIPE_LEFT';
    static SWIPE_RIGHT = 'SWIPE_RIGHT';

    constructor(props, context) {
        super(props, context);
        this.swipeConfig = Object.assign(swipeConfig, props.config);
    }

    componentWillReceiveProps(props) {
        this.swipeConfig = Object.assign(swipeConfig, props.config);
    }

    componentWillMount() {
        const shouldSetResponder = this._handleShouldSetPanResponder.bind(this);
        const responderMove = this._handlePanResponderMove.bind(this);
        const responderEnd = this._handlePanResponderEnd.bind(this);
        this._panResponder = PanResponder.create({ //stop JS beautify collapse
            onStartShouldSetPanResponder: shouldSetResponder,
            onMoveShouldSetPanResponder: shouldSetResponder,
            onPanResponderMove: responderMove,
            onPanResponderRelease: responderEnd,
            onPanResponderTerminate: responderEnd
        });
    }

    _handleShouldSetPanResponder(evt, gestureState) {
        return evt.nativeEvent.touches.length === 1 && !this._gestureIsClick(gestureState);
    }

    _gestureIsClick(gestureState) {
        return Math.abs(gestureState.dx) < 5  && Math.abs(gestureState.dy) < 5;
    }

    _handlePanResponderMove(evt, gestureState) {
        const swipeDirection = this._getSwipeDirection(gestureState);
        this._triggerMoveHandlers(swipeDirection, gestureState);
    }

    _handlePanResponderEnd(evt, gestureState) {
        const swipeDirection = this._getSwipeDirection(gestureState);
        this._triggerSwipeHandlers(swipeDirection, gestureState);
    }

    _triggerSwipeHandlers(swipeDirection, gestureState) {
        const {onSwipe, onSwipeUp, onSwipeDown, onSwipeLeft, onSwipeRight} = this.props;
        const {SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN} = GestureHandler;
        onSwipe && onSwipe(swipeDirection, gestureState);
        switch (swipeDirection) {
            case SWIPE_LEFT:
                onSwipeLeft && onSwipeLeft(gestureState);
                break;
            case SWIPE_RIGHT:
                onSwipeRight && onSwipeRight(gestureState);
                break;
            case SWIPE_UP:
                onSwipeUp && onSwipeUp(gestureState);
                break;
            case SWIPE_DOWN:
                onSwipeDown && onSwipeDown(gestureState);
                break;
        }
    }

    _triggerMoveHandlers(swipeDirection, gestureState) {
        const {onMove, onMoveUp, onMoveDown, onMoveLeft, onMoveRight} = this.props;
        const {SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN} = GestureHandler;
        onMove && onMove(swipeDirection, gestureState);
        switch (swipeDirection) {
            case SWIPE_LEFT:
                onMoveLeft && onMoveLeft(gestureState);
                break;
            case SWIPE_RIGHT:
                onMoveRight && onMoveRight(gestureState);
                break;
            case SWIPE_UP:
                onMoveUp && onMoveUp(gestureState);
                break;
            case SWIPE_DOWN:
                onMoveDown && onMoveDown(gestureState);
                break;
        }
    }

    _getSwipeDirection(gestureState) {
        const {SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN} = GestureHandler;
        const {dx, dy} = gestureState;
        if (this._isValidHorizontalSwipe(gestureState)) {
            return (dx > 0)
                ? SWIPE_RIGHT
                : SWIPE_LEFT;
        } else if (this._isValidVerticalSwipe(gestureState)) {
            return (dy > 0)
                ? SWIPE_DOWN
                : SWIPE_UP;
        }
        return null;
    }

    _isValidHorizontalSwipe(gestureState) {
        const {vx, dy} = gestureState;
        const {velocityThreshold, directionalOffsetThreshold} = this.swipeConfig;
        return isValidSwipe(vx, velocityThreshold, dy, directionalOffsetThreshold);
    }

    _isValidVerticalSwipe(gestureState) {
        const {vy, dx} = gestureState;
        const {velocityThreshold, directionalOffsetThreshold} = this.swipeConfig;
        return isValidSwipe(vy, velocityThreshold, dx, directionalOffsetThreshold);
    }

    render() {
        return (<View {...this.props} {...this._panResponder.panHandlers}/>);
    }
};

export { GestureHandler };
