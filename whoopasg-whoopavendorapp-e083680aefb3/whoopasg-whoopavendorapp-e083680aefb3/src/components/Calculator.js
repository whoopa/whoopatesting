import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import numeral from 'numeral'
import I18n from '../translations'
import {AppText} from './AppText'

const denominations = {
    "0.10": 0.1,
    "0.20": 0.2,
    "0.50": 0.5,
    "$1": 1,
    "$2": 2,
    "$5": 5,
    "$10": 10,
    "$50": 50,
};

class Calculator extends React.Component {

    state = {
        selected: [],
        value: 0
    };

    onPress(key) {
        this.setState({
            selected: [...this.state.selected, key],
            value: this.state.value + denominations[key]
        })
    }

    onClear() {
        if (this.state.selected.length > 0) {
            const lastKey = this.state.selected.slice(-1).pop();
            this.setState({
                selected: this.state.selected.slice(0, -1),
                value: this.state.value - denominations[lastKey]
            });
        }
    }

    renderDenominations() {
        return Object.keys(denominations).map(key => {
            return (
                <TouchableOpacity style={styles.denomination}
                                  onPress={this.onPress.bind(this, key)} key={`Denomination${key}`}>
                    <AppText style={styles.denominationText}>{key}</AppText>
                </TouchableOpacity>
            );
        })
    }

    render() {
        const {total} = this.props;
        return (
            <View style={[styles.container, this.props.style]}>
                <AppText style={styles.whiteLabel}>{I18n.t('changeCalculatorTitle')}</AppText>
                <View style={styles.box}>
                    <View style={styles.row}>
                        {this.renderDenominations()}
                    </View>
                    <View style={styles.pad}/>
                    <View style={styles.row}>
                        <TouchableOpacity onPress={this.onClear.bind(this)}>
                            <AppText
                                style={styles.clearLabel}>{this.state.selected.length > 0 ? I18n.t('clear') : " "}</AppText>
                        </TouchableOpacity>
                        <AppText style={styles.selectDenominations}>{this.state.selected.join(' + ')}</AppText>
                    </View>
                    <View style={styles.row}>
                        <AppText style={styles.total}>-{numeral(total).format("$0.00")}</AppText>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.row}>
                        <AppText style={styles.changeLabel}>{I18n.t('change')}</AppText>
                        <AppText style={styles.changeValue}>{numeral((this.state.value - total)).format("$0.00")}</AppText>
                    </View>
                    <View style={styles.pad}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: 260,
        margin: 20,
        marginBottom: 10,
    },
    box: {
        width: 260,
        backgroundColor: '#FFF',
        borderRadius: 10,
        padding: 10,
    },
    denomination: {
        height: 50,
        width: 50,
        borderColor: '#999',
        borderRadius: 25,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    },
    denominationText: {
        color: '#999',
        fontSize: 16,
    },
    divider: {
        height: 1,
        backgroundColor: '#CCC',
        margin: 15,
        marginLeft: 0,
        marginRight: 0,
    },
    row: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-end'
    },
    pad: {
        height: 20
    },
    clearLabel: {
        fontSize: 16,
        color: '#FF6E6E',
    },
    selectDenominations: {
        flex: 1,
        flexWrap: 'wrap',
        fontSize: 16,
        color: '#999',
        textAlign: 'right',
    },
    total: {
        flex: 1,
        fontSize: 16,
        color: '#999',
        textAlign: 'right'
    },
    changeLabel: {
        flex: 1,
        fontSize: 16,
        color: '#999'
    },
    changeValue: {
        flex: 1,
        fontSize: 28,
        marginLeft: 5,
        color: '#999',
        textAlign: 'right'
    },
    whiteLabel: {
        fontSize: 16,
        color: '#FFF',
        marginBottom: 10,
    },
});

export {Calculator};