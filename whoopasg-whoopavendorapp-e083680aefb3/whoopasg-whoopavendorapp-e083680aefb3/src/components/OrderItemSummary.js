import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import I18n from '../translations'
import {Customization} from './Customization'
import {Button} from './Button'
import {AppText} from './AppText'
import numeral from 'numeral';

const renderCustomizations = (customizations) => {
    if (customizations) {
        return customizations.map(c => <Customization
            customization={c.customization}
            key={'customization' + c.cid}
            state={0}
        />);
    }
};


const OrderItemSummary = ({item, onEdit}) => {
    let {container, title, quantity, x, price, options, columnContainer, columnGap} = styles;

    return (
        <View style={container}>
            <View style={columnContainer}>
                <AppText style={title}>{item.menu_item}</AppText>
                <View style={{paddingRight: 5}}>
                <Button onPress={onEdit}>{I18n.t('edit')}</Button>
                </View>
            </View>
            <View style={columnContainer}>
                <AppText style={x}>x</AppText>
                <AppText style={quantity}>{numeral(item.quantity).format('00')}</AppText>
                <AppText style={options}>({item.size} / {I18n.t((item.take_away ? 'takeAway' : 'dineIn'))})</AppText>
                <AppText style={price}>{numeral(item.price).format('$0,0.00')}</AppText>
            </View>
            <View style={[columnContainer, columnGap]}>
                {renderCustomizations(item.customizations)}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flex: 0,
        padding: 0,
        paddingRight: 0,
        elevation: 4,
    },
    columnContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-end',
        flexWrap: 'wrap'
    },
    title: {
        fontWeight: 'bold',
        color: '#000',
        flex: 1,
        fontSize: 28
    },
    x: {
        color: '#000',
        fontSize: 14,
        lineHeight: 20,
        padding: 5,
        paddingBottom:2,
    },
    quantity: {
        fontWeight: 'bold',
        color: '#000',
        lineHeight: 30,
        fontSize: 20,
        marginRight: 10
    },
    price: {
        color: '#000',
        flex: 0,
        lineHeight: 20,
        fontSize: 14,
        padding:5
    },
    options: {
        color: '#000',
        lineHeight: 30,
        fontSize: 20,
        flex: 1
    },
    columnGap: {
        marginTop: 10
    }
});

export {OrderItemSummary};