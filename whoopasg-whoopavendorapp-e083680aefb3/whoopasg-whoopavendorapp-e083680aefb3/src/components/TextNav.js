import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import {Spinner} from './Spinner'
import {AppText} from './AppText'

const renderItems = (items, selected, onPress) => {
    if (items) {
        return items.map((item, index) => {
            const {button, buttonSelected, buttonText, buttonTextSelected} = styles;
            let isSelected = (selected === item.id);
            return (
                <TouchableOpacity style={[button, (isSelected ? buttonSelected : null)]}
                                  onPress={() => onPress(item.id)}
                                  key={'textNav' + item.id}>
                    <AppText style={[buttonText, (isSelected ? buttonTextSelected : null)]}>{item.name}</AppText>
                </TouchableOpacity>
            );
        });
    }
    return <Spinner />
};

const TextNav = ({items, selected, onPress}) => {
    const {column, container} = styles;
    return (
        <View style={container}>
            {renderItems(items, selected, onPress)}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    button: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 10,
        borderBottomColor: '#777',
        borderBottomWidth: 1,
    },
    buttonSelected: {
        backgroundColor: '#FF6E6E'
    },
    buttonText: {
        textAlign: 'left',
        color: '#000',
        fontSize: 18,
    },
    buttonTextSelected: {
        color: '#FFF'
    },
});

export {TextNav};