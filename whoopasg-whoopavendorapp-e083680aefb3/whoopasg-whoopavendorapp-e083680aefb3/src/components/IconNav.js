import React from 'react';
import {ScrollView, View, StyleSheet, TouchableHighlight, Image} from 'react-native';
import apiConfig from '../services/api/config';
import { CachedImage } from 'react-native-cached-image';
import { AppText } from './AppText';

const renderItems = (items, selected, onPress) => {
    return items.map((item, index) => {
        const {button, buttonSelected,buttonDeSelected} = styles;
        let isSelected = (selected===item.id);
        return (
            <TouchableHighlight style={[button,(isSelected?buttonSelected:buttonDeSelected)]}
                                onPress={() => onPress(item.id)}
                                key={'category' + item.id}>
                <View>
                {renderIcon(item)}
                </View>
            </TouchableHighlight>
        );
    });
};

const renderIcon = (item) => {
    //Check if icon a single character or image
    if (item.icon.length > 1) {
        return <CachedImage source={{uri: `${apiConfig.assetUrl}/${item.icon}`}} style={styles.iconImage}/>
    }
    return <AppText style={styles.iconText}>{item.icon}</AppText>
};

const IconNav = ({items, selected, onPress}) => {
    const {container, column} = styles;
    return (
        <View style={container}>
            <ScrollView>
                <View style={column}>
                    {renderItems(items, selected, onPress)}
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: "rgba(0,0,0,0.9)",
    },
    column: {
        width: 60,
        flexDirection: 'column',
    },
    button: {
        opacity: 0.5,
        backgroundColor: '#FF6E6E',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'rgba(0,0,0,0.5)',
        elevation: 1,
        padding: 10,
    },
    buttonSelected: {
        elevation: 9,
        opacity: 1,
    },
    buttonDeSelected: {
        backgroundColor: '#81363b',
    },
    buttonText: {
        color: '#FFF',
        fontSize: 12,
        alignSelf: 'center'
    },
    iconImage: {
        width: 40,
        height: 40,
        alignSelf: 'center',
    },
    iconText: {
        backgroundColor: '#FFF',
        fontWeight: 'bold',
        fontSize: 25,
        width: 40,
        height: 40,
        textAlign: 'center',
        alignItems: 'center',
    },
});

export {IconNav};