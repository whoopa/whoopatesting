import React from 'react';
import {View, Text, Image, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import apiConfig from '../services/api/config';
import I18n from '../translations';
import numeral from 'numeral';
import {CachedImage} from 'react-native-cached-image';
import {AppText} from './AppText'
import {Toggle} from "./Toggle";

class MenuItems extends React.Component {
    state = {
        showToggle: false
    };

    toggleShowToggle(menuItemId){
        const showToggle = (this.state.showToggle === menuItemId) ? false : menuItemId;
        this.setState({
            showToggle
        })
    }

    renderMenuItem(menuItem){
        const {showToggle} = this.state;
        const {onSoldOut} = this.props;
        const {thumbnailContainerStyle, headerContainerStyle, headerTitleTextStyle, menuItemBox, toggleContainer, closeButton, closeButtonText} = styles;
        return(
            <View style={[menuItemBox, styles.elevated]}>
                <View style={[thumbnailContainerStyle, {opacity: menuItem.sold_out ? 0.3 : 1}]}>
                    {menuItem.icon ?
                        <CachedImage source={{uri: `${apiConfig.assetUrl}/${menuItem.icon}`}}
                                     style={styles.iconImage}/>
                        :
                        <Image source={require('../assets/images/defaultMenu.png')} style={styles.iconImage}/>
                    }
                </View>
                <View style={[headerContainerStyle, {opacity: menuItem.sold_out ? 0.3 : 1}]}>
                    <AppText style={headerTitleTextStyle}>{menuItem.name}</AppText>
                    <AppText
                        style={styles.headerPriceTextStyle}>{numeral(menuItem.sizes[0].price).format('$0,0.00')}</AppText>
                </View>
                {showToggle === menuItem.id ?
                <View style={toggleContainer}>
                    <TouchableWithoutFeedback onPress={()=>{this.toggleShowToggle(menuItem.id)}}>
                        <View style={closeButton}>
                        <AppText style={closeButtonText}>X</AppText>
                        </View>
                    </TouchableWithoutFeedback>
                    <Toggle onLeft={!menuItem.sold_out} noElevation
                            leftText={I18n.t('available')} rightText={I18n.t('soldOut')}
                            onChange={()=>{
                                this.toggleShowToggle(menuItem.id);
                                onSoldOut(menuItem.id, !menuItem.sold_out)
                            }} />
                </View>
                : null}
            </View>
        );
    }

    renderMenuItems() {
        const {items, onPress} = this.props;
        const self = this;
        if (items) {
            return this.props.items.map(function (menuItem) {
                return (
                    <TouchableWithoutFeedback
                        key={menuItem.id}
                        onPress={() => menuItem.sold_out ? null : onPress(menuItem.id)}
                        onLongPress={self.toggleShowToggle.bind(self, menuItem.id)}
                    >
                        {self.renderMenuItem.bind(self, menuItem)()}
                    </TouchableWithoutFeedback>
                );
            });
        } else {
            return null;
        }
    }

    render() {
        return (
            <View style={[styles.menuItemContainer]}>
                {this.renderMenuItems()}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    iconImage: {
        width: 110,
        height: 110,
        alignSelf: 'center'
    },
    elevated: {
        elevation: 10,
        // padding: 10,
        backgroundColor: '#FFF'
    },
    menuItemContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    menuItemBox: {
        // width : 390,
        width: 382,
        borderColor: 'rgba(0,0,0,0.5)',
        borderWidth: 1,
        borderBottomWidth: 0,
        padding: 4,
        paddingBottom: 5,
        flexDirection: 'row',
        marginRight: -1,
        marginTop: 0,
    },
    thumbnailContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerContainerStyle: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingLeft: 10,
        flexWrap: 'wrap',
        flex: 1
    },
    headerTitleTextStyle: {
        flex: 1,
        fontSize: 30,
        paddingTop: 5,
        lineHeight: 32,
        color: '#000',
        flexWrap: 'wrap'
    },
    headerPriceTextStyle: {
        fontSize: 20,
        paddingBottom: 10,
        color: '#000'
    },
    toggleContainer:{
        position: 'absolute',
        top:0, bottom:0, left:0, right: 0,
        backgroundColor: 'rgba(255,255,255,0.9)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeButton:{
        backgroundColor: '#FF6E6E',
        width: 30,
        height:30,
        borderRadius: 30,
        position: 'absolute',
        top: 2,
        right: 2,
    },
    closeButtonText:{
        fontWeight: 'bold',
        color: '#FFF',
        fontSize: 25,
        lineHeight: 25,
        textAlign: 'center',
    }
});

export {MenuItems};