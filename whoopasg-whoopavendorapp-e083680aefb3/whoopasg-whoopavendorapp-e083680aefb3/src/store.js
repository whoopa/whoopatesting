import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import reducers from './reducer'
import { autoRehydrate } from 'redux-persist'

export default function configureStore(initialState = undefined) {
    let middleware = [];
    middleware.push(thunk);

    console.log("Environment:", __DEV__);

    if (__DEV__) {
        const logger = createLogger();
        middleware.push(logger);
    }

    const enhancer = compose(
        applyMiddleware(...middleware),
        autoRehydrate()
    );
    const store = createStore(reducers, initialState, enhancer);
    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = require('./reducer').default;
            store.replaceReducer(nextRootReducer)
        })
    }
    return store;
}
