/* global fetch */
import fetchival from 'fetchival';
import apiConfig from './config';
import {getLanguages} from 'react-native-i18n'

export default class API {
    static _instance = null;
    static locale = "";

    constructor() {
        console.log("API Constructor");
        getLanguages().then(language => {
            console.log("API Lang", language);
            API.locale = language[0].replace('-', '_')
        });
    }

    static instance() {
        if (API._instance === null) {
          API._instance = new API();
        }

        return API._instance;
    }

    // static authenticate(password) {
    //
    //     let encodedUsername = password.substring(0, (password.length - 4));
    //     let username = parseInt(encodedUsername, 36);
    //     let real_password = password.substr(-4);
    //     console.log(`Auth ${apiConfig.url}/oauth/v2/token`, username, real_password);
    //
    //     return fetchival(`${apiConfig.url}/oauth/v2/token`)['post']({
    //         'grant_type': 'password',
    //         'client_id': apiConfig.clientId,
    //         'client_secret': apiConfig.clientSecret,
    //         'username': username,
    //         'password': real_password
    //     }).catch(API.handleError)
    // };
    //
    // static refresh(refresh_token) {
    //     console.log(`Refresh ${apiConfig.url}/oauth/v2/token`, refresh_token);
    //     return fetchival(`${apiConfig.url}/oauth/v2/token`)['post']({
    //         'grant_type': 'refresh_token',
    //         'client_id': apiConfig.clientId,
    //         'client_secret': apiConfig.clientSecret,
    //         'refresh_token': refresh_token
    //     }).catch(API.handleError)
    // };

    static checkPassword(password, payload = {}) {
        console.log(`${apiConfig.url}/options/checks/passwords`, password, payload);
        return fetchival(`${apiConfig.url}/options/checks/passwords`, {
            headers: {'X-AUTH-TOKEN': password}
        })['post'](payload).catch(API.handleError)
    };

    static handleError(err) {
        if (err.response) {
            if (err.response.json) {
                //console.log("JSON ERROR: ", err.response.url, err.response.text());
                return err.response.json()
                    .then(json => {
                        console.log(json);
                        const code = (json.code || err.response.status);
                        const msg = (json.message || json.error_description || json.error)
                        throw `${msg}`
                    })
                    .catch(e => {
                        console.log(e);
                        const code = (err.response.status);
                        throw `Error ${code} ${e.toString()}`
                    })
                    ;
            } else {
                console.log("Else Error",err);
                throw (`Error ${err.response.status}: ` + err.response.statusText ? err.response.statusText : "Unknown")
            }
        } else {
            console.log("Misc Error",err);
            throw err;
        }
    }

    callSecure(password, endPoint, payload = {}, method = 'get', headers = {}) {
        console.log('secure call', API._instance);
        let apiHeaders = {'Accept-Language': `${API.locale}`, ...headers};
        if (password) {
            apiHeaders = {'X-AUTH-TOKEN': password, ...apiHeaders}
        }

        console.log(method, `${apiConfig.url}/${endPoint}`, payload, apiHeaders);

        return fetchival(`${apiConfig.url}/${endPoint}`, {
            headers: apiHeaders,
        })[method.toLowerCase()](payload)
            .catch(function (err) {
                if (err.response) {
                    if (err.response.status === 401) {
                        //Token Expired
                        throw new ExpiredTokenError(endPoint, payload, method, headers)
                    } else {
                        return API.handleError(err)
                    }
                } else {
                    console.log(err);
                    throw err;
                }
            })
    };
}

export class ExpiredTokenError {
    endPoint;
    payload;
    method;
    headers;

    constructor(endPoint, payload, method, headers) {
        this.endPoint = endPoint;
        this.payload = payload;
        this.method = method;
        this.headers = headers;
    }
}



